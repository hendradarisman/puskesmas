<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class icons {    
    public function xls($width,$height){
            return "<img src=\"".base_url()."assets/icons/File-Extension-Xls.png\" width=\"".$width."\" height=\"".$height."\" />";
        }
        
    public function pdf($width,$height){
            return "<img src=\"".base_url()."assets/icons/File-Extension-Pdf.png\" width=\"".$width."\" height=\"".$height."\" />";
        }
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

