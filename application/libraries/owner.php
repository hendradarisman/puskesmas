<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class owner {
        var $nama='';
        var $noijin='';
        var $alamat='';
        var $telp='';
        var $fax='';
        var $email='';
        var $pjklinik='';
        var $pjlab='';
        var $kota='';
        var $website='';
        var $logo='';
        var $favicon='';
    
    public function __construct(){
        $query = "SELECT nama,noijin,alamat,telp,fax,email"
                . " ,pjklinik,pjlab,kota,website,logo FROM MstKlinik ";
        $result = mysql_query($query);
        $row = mysql_fetch_array($result);
        if(!empty($row[0])){
            $this->nama     = $row[0];
            $this->noijin   = $row[1];
            $this->alamat   = $row[2];
            $this->telp     = $row[3];
            $this->fax      = $row[4];
            $this->email    = $row[5];
            $this->pjklinik = $row[6];
            $this->pjlab    = $row[7];
            $this->kota     = $row[8];
            $this->website  = $row[9];
            $this->logo     = $row[10]; 
            $this->favicon  = "<link rel=\"icon\" type=\"image/png\" href=\"".$row[10]."\">"; 
        }
    }
    
    public function setfavicon($param) {
        $query = "SELECT nama,noijin,alamat,telp,fax,email"
                . " ,pjklinik,pjlab,kota,website,logo FROM MstKlinik ";
        $result = mysql_query($query);
        $row = mysql_fetch_array($result);
        if(!empty($row[0])){
            $this->favicon  = "<link rel=\"icon\" type=\"image/png\" href=\"$param".$row[10]."\">"; 
        }
        return $this->favicon;
    }
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

