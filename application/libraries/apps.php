<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class apps {
    var $name="Puskesmas Mari Pari";
    var $release="Beta V0.1";
    var $ver="Beta V0.1";
    var $modname="";
    
    public function __construct(){
        $this->CI =& get_instance();
        $this->CI->load->database();

        $query = $this->CI->db->select('nama')->get('mstklinik');
		$row = $query->row_array();
        if(!empty($row[0])){
            $this->name = $row[0];
        }else{
            $this->name = "LABKESDA KABUPATEN BANDUNG BARAT";
        }
        
    }
        
    public function modulname() {
        return $this->modname;
    }
    
    public function titlepage($param) {
        // $query = "SELECT namamodul FROM mstmodul "
        //         . " WHERE kelasmodul='$param'";
        // $result = mysql_query($query);
        // $row = mysql_fetch_array($result);
        // $this->modname = $row[0];
        
        // return $row[0]." - ".$this->name;

        $query = $this->CI->db->select('namamodul')->where('kelasmodul',$param)->get('mstmodul');
		$row = $query->row_array();
        $this->modname = $row[0];
        
        return $row[0]." - ".$this->name;

    }
    
    public function modulsource($param) {
      
        $query = $this->CI->db->select('namamodul')->where('kelasmodul',$param)->get('mstmodul');
		$row = $query->row_array();
        $this->modname = $row[0];
        
        return $row[0];
    }
}

/* 
 * Created by Pikamedia
 * Email : info@pikamedia.id
 * info@pikamedia.id
 */

