<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class apps {
    var $name="Puskesmas Mari Pari";
    var $release="Beta V0.1";
    var $ver="Beta V0.1";
    var $modname="";
    
    public function __construct(){
        $query = "SELECT nama FROM mstklinik ";
        $result = mysql_query($query);
        $row = mysql_fetch_array($result);
        if(!empty($row[0])){
            $this->name = $row[0];
        }else{
            $this->name = "Puskesmas Mari Pari";
        }
    }
        
    public function modulname() {
        return $this->modname;
    }
    
    public function titlepage($param) {
        $query = "SELECT namamodul FROM mstmodul "
                . " WHERE kelasmodul='$param'";
        $result = mysql_query($query);
        $row = mysql_fetch_array($result);
        $this->modname = $row[0];
        return $row[0]." - ".$this->name;
    }
    
    public function modulsource($param) {
        $query = "SELECT namamodul FROM mstmodul "
                . " WHERE kelasmodul='$param'";
        $result = mysql_query($query);
        $row = mysql_fetch_array($result);
        return $row[0];
    }
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

