
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
<?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
      
    ?>
      <!-- <body onload="tampil_grid();"> -->
  
<div id="wrapper">
<?php
    $this->load->view('others/nav');
?>

<div id="page-wrapper">
  <div class='row'>
    <div class='col-lg-12'>
      <h3 class="page-header">
        <a class="btn btn-primary no-print" href="javascript:printDiv('area-1');">Print</a>
      </h3>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div id="area-1">
        <div>

          <div align="center">
            <b><u>SURAT RUJUKAN</u></b></br>
           PSKS/<?php echo date("Y"); ?>/124
          </div>
        </div>
        <?php 
      

        foreach ($tampil as $show){
            //var_dump($show);
            $tgl = $show->tanggallahir;

            function hitung_umur($tgl) {
                list($year,$month,$day) = explode("-",$tgl);
                $year_diff  = date("Y") - $year;
                $month_diff = date("m") - $month;
                $day_diff   = date("d") - $day;
                if ($month_diff < 0) $year_diff--;
                    elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
                return $year_diff;
            }
            //Tampilkan Umur dengan Tanggal Lahir 1990-Oktober-25
           
            
            ?>
         
    <div>
        <p>
        Yth. Dr. Agung</br>
        
        <?php echo $show->nama_rs_dr;?></br>
        Ditempat
        </br>
        </br>
        Mohon pemeriksaan dan penanganan lebih lanjut terhadap Pasien :</br>
<table border="0" width="500px">
  <tr>
    <td><b>Nama Pasien</b> </td>
    <td>:</td>
    <td>&nbsp;<?php echo $show->namalengkap;?></td>
  </tr>
  <tr>
    <td><b>Jenis Kelamin</b> </td>
    <td>:</td>
    <td>&nbsp;<?php if ($show->kelamin='L')
    echo "Laki Laki";
    else 
    echo "Perempuan";?></td>
  </tr>
  <tr>
    <td><b>Umur</b> </td>
    <td>:</td>
    <td>&nbsp;<?php  echo hitung_umur($tgl);?> Tahun</td>
  </tr>
  <tr>
    <td><b>Alamat</b> </td>
    <td>:</td>
    <td>&nbsp;Cisitu</td>
  </tr>
</table>
</br>
</br>

Dengan hasil pemeriksaan sebagai berikut:</br>
<table border="0" width="300px">
  <tr>
    <td><b>1.</b> </td>
    <td><b>Hasil Anamnesa</b> </td>
    <td>:</td>
    <td>&nbsp;--</td>
  </tr>
  <tr>
    <td><b>2.</b> </td>
    <td><b>Diagnose</b> </td>
    <td>:</td>
    <td>&nbsp;--</td>
  </tr>
  <tr>
    <td><b>3.</b> </td>
    <td><b>Pengobatan Sementara</b> </td>
    <td>:</td>
    <td>&nbsp;--</td>
  </tr>
</table>
</br>
Demikian atas kerjasamanya yang baik, kami ucapkan terimakasih.
</br>
</br>
<div align="Right">
  Garut, <?php echo date("d M Y"); ?></br>
  Yang merujuk,</br>
  </br>
  </br>
  </br>
  Uus Tampan
</div>
</p>
</div>
<?php
        }
        ?>
      </div>
    </div>
  </div>
  <!-- /#page-wrapper -->

  <textarea id="printing-css" style="display:none;">.no-print{display:none}</textarea>
<iframe id="printing-frame" name="print_frame" src="about:blank" style="display:none;"></iframe>
<script type="text/javascript">
//<![CDATA[
function printDiv(elementId) {
    var a = document.getElementById('printing-css').value;
    var b = document.getElementById(elementId).innerHTML;
    window.frames["print_frame"].document.title = document.title;
    window.frames["print_frame"].document.body.innerHTML = '<style>' + a + '</style>' + b;
    window.frames["print_frame"].window.focus();
    window.frames["print_frame"].window.print();
}
//]]>
</script>