<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo $this->owner->favicon; ?>
    
    <title><?php echo $this->apps->name; ?></title>
    <?php
        $this->load->view('others/css');
        $this->load->view('others/js');
        $statregister = $this->session->userdata('statregister');
        $this->session->unset_userdata('statregister');
    ?>
    <style type="text/css">
        body{
            background-color: #012E5F;
        }
    </style>
</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-repeat"></span>
                            Lupa Password
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="<?php echo base_url();?>account/submit">
                            <fieldset>
                                <div class="form-group col-md-12">
                                    <input class="form-control" placeholder="Email Anda" name="txtemail" 
                                           type="email" autofocus="" required="">
                                </div>
                                <div class="form-group col-md-12">
                                    <button type="submit" class="btn btn-primary">Reset password saya</button>
                                    <a href="<?php echo base_url();?>" type="button" class="btn btn-default">Batal</a>
                                </div>
                            </fieldset>
                        </form>
                        <div class="col-md-12" style="text-align: left;">
                            Sudah punya akun? <a href="<?php echo base_url();?>access/login">Silahkan Masuk!</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
