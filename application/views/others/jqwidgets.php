<link rel="stylesheet" type="text/css" href="<?=base_url() ?>assets/jqwidgets/styles/jqx.base.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url() ?>assets/jqwidgets/styles/jqx.metro.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url() ?>assets/jqwidgets/styles/jqx.fresh.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url() ?>assets/jqwidgets/styles/jqx.bootstrap.css" />
<link rel="stylesheet" type="text/css" href="<?=base_url() ?>assets/jqwidgets/styles/jqx.metrodark.css" />

<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxcore.js"></script>

<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxgrid.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxgrid.selection.js"></script>	
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxgrid.filter.js"></script>		
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxgrid.pager.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxgrid.sort.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxgrid.columnsresize.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxgrid.grouping.js"></script> 
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxgrid.edit.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxgrid.aggregates.js"></script> 

<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxdata.js"></script>			
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxscrollbar.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxbuttons.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxdropdownlist.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxmenu.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxlistbox.js"></script>

<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxwindow.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxtabs.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxinput.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxvalidator.js"></script>

<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxdropdownbutton.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxnumberinput.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxcalendar.js"></script>

<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxcombobox.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxcheckbox.js"></script>


<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxpanel.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxtree.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxdropdownlist.js"></script>	
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxslider.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/jqwidgets/jqxdatetimeinput.js"></script>
<script type="text/javascript">
    var globTheme='metro';
</script>