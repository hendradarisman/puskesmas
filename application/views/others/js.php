<!--Start Include Jquery -->
<script type="text/javascript" src="<?=base_url() ?>assets/js/jquery-1.9.1.js"></script>

<!-- Core Scripts - Include with every page -->
<script src="<?=base_url() ?>assets/js/jquery-1.10.2.js"></script>
<script src="<?=base_url() ?>assets/js/bootstrap.min.js"></script>
<script src="<?=base_url() ?>assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>

<!-- SB Admin Scripts - Include with every page -->
<script src="<?=base_url() ?>assets/js/sb-admin.js"></script>


<!--Start Include Bootstrap -->
<script type="text/javascript" src="<?=base_url() ?>assets/js/bootstrap.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/affix.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/alert.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/button.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/carousel.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/collapse.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/modal.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/popover.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/scrollspy.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/tab.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/tooltip.js"></script>
<script type="text/javascript" src="<?=base_url() ?>assets/js/transition.js"></script>

<!--Start Include Datatable -->
<script src="<?=base_url() ?>assets/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="<?=base_url() ?>assets/js/plugins/dataTables/dataTables.bootstrap.js"></script>

<script src="<?=base_url() ?>assets/js/moment.js"></script>
<script src="<?=base_url() ?>assets/js/bootstrap-datetimepicker.min.js"></script>

<script src="<?=base_url() ?>assets/js/jquery.form.js"></script>
<script src="<?=base_url() ?>assets/nicedit/nicEdit.js"></script>
