<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0;background-color: #0B5A8F;">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?=base_url() ?>"><?php echo $this->apps->name; ?></a>
            </div>
            <!-- /.navbar-header -->
            <ul class="nav navbar-top-links navbar-left">
                <?php
                        $sesArray = $this->session->all_userdata();
                        $sidgrup = $sesArray['idgrup'];

                        $data = array(
                            'mstmenu.idmenu',
                            'mstmenu.namamenu',
                            'mstmenu.keterangan'
                        );

                        $this->db->select($data);
                        $this->db->join('mstmodul', 'tranaksesgrup.idmodul = mstmodul.idmodul');
                        $this->db->join('mstmenu', 'mstmenu.idmenu = mstmodul.idmenu');
                        $this->db->where('mstmenu.datastat !=', 'DELETED'); 
                        $this->db->where('tranaksesgrup.idgrup',$sidgrup); 
                        $this->db->order_by("mstmenu.namamenu", "asc"); 
                        $this->db->group_by(array('mstmenu.idmenu',
                                                    'mstmenu.namamenu',
                                                    'mstmenu.keterangan')); 
                        $query = $this->db->get("tranaksesgrup");
                        if ($query->num_rows() > 0)
                        {
                             foreach ($query->result() as $row)
                                {
                    ?>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?php echo $row->namamenu; ?>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts" style="width: auto;">
                        <?php
                                $data = array(
                                    'mstmodul.namamodul',
                                    'mstmodul.kelasmodul',
                                    'mstmodul.keterangan'
                                );

                                $this->db->select($data); 
                                $this->db->join('mstmodul', 'tranaksesgrup.idmodul = mstmodul.idmodul');
                                $this->db->join('mstmenu', 'mstmenu.idmenu = mstmodul.idmenu');

                                $this->db->where('mstmodul.idmenu',$row->idmenu); 
                                $this->db->where('mstmodul.datastat !=', 'DELETED');
                                $this->db->where('mstmenu.datastat !=', 'DELETED'); 
                                $this->db->where('tranaksesgrup.idgrup',$sidgrup); 
                                $this->db->order_by("mstmodul.namamodul", "asc"); 
                                $query = $this->db->get("tranaksesgrup");
                                if ($query->num_rows() > 0)
                                {
                                     foreach ($query->result() as $rowmodul)
                                        {
                            ?>
                        <li>
                            <a href="<?=base_url() ?><?php echo $rowmodul->kelasmodul; ?>"><?php echo $rowmodul->namamodul; ?></a>
                        </li>
                        <?php
                                        }

                                }
                            ?>
                        </ul>
                    </li>      
                    <?php
                                }
                        }
                    ?>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
            </ul>
            
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <a href="#">
                                <div>
                                    <strong>John Smith</strong>
                                    <span class="pull-right text-muted">
                                        <em>Yesterday</em>
                                    </span>
                                </div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eleifend...</div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
                <!-- /.dropdown -->
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <span class="pull-right text-muted">40% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-comment fa-fw"></i> New Comment
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> Message Sent
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-alerts -->
                </li>
                <!-- /.dropdown -->
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url();?>access/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
        </nav>

<?php

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

