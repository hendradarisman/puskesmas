<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
            $statproses = $this->session->userdata('statproses');
            $this->session->unset_userdata('statproses');            
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->   
    <script>
        $(document).ready(function() {
            $('#tbDataPasien').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": "<?=base_url() ?>trantindakan/jsondaftarpasien",
                "columns": [
                    {"width": "5%" },
                    null,
                    {"width": "30%" },
                    {"width": "30%" },
                    {"width": "5%" }
                ],
                "oLanguage": {
                    "sProcessing": "<div class=\"col-lg-12\"><h5><label class=\"label label-danger\">Silahkan tunggu, sedang mengambil data</label><h5></div>"
                }
            });
            $('#tbDataPasien').tooltip({
                selector: "[data-toggle=tooltip]",
                container: "body"
            });
        });
    </script>
  </head>

  <body>
    <div id="wrapper">
        <?php
            $this->load->view('others/nav');
        ?>
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
            <?php
                if($statproses=="ok"){
            ?>
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <span class="glyphicon glyphicon-ok-circle"></span>
                    Data Berhasil disimpan.
                </div>
            <?php
                }elseif($statproses=="gagal"){
            ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <span class="glyphicon glyphicon-exclamation-sign"></span>
                    Data gagal disimpan, silahkan ulangi sesaat lagi.
                </div>
            <?php
                }elseif($statproses=="kurang"){
            ?>
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <span class="glyphicon glyphicon-exclamation-sign"></span>
                    Data gagal disimpan! <strong>Anamnesis, Pemeriksaan fisik, dan diagnosis tidak boleh kosong!</strong>
                </div>
            <?php
                }
            ?>     
            <table class="table table-striped table-bordered table-hover" id="tbDataPasien">
                <thead>
                    <tr class="danger">
                        <th style="width: 15%;text-align: center;">No.</th>
                        <th style="text-align: center;">Nama Pasien</th>
                        <th style="width: 10%;text-align: center;">Keluhan</th>
                        <th style="width: 10%;text-align: center;">Alamat</th>
                        <th style="width: 7%;text-align: center;">Pilih</th>           
                    </tr>
                </thead>

            </table>
            </div>
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
  </body>
</html>
