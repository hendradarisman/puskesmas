<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->setfavicon('../../'); ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->   
    <script type="text/javascript">   
            bkLib.onDomLoaded(function() { 
                nicEditors.allTextAreas({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript']});
                
            });
            
            function add_pasien(kdpasien){
                var xkdpasien = document.getElementById('kdpasien');
                    xkdpasien.value = kdpasien;
                    getpasien();
            }
            
            function clsobat(){
                $('#kodeobat').val('');
                $('#idobat').val('');
                $('#jumlahobat').val('');
                $('#hargaobat').val('');
                $("#namaobat").html("<label class=\"label label-info\">Nama obat : - </label>");
            }
            
            function clstindakan(){
                $('#tindakanid').val('');
                $('#kodetindakan').val('');
                $('#hargatindakan').val('');
                $('#keterangan').val('');
                $("#nmtindakan").html("<label class=\"label label-info\">Nama Tindakan : - </label>");
            }
            
            function add_tindakan(id,kode,nama,tarif){
                var v1 = document.getElementById('tindakanid');
                    v1.value = id;
                var v2 = document.getElementById('kodetindakan');
                    v2.value = kode;
                $("#nmtindakan").html("<label class=\"label label-info\">Nama tindakan : " + nama + "</label>");
                var v4 = document.getElementById('hargatindakan');
                    v4.value = tarif;
            }
            
            function add_resep(idobat, kodeobat, namaobat, sisaobat, hargajual){
                var v1 = document.getElementById('idobat');
                    v1.value = idobat;
                var v2 = document.getElementById('kodeobat');
                    v2.value = kodeobat;
                $("#namaobat").html("<label class=\"label label-info\">Nama obat : " + namaobat + "</label>");
                var v3 = document.getElementById('sisaobat');
                    v3.value = sisaobat;
                var v4 = document.getElementById('hargaobat');
                    v4.value = hargajual;
            }
            
            function getpasien(){
                var kdpasien=$('#kdpasien').val();
                $("#divpasien").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/getpasien'; ?>",
                    data:{"kdpasien":kdpasien},
                    success: function(resp){                            
                            $("#divpasien").html(resp);
                            $('#kdpasien').focus();
                            var idpasien=$('#idpasien').val();
                            if(idpasien!==undefined){
                                getformtindakan();
                            }
                    }
                });
            };
            
            function getformtindakan(){
                gettabelresep();
                gettabeltindakan();
                gettblfoto();
            };
            
            function gettabelresep(){
                var idregistrasi=$('#idregistrasi').val();
                $("#tblresep").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/gettabelresep'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){                            
                            $("#tblresep").html(resp);
                    }
                });
            };
            
            function gettabeltindakan(){
                var idregistrasi=$('#idregistrasi').val();
                $("#tabeltindakan").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/gettabeltindakan'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){                            
                            $("#tabeltindakan").html(resp);
                    }
                });
            };
            
            function gettbantrianpasien(){
                $("#tbantrianpasien").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/getantrianpasien'; ?>",
                    success: function(resp){                            
                            $("#tbantrianpasien").html(resp);
                    }
                });
            }
            
            function getDaftarTindakan(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/getDaftarTindakan'; ?>",
                    success: function(resp){                            
                            $("#dvTindakanPasien").html(resp);
                    }
                });            
            }
            
            function getDaftarObat(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/getDaftarObat'; ?>",
                    success: function(resp){                            
                            $("#dvMstObat").html(resp);
                    }
                }); 
            }
            
            function gettblfoto(){
                var idregistrasi=$('#idregistrasi').val();  
                $("#tblfoto").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/gettblfoto'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){                            
                            $("#tblfoto").html(resp);
                            var v1 = document.getElementById('fileupload');
                                v1.value = "";
                            var v2 = document.getElementById('txtKeteranganFoto');
                                v2.value = "";
                    }
                });
            }
            
            function getfotopasien(idtrangambar){
                $("#fotopasien").html("<h3><label class=\"label label-info\">Mengambil gambar ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/modalfoto'; ?>",
                    data:{"idtrangambar":idtrangambar},
                    success: function(resp){                            
                            $("#fotopasien").html(resp);
                    }
                });
            }
            
            function hapusfoto(idtrangambar,namafoto){
                var r=confirm("Lanjutkan menghapus data '"+namafoto+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'trantindakan/hapusfoto'; ?>",
                            data: {"idtrangambar":idtrangambar,
                                    "stat":"hapus"},
                            success: function(resp) {
                                if(resp=='1'){
                                   $('.msg').html('Berhasil dihapus');
                                   gettblfoto();
                                }else{
                                   $('.msg').html('Gagal dihapus');
                                   return false;
                                }
                            },
                            error: function(resp){
                                $('.msg').html('Gagal dihapus');
                                  return false;
                            }
                        });
                  }
            };
            
            function bataltindakan(idtrantindakan,kodetindakan){
                var r=confirm("Lanjutkan menghapus data '"+kodetindakan+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'trantindakan/prosesdata'; ?>",
                            data: {"idtrantindakan":idtrantindakan,
                                    "stat":"hapus"},
                            success: function(resp){
                                    if(resp=='1'){
                                        //alert("Data "+kodetindakan+" Berhasil diproses!");
                                        gettabeltindakan();
                                    }else{
                                        alert("Data "+kodetindakan+" Gagal diproses! : Error : "+resp);
                                        return false;
                           }
                            }
                        });
                  }
            };
            
            function batalresep(idtranresep,kodeobat){
                var r=confirm("Lanjutkan menghapus data '"+kodeobat+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'trantindakan/prosesresep'; ?>",
                            data: {"idtranresep":idtranresep,"jumlahobat":"0",
                                    "hargaobat":"0","stat":"hapus"},
                            success: function(resp){
                                    if(resp=='1'){
                                        //alert("Data "+kodeobat+" Berhasil diproses!");
                                        gettabelresep();
                                        clsobat();
                                        $('#kodeobat').focus();
                                    }else{
                                        alert("Data "+kodeobat+" Gagal diproses! : Error : "+resp);
                                        return false;
                           }
                            }
                        });
                  }
            };
            
            function simpantindakan(){
                var idregistrasi=$('#idregistrasi').val();
                var tindakanid=$('#tindakanid').val();
                var hargatindakan=$('#hargatindakan').val();
                var keterangan=$('#keterangan').val();
                var idpasien=$('#idpasien').val();
                var kodetindakan=$('#kodetindakan').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/prosesdata'; ?>",
                    data:{"idregistrasi":idregistrasi,"tindakanid":tindakanid,"hargatindakan":hargatindakan,
                            "keterangan":keterangan,"idpasien":idpasien},
                    success: function(resp){                            
                            if(resp=='1'){
                               //alert("Data "+kdpasien+" Berhasil diproses!");
                               gettabeltindakan();
                               clstindakan();
                               $('#kodetindakan').focus();
                           }else{
                               alert("Data "+kodetindakan+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                    }
                });
            };
            
            function simpanresep(){
                var idregistrasi=$('#idregistrasi').val();
                var kodeobat=$('#kodeobat').val();
                var idobat=$('#idobat').val();
                var jumlahobat=$('#jumlahobat').val();
                var hargaobat=$('#hargaobat').val();
                var idpasien=$('#idpasien').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/prosesresep'; ?>",
                    data:{"idregistrasi":idregistrasi,"idobat":idobat,"jumlahobat":jumlahobat,
                            "hargaobat":hargaobat,"idpasien":idpasien},
                    success: function(resp){                            
                            if(resp=='1'){
                               //alert("Data "+kdpasien+" Berhasil diproses!");
                               gettabelresep();
                               clsobat();
                               $('#kodeobat').focus();
                           }else{
                               alert("Data "+kodeobat+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                    }
                });
            };
            
            function simpanfoto(){
                var idregistrasi=$('#idregistrasi').val();
                var txtKeteranganFoto=$('#txtKeteranganFoto').val();
                var idpasien=$('#idpasien').val();
                var fileupload=$('#fileupload').val();
                
                $('#uploadfotopasien').ajaxForm({
                 url:'<?php echo base_url().'trantindakan/uploadfoto'; ?>',
                 type: 'post',
                 data:{"idregistrasi":idregistrasi,"txtKeteranganFoto":txtKeteranganFoto
                            ,"idpasien":idpasien,"fileupload":fileupload},
                 //resetForm: true,
                 beforeSubmit: function() {
                  $('.msg').html('Silahkan Tunggu ... ');
                 },
                 success: function(resp) {
                   if(resp=='1'){
                       $('.msg').html('Berhasil di simpan');
                       gettblfoto();
                   }else{
                       $('.msg').html('Gagal di simpan');
                       return false;
                    }
                 },
                 error: function(resp){
                     $('.msg').html('Gagal di simpan');
                       return false;
                 }
                });     
            };
            
            function submitpasien(){
                var idtrandiagnosis=$('#idtrandiagnosis').val();
                var idregistrasi=$('#idregistrasi').val();
                var idpasien=$('#idpasien').val();
                var kdpasien=$('#kdpasien').val();
                var diagnosis=$('#diagnosis').val();
                var catatan=$('#catatan').val();
                var anamnesis=$('#anamnesis').val();
                //alert(idregistrasi);
                if(idregistrasi=="" || idregistrasi==undefined){
                    alert("Pasien tidak ada, silahkan cek data pasien!");
                    return false;
                }
                
                if(anamnesis==""){
                    alert("Anamnesis harus diisi!");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/submitpasien'; ?>",
                    data:{"idregistrasi":idregistrasi,"idpasien":idpasien,"diagnosis":diagnosis,
                            "catatan":catatan,"idtrandiagnosis":idtrandiagnosis,"anamnesis":anamnesis},
                    success: function(resp){                            
                            if(resp=='1'){
                               alert("Data "+kdpasien+" Berhasil diproses!");
                               location.reload();
                           }else{
                               alert("Data "+kdpasien+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                    }
                });
            };
            
            function tampil_riwayat(){
                var kdpasien=$('#kdpasien').val();
                $("#tbRMPasien").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/getriwayatpasien'; ?>",
                    data:{"kdpasien":kdpasien},
                    success: function(resp){                            
                            $("#tbRMPasien").html(resp);
                    }
                });
            };
    </script>
  </head>

  <body onload="getformtindakan();">
    <div id="wrapper">
        <?php
            $this->load->view('others/nav');
        ?>
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        <div class="row">
<?php                
        $idregistrasi = $this->uri->segment(3);
        $qrypasien="SELECT a.`idregistrasi`,a.`noantrian`,b.`namalengkap`,a.`keluhan`
                    ,b.`alamat`,b.`idpasien`,b.`kodepasien` FROM `tranregistrasi` a 
                            INNER JOIN `mstpasien` b ON a.`idpasien` = b.`idpasien`
                        WHERE idregistrasi = '$idregistrasi'";
        $result0=  mysql_query($qrypasien);
        $rowpasien=mysql_fetch_array($result0);
?>
<input type="hidden" value="<?php echo $rowpasien[6]?>" id="kdpasien" name="kdpasien" />
<div class="col-lg-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title">Data Pasien</h3>
      </div>
      <div class="panel-body">
          <table style="font-size: 16px;" class="table">
              <tr>
                  <td width="70">Nama</td>
                  <td> : <?php echo $rowpasien[2];?></td>
                  <td rowspan="3" width="70">
                    <button type="button" class="btn btn-info btn-lg" data-backdrop="static" style="height: 105px;"
                            data-toggle="modal" data-target="#myRMPasien" onclick="tampil_riwayat();">
                        <span class="glyphicon glyphicon-list-alt"></span> Lihat Rekam Medis Pasien
                    </button>
                  </td>
              </tr>
              <tr>
                  <td width="70">Alamat</td>
                  <td> : <?php echo $rowpasien[4];?></td>
              </tr>
              <tr>
                  <td width="70">Keluhan</td>
                  <td> : <?php echo $rowpasien[3];?></td>
              </tr>
           </table>
      </div>
    </div>    
</div>                       
<div class="col-lg-12">   
<form class="form-inline" role="form">
    <input type="hidden" class="form-control" id="tindakanid" >
    <div class="form-group">
        <div class="input-group" style="max-width: 175px;">
            <input type="text" class="form-control" id="kodetindakan" placeholder="Kode Tindakan">
            <div class="input-group-btn">
                <button onclick="getDaftarTindakan();" type="button" class="btn btn-info" data-backdrop="static" data-toggle="modal" data-target="#myModalTindakan"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>                
    </div>
    <div class="form-group">
        <input type="text" class="form-control" id="hargatindakan" placeholder="Tarif">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" id="keterangan" placeholder="Keterangan (optional)">
    </div>
    <button type="button" class="btn btn-primary" onclick="simpantindakan();"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
</form>
<div style="padding: 10px 0px;">
    <div id="nmtindakan"><label class="label label-info">Nama Tindakan : -</label></div>
</div>

<div style="height:230px;overflow-x: hidden;overflow-y: auto;border: solid 1px #D1D1D1;padding: 2px 2px;margin-bottom: 10px;">
    <div id="tabeltindakan">
        <table class="table table-striped table-bordered table-hover" id="tbtindakan">
            <thead>
                <tr class="danger">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Nama Tindakan</th>
                    <th style="text-align: center;">Keterangan</th>
                    <th style="text-align: center;">Tarif</th>
                    <th style="text-align: center;">Batal</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query2 = "SELECT b.`kodetindakan`,b.`namatindakan`,a.`tarif`,a.`idtrantindakan`,a.keterangan
                                FROM `trantindakan` a INNER JOIN `msttindakan` b ON a.`idtindakan` = b.`idtindakan`	
                                WHERE a.`idregistrasi` = '$idregistrasi' and a.datastat <> 'DELETED' ORDER BY a.`tgltindakan` ASC";
                    //echo $query2;
                    $result2=  mysql_query($query2);
                    while($row=mysql_fetch_array($result2)){
                       echo "<tr>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[0]</td>";
                       echo "<td style=\"width: 25%;text-align: left\">$row[1]</td>";
                       echo "<td style=\"text-align: left\">$row[4]</td>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[2]</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-default\" href=\"javascript:void(0);\" onclick=\"bataltindakan('$row[3]','$row[0]')\">Batal</a>
                            </td>";
                       echo "</tr>";
                    }
                ?>
            </tbody>                          
        </table>
    </div>
</div>   

<!--Resep Area Start-->
<center>
    <h3 style="border-bottom: solid #D1D1D1 1px;padding-bottom: 5px;">
        Resep Obat
    </h3>
</center>    
<form class="form-inline" role="form">
    <input type="hidden" class="form-control" id="idobat" >
    <div class="form-group">
        <div class="input-group" style="max-width: 175px;">
            <input type="text" class="form-control" id="kodeobat" placeholder="Kode Obat">
            <div class="input-group-btn">
                <button onclick="getDaftarObat();" type="button" class="btn btn-info" data-backdrop="static" data-toggle="modal" data-target="#myModalObat"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>                
    </div>
    <div class="form-group">
        <input type="hidden" class="form-control" id="sisaobat">
        <input type="text" class="form-control" id="jumlahobat" placeholder="Jumlah Obat">
    </div>    
    <div class="form-group">
        <input type="text" class="form-control" id="hargaobat" placeholder="Harga">
    </div>
    <button type="button" class="btn btn-primary" onclick="simpanresep();"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
</form>
<div style="padding: 10px 0px;">
    <div id="namaobat"><label class="label label-info">Nama Obat : -</label></div>
</div>

<div style="height:230px;overflow-x: hidden;overflow-y: auto;border: solid 1px #D1D1D1;padding: 2px 2px;margin-bottom: 10px;">
    <div id="tblresep">
        <table class="table table-striped table-bordered table-hover" id="tbresep">
            <thead>
                <tr class="danger">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Nama Obat</th>
                    <th style="text-align: center;">Jumlah</th>
                    <th style="text-align: center;">Harga</th>
                    <th style="text-align: center;">Batal</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query3 = "SELECT b.`kodeobat`,b.`namaobat`,(a.`harga`*a.`jmlobat`) harga,a.`jmlobat`,a.`idtranresep`
                                FROM tranresep a INNER JOIN mstobat b ON a.`idobat` = b.`idobat`	
                                WHERE a.`idregistrasi` = '$idregistrasi' and a.datastat <> 'DELETED' ORDER BY a.`tglresep` ASC";
                    //echo $query3;
                    $result3=  mysql_query($query3);
                    while($row=mysql_fetch_array($result3)){
                       echo "<tr>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[0]</td>";
                       echo "<td style=\"text-align: left\">$row[1]</td>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[2]</td>";
                       echo "<td style=\"width: 25%;text-align: left\">$row[3]</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-default\" href=\"javascript:void(0);\" onclick=\"batalresep('$row[4]','$row[0]')\">Batal</a>
                            </td>";
                       echo "</tr>";
                    }
                ?>
            </tbody>                          
        </table>    
    </div>
</div> 

<!--Resep Area End-->



<!--Upload Foto Area Start-->
<center>
    <h3 style="border-bottom: solid #D1D1D1 1px;padding-bottom: 5px;">
        Upload Foto
    </h3>
</center>    
<iframe name="upload-frame" id="upload-frame" style="display:none;"></iframe>  
<form class="form-inline" role="form" name="uploadfotopasien" id="uploadfotopasien" action="javascript:simpanfoto();" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <input id="fileupload" name="fileupload" type="file">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" id="txtKeteranganFoto" name="txtKeteranganFoto" placeholder="Keterangan">
    </div>
    <button type="submit" onclick="javascript:simpanfoto();" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>&nbsp;Upload</button>
</form>
<div style="padding: 10px 0px;">
    <div id="namaobat"><label class="label label-info msg">Status</label></div>
</div>
<div style="height:230px;overflow-x: hidden;overflow-y: auto;border: solid 1px #D1D1D1;padding: 2px 2px;margin-bottom: 10px;">
    <div id="tblfoto">
        <table class="table table-striped table-bordered table-hover" id="tbresep">
            <thead>
                <tr class="danger">
                    <th style="text-align: center;">Foto</th>
                    <th style="text-align: center;">Catatan</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>                  
        </table>    
    </div>
</div> 

<!--Upload Foto End-->

<form  role="form" name="frmDiagnosaPasien" id="frmDiagnosaPasien" action="<?php echo base_url().'trantindakan/submitpasien'; ?>" method="post">
<?php
$querydiagnosis = "SELECT `idtrandiagnosis`, 
                        `diagnosis`,
                        `catatan`,
                        anamnesis,
                        pemeriksaanfisik
                      FROM `trandiagnosis`
                      WHERE `idregistrasi` = '$idregistrasi'";
$resultdiagnosis=  mysql_query($querydiagnosis);
$xrow=mysql_fetch_array($resultdiagnosis);
$idtrandiagnosis=$xrow[0];
$diagnosis=$xrow[1];
$catatan=$xrow[2];
$anamnesis = $xrow[3];
$pemeriksaanfisik = $xrow[4];
?>
<input type="hidden" class="form-control" id="idregistrasi" name="idregistrasi" value="<?php echo $rowpasien[0];?>">            
<input type="hidden" class="form-control" id="idpasien" name="idpasien" value="<?php echo $rowpasien[5];?>">   
<input type="hidden" class="form-control" id="edits" name="edits" value="<?php echo $this->uri->segment(4);?>"> 
<center>
    <h3 style="border-bottom: solid #D1D1D1 1px;padding-bottom: 5px;">
        Anamnesis dan Diagnosa Pasien
    </h3>
</center>    
<div class="form-group">
    <label>Anamnesis</label>
    <textarea style="resize: none;" class="form-control" id="anamnesis" name="anamnesis" placeholder="Anamnesis Pasien"><?php echo $anamnesis;?></textarea>
</div>
<div class="form-group">
    <label>Pemeriksaan Fisik</label>
    <textarea style="resize: none;" class="form-control" id="pemeriksaanfisik" name="pemeriksaanfisik" placeholder="Pemeriksaan Fisik Pasien"><?php echo $pemeriksaanfisik;?></textarea>
</div>
<div class="form-group">
    <label>Diagnosis</label>
    <input type="hidden" class="form-control" id="idtrandiagnosis" name="idtrandiagnosis" value="<?php echo $idtrandiagnosis;?>">
    <textarea style="resize: none;" class="form-control" id="diagnosis" name="diagnosis" placeholder="Diagnosa masalah"><?php echo $diagnosis;?></textarea>
</div>
<div class="form-group">
    <label>Catatan Kepada Pasien</label>
    <textarea style="resize: none;" class="form-control" id="catatan" name="catatan" placeholder="Catatan kepada pasien"><?php echo $catatan;?></textarea>
</div>
<div style="margin-bottom: 50px;">
    <button type="submit" class="btn btn-danger">
        Proses Data Pasien
    </button>
    <?php 
        $edit = $this->uri->segment(4);
        if(!empty($edit) || $edit !=""){
    ?>
        <a type="button" class="btn btn-default" href="<?php echo base_url()."trantindakanedit"; ?>">
            Batal
        </a>    
    <?php
        }else{
    ?>
        <a type="button" class="btn btn-default" href="<?php echo base_url()."trantindakan"; ?>">
            Batal
        </a>    
    <?php
        }
    ?>
</div>    
</form>

<!--Modal Tindakan area start                -->
<div class="modal fade" id="myModalTindakan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Tindakan</h4>
      </div>
      <div class="modal-body">
          <div style="min-height: 400px;min-width: 500px;">
              <div id="dvTindakanPasien"></div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal area end-->

<!--Modal Obat area start                -->
<div class="modal fade" id="myModalObat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Obat</h4>
      </div>
      <div class="modal-body">
        <div style="min-height: 400px;min-width: 500px;">
            <div id="dvMstObat"></div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal area end-->



<!--Modal Obat area start                -->
<div class="modal fade" id="myModalFoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Foto Pasien</h4>
      </div>
      <div class="modal-body">
              <div id="fotopasien"></div>
      </div>  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>    
<!--Modal area end-->
            </div>
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
    
<!--Modal area start                -->
<div class="modal fade" id="myRMPasien" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: auto;margin: 10px 10px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Rekam Medis Pasien</h4>
      </div>
      <div class="modal-body"> 
            <div id="tbRMPasien"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal area end-->
  </body>
</html>
