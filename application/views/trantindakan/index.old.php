<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->   
    <script type="text/javascript">      
            function add_pasien(kdpasien){
                var xkdpasien = document.getElementById('kdpasien');
                    xkdpasien.value = kdpasien;
                    getpasien();
            }
            
            function clsobat(){
                $('#kodeobat').val('');
                $('#idobat').val('');
                $('#jumlahobat').val('');
                $('#hargaobat').val('');
                $("#namaobat").html("<label class=\"label label-info\">Nama obat : - </label>");
            }
            
            function clstindakan(){
                $('#tindakanid').val('');
                $('#kodetindakan').val('');
                $('#hargatindakan').val('');
                $('#keterangan').val('');
                $("#nmtindakan").html("<label class=\"label label-info\">Nama Tindakan : - </label>");
            }
            
            function add_tindakan(id,kode,nama,tarif){
                var v1 = document.getElementById('tindakanid');
                    v1.value = id;
                var v2 = document.getElementById('kodetindakan');
                    v2.value = kode;
                $("#nmtindakan").html("<label class=\"label label-info\">Nama tindakan : " + nama + "</label>");
                var v4 = document.getElementById('hargatindakan');
                    v4.value = tarif;
            }
            
            function add_resep(idobat, kodeobat, namaobat, sisaobat, hargajual){
                var v1 = document.getElementById('idobat');
                    v1.value = idobat;
                var v2 = document.getElementById('kodeobat');
                    v2.value = kodeobat;
                $("#namaobat").html("<label class=\"label label-info\">Nama obat : " + namaobat + "</label>");
                var v3 = document.getElementById('sisaobat');
                    v3.value = sisaobat;
                var v4 = document.getElementById('hargaobat');
                    v4.value = hargajual;
            }
            
            function getpasien(){
                var kdpasien=$('#kdpasien').val();
                $("#divpasien").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/getpasien'; ?>",
                    data:{"kdpasien":kdpasien},
                    success: function(resp){                            
                            $("#divpasien").html(resp);
                            $('#kdpasien').focus();
                            var idpasien=$('#idpasien').val();
                            if(idpasien!==undefined){
                                getformtindakan();
                            }
                    }
                });
            };
            
            function getformtindakan(){
                var idregistrasi=$('#idregistrasi').val();
                $("#divdatapasien").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/formtrantindakan'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){                            
                            $("#divdatapasien").html(resp);
                            gettabelresep();
                            gettabeltindakan();
                            gettblfoto();
                    }
                });
            };
            
            function gettabelresep(){
                var idregistrasi=$('#idregistrasi').val();
                $("#tblresep").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/gettabelresep'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){                            
                            $("#tblresep").html(resp);
                    }
                });
            };
            
            function gettabeltindakan(){
                var idregistrasi=$('#idregistrasi').val();
                $("#tabeltindakan").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/gettabeltindakan'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){                            
                            $("#tabeltindakan").html(resp);
                    }
                });
            };
            
            function gettbantrianpasien(){
                $("#tbantrianpasien").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/getantrianpasien'; ?>",
                    success: function(resp){                            
                            $("#tbantrianpasien").html(resp);
                    }
                });
            }
            
            function gettblfoto(){
                var idregistrasi=$('#idregistrasi').val();  
                $("#tblfoto").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/gettblfoto'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){                            
                            $("#tblfoto").html(resp);
                            var v1 = document.getElementById('fileupload');
                                v1.value = "";
                            var v2 = document.getElementById('txtKeteranganFoto');
                                v2.value = "";
                    }
                });
            }
            
            function getfotopasien(idtrangambar){
                $("#fotopasien").html("<h3><label class=\"label label-info\">Mengambil gambar ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/modalfoto'; ?>",
                    data:{"idtrangambar":idtrangambar},
                    success: function(resp){                            
                            $("#fotopasien").html(resp);
                    }
                });
            }
            
            function hapusfoto(idtrangambar,namafoto){
                var r=confirm("Lanjutkan menghapus data '"+namafoto+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'trantindakan/hapusfoto'; ?>",
                            data: {"idtrangambar":idtrangambar,
                                    "stat":"hapus"},
                            success: function(resp) {
                                if(resp=='1'){
                                   $('.msg').html('Berhasil dihapus');
                                   gettblfoto();
                                }else{
                                   $('.msg').html('Gagal dihapus');
                                   return false;
                                }
                            },
                            error: function(resp){
                                $('.msg').html('Gagal dihapus');
                                  return false;
                            }
                        });
                  }
            };
            
            function bataltindakan(idtrantindakan,kodetindakan){
                var r=confirm("Lanjutkan menghapus data '"+kodetindakan+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'trantindakan/prosesdata'; ?>",
                            data: {"idtrantindakan":idtrantindakan,
                                    "stat":"hapus"},
                            success: function(resp){
                                    if(resp=='1'){
                                        //alert("Data "+kodetindakan+" Berhasil diproses!");
                                        gettabeltindakan();
                                    }else{
                                        alert("Data "+kodetindakan+" Gagal diproses! : Error : "+resp);
                                        return false;
                           }
                            }
                        });
                  }
            };
            
            function batalresep(idtranresep,kodeobat){
                var r=confirm("Lanjutkan menghapus data '"+kodeobat+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'trantindakan/prosesresep'; ?>",
                            data: {"idtranresep":idtranresep,"jumlahobat":"0",
                                    "hargaobat":"0","stat":"hapus"},
                            success: function(resp){
                                    if(resp=='1'){
                                        //alert("Data "+kodeobat+" Berhasil diproses!");
                                        gettabelresep();
                                        clsobat();
                                        $('#kodeobat').focus();
                                    }else{
                                        alert("Data "+kodeobat+" Gagal diproses! : Error : "+resp);
                                        return false;
                           }
                            }
                        });
                  }
            };
            
            function simpantindakan(){
                var idregistrasi=$('#idregistrasi').val();
                var tindakanid=$('#tindakanid').val();
                var hargatindakan=$('#hargatindakan').val();
                var keterangan=$('#keterangan').val();
                var idpasien=$('#idpasien').val();
                var kodetindakan=$('#kodetindakan').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/prosesdata'; ?>",
                    data:{"idregistrasi":idregistrasi,"tindakanid":tindakanid,"hargatindakan":hargatindakan,
                            "keterangan":keterangan,"idpasien":idpasien},
                    success: function(resp){                            
                            if(resp=='1'){
                               //alert("Data "+kdpasien+" Berhasil diproses!");
                               gettabeltindakan();
                               clstindakan();
                               $('#kodetindakan').focus();
                           }else{
                               alert("Data "+kodetindakan+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                    }
                });
            };
            
            function simpanresep(){
                var idregistrasi=$('#idregistrasi').val();
                var kodeobat=$('#kodeobat').val();
                var idobat=$('#idobat').val();
                var jumlahobat=$('#jumlahobat').val();
                var hargaobat=$('#hargaobat').val();
                var idpasien=$('#idpasien').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/prosesresep'; ?>",
                    data:{"idregistrasi":idregistrasi,"idobat":idobat,"jumlahobat":jumlahobat,
                            "hargaobat":hargaobat,"idpasien":idpasien},
                    success: function(resp){                            
                            if(resp=='1'){
                               //alert("Data "+kdpasien+" Berhasil diproses!");
                               gettabelresep();
                               clsobat();
                               $('#kodeobat').focus();
                           }else{
                               alert("Data "+kodeobat+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                    }
                });
            };
            
            function simpanfoto(){
                var idregistrasi=$('#idregistrasi').val();
                var txtKeteranganFoto=$('#txtKeteranganFoto').val();
                var idpasien=$('#idpasien').val();
                var fileupload=$('#fileupload').val();
                
                $('#uploadfotopasien').ajaxForm({
                 url:'<?php echo base_url().'trantindakan/uploadfoto'; ?>',
                 type: 'post',
                 data:{"idregistrasi":idregistrasi,"txtKeteranganFoto":txtKeteranganFoto
                            ,"idpasien":idpasien,"fileupload":fileupload},
                 //resetForm: true,
                 beforeSubmit: function() {
                  $('.msg').html('Silahkan Tunggu ... ');
                 },
                 success: function(resp) {
                   if(resp=='1'){
                       $('.msg').html('Berhasil di simpan');
                       gettblfoto();
                   }else{
                       $('.msg').html('Gagal di simpan');
                       return false;
                    }
                 },
                 error: function(resp){
                     $('.msg').html('Gagal di simpan');
                       return false;
                 }
                });     
            };
            
            function submitpasien(){
                var idtrandiagnosis=$('#idtrandiagnosis').val();
                var idregistrasi=$('#idregistrasi').val();
                var idpasien=$('#idpasien').val();
                var kdpasien=$('#kdpasien').val();
                var diagnosis=$('#diagnosis').val();
                var catatan=$('#catatan').val();
                var anamnesis=$('#anamnesis').val();
                //alert(idregistrasi);
                if(idregistrasi=="" || idregistrasi==undefined){
                    alert("Pasien tidak ada, silahkan cek data pasien!");
                    return false;
                }
                
                if(anamnesis==""){
                    alert("Anamnesis harus diisi!");
                    return false;
                }
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/submitpasien'; ?>",
                    data:{"idregistrasi":idregistrasi,"idpasien":idpasien,"diagnosis":diagnosis,
                            "catatan":catatan,"idtrandiagnosis":idtrandiagnosis,"anamnesis":anamnesis},
                    success: function(resp){                            
                            if(resp=='1'){
                               alert("Data "+kdpasien+" Berhasil diproses!");
                               location.reload();
                           }else{
                               alert("Data "+kdpasien+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                    }
                });
            };
            
            function tampil_riwayat(){
                var kdpasien=$('#kdpasien').val();
                $("#tbRMPasien").html("<h3><label class=\"label label-info\">Silahkan Tunggu ...</label></h3>");
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'trantindakan/getriwayatpasien'; ?>",
                    data:{"kdpasien":kdpasien},
                    success: function(resp){                            
                            $("#tbRMPasien").html(resp);
                    }
                });
            };
    </script>
  </head>

  <body onload="getpasien();">
    <div id="wrapper">
        <?php
            $this->load->view('others/nav');
        ?>
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        <div class="row">
            <div class="col-lg-4">
                <form role="form" action="javascript:getpasien();">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" class="form-control" id="kdpasien" placeholder="Masukkan Kode Pasien">
                            <div class="input-group-btn">
                                <button onclick="gettbantrianpasien();" type="button" class="btn btn-info" data-backdrop="static" data-toggle="modal" data-target="#myModal">Cari</button>
                            </div>
                        </div>                
                    </div>
                </form>                 
                <div id="divpasien"></div>
            </div>
            <div class="col-lg-8">
                <div id="divdatapasien"></div>
            </div>
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->

<!--Modal area start                -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Pasien</h4>
      </div>
      <div class="modal-body">   
          <div id="tbantrianpasien"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal area end-->

<!--Modal area start                -->
<div class="modal fade" id="myRMPasien" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: auto;margin: 10px 10px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Rekam Medis Pasien</h4>
      </div>
      <div class="modal-body"> 
            <div id="tbRMPasien"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal area end-->
  </body>
</html>
