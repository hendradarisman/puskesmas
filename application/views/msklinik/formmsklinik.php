<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo $this->owner->favicon; ?>
    
    <title><?php echo $this->apps->name; ?></title>
    <?php
    error_reporting(0);
        $this->load->view('others/css');
        $this->load->view('others/js');
        $proses = $this->session->userdata('proses');
        $this->session->unset_userdata('proses');
    ?>
    <style type="text/css">
        body{
            background-color: #012E5F;
        }
    </style>
</head>

<body style="min-width: 200px;">
    <form role="form" method="post" action="<?php echo base_url();?>msklinik/prosesdata" enctype="multipart/form-data">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="login-panel panel panel-default" style="margin-top: 2%;">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <span class="glyphicon glyphicon-lock"></span>
                                <?php echo $this->apps->name; ?>
                            </h3>
                        </div>
                        <div class="panel-body">
                            <?php
                                if($proses=="ok"){
                            ?>
                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <span class="glyphicon glyphicon-ok-circle"></span>
                                    Selamat, Data berhasil disimpan. Silahkan tekan kembali untuk menuju halaman utama.
                                </div>
                            <?php
                                }elseif($proses=="gagal"){
                            ?>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <span class="glyphicon glyphicon-exclamation-sign"></span>
                                    Maaf, Data gagal disimpan. Silahkan isian anda.
                                </div>
                            <?php
                                }else{
                            ?>
                                <div class="alert alert-danger">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    <span class="glyphicon glyphicon-exclamation-sign"></span>
                                    Silahkan isikan identitas klinik dengan benar!
                                </div>                            
                            <?php
                                }
                            ?>
                            
    <?php
            $query = $this->db->get("mstklinik");
            if($query->num_rows>0)
            {          
                $row = $query->row();
                $nama = $row->nama;
                $noijin = $row->noijin;
                $alamat = $row->alamat;
                $telp = $row->telp;
                $fax = $row->fax;            
                $email = $row->email;
                $pjklinik = $row->pjklinik;
                $pjlab = $row->pjlab;
                $kota = $row->kota;
                $website = $row->website;
                $logo = $row->logo;
            }
    ?>
                            <div class="col-lg-4">        
                                    <div class="form-group">
                                        <label for="nama">Nama Klinik </label>
                                        <input name="nama" maxlength="1000" type="text" class="form-control" id="nama" placeholder="Masukkan Nama Klinik" value="<?php echo $nama;?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="noijin">No. Ijin Praktek </label>
                                        <input name="noijin" maxlength="500" type="text" class="form-control" id="noijin" placeholder="Masukkan No. Ijin Praktek" value="<?php echo $noijin;?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="alamat">Alamat </label>
                                        <textarea name="alamat" maxlength="1000" style="resize: vertical;min-height: 104px;" class="form-control" id="alamat" placeholder="Alamat"><?php echo $alamat;?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="kota">Kota <small>(optional)</small></label>
                                        <input name="kota" maxlength="200" type="text" class="form-control" id="kota" placeholder="Masukkan Nama Kota" value="<?php echo $kota;?>">
                                    </div>
                            </div>        

                            <div class="col-lg-4">
                                    <div class="form-group">
                                        <label for="website">Website <small>(optional)</small></label>
                                        <input name="website" maxlength="200" type="text" class="form-control" id="website" placeholder="Masukkan Website" value="<?php echo $website;?>">
                                    </div>        

                                    <div class="form-group">
                                        <label for="telp">No. Telp </label>
                                        <input name="telp" maxlength="20" type="text" class="form-control" id="telp" placeholder="Masukkan No. Telp" value="<?php echo $telp;?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="fax">Fax <small>(optional)</small></label>
                                        <input name="fax" maxlength="20" type="text" class="form-control" id="fax" placeholder="Masukkan No. FAX" value="<?php echo $fax;?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="email">Email <small>(optional)</small></label>
                                        <input name="email" maxlength="1000" type="text" class="form-control" id="email" placeholder="Masukkan Email" value="<?php echo $email;?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="pjklinik">Penanggung Jawab Klinik </label>
                                        <input name="pjklinik" maxlength="100" type="text" class="form-control" id="pjklinik" placeholder="Masukkan Penanggung Jawab Klinik" value="<?php echo $pjklinik;?>">
                                    </div>

                                    <div class="form-group">
                                        <label for="pjlab">Penanggung Jawab Lab. </label>
                                        <input name="pjlab" maxlength="100" type="text" class="form-control" id="pjlab" placeholder="Masukkan Penanggung Jawab Lab." value="<?php echo $pjlab;?>">
                                    </div>

                                    <button type="submit" class="btn btn-primary">
                                        Simpan
                                    </button>
                                    <a class="btn btn-default" href="<?=base_url() ?>">
                                        Kembali
                                    </a>            
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <?php
                                        if(empty($logo) || $logo ==""){
                                    ?>
                                            <img class="img-rounded" src="<?=base_url() ?>assets/images/nologo.png" />
                                    <?php
                                        }else{
                                    ?>
                                            <img class="img-rounded" width="350" height="350" src="<?php echo base_url().$logo; ?>" />
                                    <?php
                                        }
                                    ?>
                                    <label><small>*Resolusi terbaik panjang 350px lebar 350px</small></label>
                                    <input type="file" name="logoklinik" id="logoklinik" />
                                </div>
                            </div>                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>    
</body>

</html>