<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>

    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->
    <script type="text/javascript">
            var userstat = 0;
            function tampil_grid(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'mspasien/gridmspasien'; ?>",
                    success: function(resp){   
                            $("#konten").html(resp);
                    }
                });
            };
            
            function tampil_form(idpasien,kelamin){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'mspasien/formmspasien'; ?>",
                    data: {"idpasien":idpasien},
                    success: function(resp){                            
                            $("#konten").html(resp);
                            var xcmbkelamin = document.getElementById('cmbkelamin');
                                xcmbkelamin.value = kelamin;
                    }
                });
            };
                       
            function simpandata(){
                var idpasien=$('#idpasien').val();
                var namalengkap=$('#namalengkap').val();
                var cmbkelamin=$('#cmbkelamin').val();
                var tmplahir=$('#tmplahir').val();
                var tgllahir=$('#tgllahir').val();
                var alamat=$('#alamat').val();
                
                var telp=$('#telp').val();
                var hp=$('#hp').val();
                var kerabatnama=$('#kerabatnama').val();
                var kerabatstatus=$('#kerabatstatus').val();
                var kerabattelp=$('#kerabattelp').val();
                $.ajax({
                   type: "POST",
                   url: "<?php echo base_url().'mspasien/prosesdata'; ?>",
                   data: {"idpasien":idpasien,"namalengkap":namalengkap,"cmbkelamin":cmbkelamin
                           ,"tmplahir":tmplahir,"tgllahir":tgllahir,"alamat":alamat
                           ,"telp":telp,"hp":hp,"kerabatnama":kerabatnama,"kerabatstatus":kerabatstatus,"kerabattelp":kerabattelp},
                   success: function(resp){
                           if(resp=='1'){
                               alert("Data "+namalengkap+" Berhasil diproses!");
                               tampil_grid();
                           }else{
                               alert("Data "+namalengkap+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                   }
               });   
            };
            
            function hapusdata(idpasien,namalengkap){
                var r=confirm("Lanjutkan menghapus data '"+namalengkap+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'mspasien/prosesdata'; ?>",
                            data: {"idpasien":idpasien,
                                    "stat":"hapus"},
                            success: function(resp){
                                    alert(resp);
                                    tampil_grid();
                            }
                        });
                  }
            };
            
        function getkartupasien(url) {
            window.open(url, "MsgWindow", "width=370, height=230");
            //window.open(url);
            //var myWindow = window.open(url, "MsgWindow", "width=200, height=100");
            //myWindow.document.write("<p>This is 'MsgWindow'. I am 200px wide and 100px tall!</p>");
        }
</script>
  </head>

  <body onload="tampil_grid();">

    <div id="wrapper">

        <?php
            $this->load->view('others/nav');
        ?>

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        
        <div class="row">
            <div id="konten"></div>
            <?php
                if($this->uri->segment(3)){
                    ?>
            <div class="col-lg-12">
                        <a target="_self" href="<?=base_url() ?><?php echo $this->uri->segment(3);?>" 
                            class="btn btn-danger">
                            <span class="glyphicon glyphicon-circle-arrow-left"></span>
                            &nbsp;
                            Kembali ke <?php echo $this->apps->modulsource($this->uri->segment(3)); ?>
                        </a>            
                </div>
                    <?php
                }
            ?>
        </div><!-- /.row -->
        
      </div><!-- /#page-wrapper -->
      
    </div><!-- /#wrapper -->
  </body>
</html>
