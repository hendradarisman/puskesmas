<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->
    <script type="text/javascript">
            var userstat = 0;
            function tampil_grid(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'msadmin/gridmsadmin'; ?>",
                    success: function(resp){   
                            $("#konten").html(resp);
                    }
                });
            };
            
            function tampil_form(idadmin,kelamin){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'msadmin/formmsadmin'; ?>",
                    data: {"idadmin":idadmin},
                    success: function(resp){                            
                            $("#konten").html(resp);
                            var xcmbkelamin = document.getElementById('cmbkelamin');
                                xcmbkelamin.value = kelamin;
                    }
                });
            };
                       
            function simpandata(){
                var idadmin=$('#idadmin').val();
                var namalengkap=$('#namalengkap').val();
                var cmbkelamin=$('#cmbkelamin').val();
                var alamat=$('#alamat').val();
                
                var telp=$('#telp').val();
                var hp=$('#hp').val();
                var email=$('#email').val();
                $.ajax({
                   type: "POST",
                   url: "<?php echo base_url().'msadmin/prosesdata'; ?>",
                   data: {"idadmin":idadmin,"namalengkap":namalengkap,"cmbkelamin":cmbkelamin
                           ,"alamat":alamat,"telp":telp,"hp":hp,"email":email},
                   success: function(resp){
                           if(resp=='1'){
                               alert("Data "+namalengkap+" Berhasil diproses!");
                               tampil_grid();
                           }else{
                               alert("Data "+namalengkap+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                   }
               });   
            };
            
            function hapusdata(idadmin,namalengkap){
                var r=confirm("Lanjutkan menghapus data '"+namalengkap+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'msadmin/prosesdata'; ?>",
                            data: {"idadmin":idadmin,
                                    "stat":"hapus"},
                            success: function(resp){
                                    alert(resp);
                                    tampil_grid();
                            }
                        });
                  }
            };
    </script>
  </head>

  <body onload="tampil_grid();">

    <div id="wrapper">

        <?php
            $this->load->view('others/nav');
        ?>

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        
        <div class="row">
            <div id="konten"></div>
        </div><!-- /.row -->
        
      </div><!-- /#page-wrapper -->
      
    </div><!-- /#wrapper -->
  </body>
</html>
