<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
<?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
      
    ?>
      <!-- <body onload="tampil_grid();"> -->
  

<div id="wrapper">
<?php
    $this->load->view('others/nav');
?>
<div id="page-wrapper">
<div class="row">
  <div class="col-lg-12">
        <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
            <h1>
                <?php 
                    echo $this->apps->modulname();
                ?>                    
            </h1>
        </div>    
  </div>
</div><!-- /.row -->
    
  <!-- Main content -->
  <!-- <section class="content">
    <div class="box box-primary">
      <div class="box-body"> -->
        <!-- <a data-toggle="modal" href='#tambah' class="btn btn-primary btn-flat"><i class="fa fa-plus"></i> Tambah Data</a> -->
        <hr class="garis"></hr>
        <table class="table table-bordered table-condensed" id="example1">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama Obat</th>
              <th>Jumlah Obat</th>
              <th>Nama Pasien</th>
              <th>Keterangan Pakai</th>
              <th>Tanggal Resep</th>
            </tr> 
          </thead>
          <tbody>
            <?php $i=1; foreach ($resep as $dt): ?>
            <tr>
              <td><?=$i?></td>
              <td><?=$dt->namaobat?></td>
              <td><?=$dt->jmlobat?></td>
              <td><?=$dt->namalengkap?></td>
              <td><?=$dt->keteranganresep?></td>
              <td><?=$dt->tglresep?></td>
            
            </tr>
            <?php $i++; endforeach ?>
          </tbody>
         
        </table>
      </div>
    </div>
  <!-- </section>/.content -->
  </div>
  </div>
  </div>

</div><!-- /.content-wrapper -->

<div class="modal fade" id="tambah">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Tambah Data</h4>
      </div>
      <form action="<?=base_url('Rujukan/rujukan_add')?>" method="post">
        <div class="modal-body">
          <div class="form-group">
            <label>Nama RS / DR</label>
            <input type="text" name="nama_rs_dr" value="" placeholder="" class="form-control">
          </div>
          <div class="form-group">
            <label>Alamat</label>
            <textarea name="alamat" class="form-control"></textarea>
          </div>
          <div class="form-group">
            <label>No Telepon</label>
            <input type="number" name="no_telp" value="" placeholder="" class="form-control">
          </div>
          <div class="form-group">
            <label>Email</label>
            <input type="text" name="email" value="" placeholder="" class="form-control">
          </div>
          <div class="form-group">
            <label>Keterangan</label>
            <textarea name="keterangan" class="form-control"></textarea>
          </div>
          <div class="form-group">
            <label>Status</label>
            <select name="is_aktif" class="form-control">
              <option value="1">Aktif</option>
              <option value="2">Non Aktif</option>
            </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
