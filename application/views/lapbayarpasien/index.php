<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>

    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <script type="text/javascript">
        checked = false;
        function checkedAll () {
            if (checked == false){checked = true}else{checked = false}
            for (var i = 0; i < document.getElementById('frmlapbayar').elements.length; i++) {
                document.getElementById('frmlapbayar').elements[i].checked = checked;
            }
        }
    </script>
  </head>

  <body>

    <div id="wrapper">

        <?php
            $this->load->view('others/nav');
        ?>

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        
        <div class="row">
            <div class="col-lg-3">
                <form name="frmlapbayar" id="frmlapbayar" method="post" action="<?=base_url() ?>lapbayarpasien/export">
                    <div class="form-group">
                        <label for="chkdokter">Pilih dokter </label>
                        <div style="height: 150px; overflow-y: auto; border: 1px solid #dadada;" id="chkdokter">
                            <div style="margin: 5px 5px;">
                                <?php
                                    $query = "SELECT iddokter,namalengkap FROM mstdokter order by namalengkap asc";
                                    $result=  mysql_query($query);
                                    $idk=1;
                                    while($row=  mysql_fetch_array($result)){
                                ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" id="dk<?php echo $idk; ?>" name="check_list[]" value="<?php echo $row[0]; ?>" />
                                            <?php echo $row[1]; ?>
                                    </label>
                                </div>
                                <?php
                                        $idk++;
                                    }
                                ?>
                            </div>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type='checkbox' name='checkall' onclick='checkedAll();'> Pilih semua
                            </label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="tglawal">Periode Awal <small>(YYYY-MM-DD)</small></label>
                        <input type="text" class="form-control" id="tglawal" name="tglawal" data-date-format="YYYY-MM-DD" placeholder="Masukkan Tanggal" value="<?php echo date('Y-m-d');?>"/>
                        <script type="text/javascript">
                            $(function () {
                                  $('#tglawal').datetimepicker();
                              });
                        </script>
                    </div>
                    
                    <div class="form-group">
                        <label for="tglakhir">Periode Akhir <small>(YYYY-MM-DD)</small></label>
                        <input type="text" class="form-control" id="tglakhir" name="tglakhir" data-date-format="YYYY-MM-DD" placeholder="Masukkan Tanggal" value="<?php echo date('Y-m-d');?>"/>
                        <script type="text/javascript">
                            $(function () {
                                  $('#tglakhir').datetimepicker();
                              });
                        </script>
                    </div>

                    <div class="radio">
                        <label>
                        <input type="radio" name="optCetak" id="optCetak" value="excel">
                            Cetak ke Excel
                        </label>
                    </div>
                    
                    <div class="radio">
                        <label>
                        <input type="radio" name="optCetak" id="optCetak" value="pdf">
                            Cetak ke Pdf
                        </label>
                    </div>
                    
                    
                    <button type="submit" class="btn btn-success">
                        Export Laporan
                    </button>
                </form>
            </div>
        </div><!-- /.row -->
        
      </div><!-- /#page-wrapper -->
      
    </div><!-- /#wrapper -->
  </body>
</html>
