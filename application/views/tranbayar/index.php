<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->
    <script type="text/javascript">      
            function tampil_grid(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'tranbayar/gridtranbayar'; ?>",
                    success: function(resp){   
                            $("#konten").html(resp);
                    }
                });
            };
            
            function getdetailtagihan(idregistrasi){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'tranbayar/getdetailtagihan'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){   
                            $("#bayarpasien").html(resp);
                    }
                });
                $("#pasienbayar").focus();
            };
            
            function datatranbayar(idregistrasi){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'tranbayar/datatranbayar'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){   
                            $("#bayarpasien").html(resp);
                    }
                });
            };
            
            function hitungselisih(tagihan){
                var bayar = $("#pasienbayar").val();
                var selisih = 0;
                selisih = bayar - tagihan;
                if(bayar=="" || bayar==0){
                    $("#kembali").val("Rp. -");
                }else{
                    $("#kembali").val("Rp. " + selisih);
                }
            }
            
            function submitpembayaran(idregistrasi){
                var jmlbayar =  $("#pasienbayar").val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'tranbayar/submitpembayaran'; ?>",
                    data:{"idregistrasi":idregistrasi,"jmlbayar":jmlbayar},
                    success: function(resp){   
                            if(resp=='1'){
                                //alert("Data Berhasil diproses!");
                                tampil_grid();
                                var url = "<?php echo base_url().'tranbayar/cetaknota/';?>"+idregistrasi;
                                window.open(url, "MsgWindow", "width=370, height=600");
                           }else{
                               alert("Data Gagal diproses! : Error : "+resp);
                               return false;
                           }
                    }
                });   
            }
            
            function cetaknota(url) {
            window.open(url, "MsgWindow", "width=370, height=600");
        }
    </script>
  </head>

  <body onload="tampil_grid();">
    <div id="wrapper">
        <?php
            $this->load->view('others/nav');
        ?>
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        <div class="row">
            <div id="konten"></div>
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
    
  </body>
</html>
