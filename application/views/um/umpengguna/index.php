<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->
    <script type="text/javascript">
            var userstat = 0;
            var globIdpengguna="";
            function tampil_grid(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'umpengguna/gridumpengguna'; ?>",
                    success: function(resp){   
                            $("#kontenpengguna").html(resp);
                    }
                });
            };
            
            function tampil_form(idpengguna){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'umpengguna/formumpengguna'; ?>",
                    data: {"idpengguna":idpengguna},
                    success: function(resp){                            
                            $("#kontenpengguna").html(resp);
                            globIdpengguna = idpengguna;
                    }
                });
            };
            
            function add_pengguna(id,nama){
                var vidperson = document.getElementById("idperson");
                    vidperson.value = id;
                var vnamalengkap = document.getElementById("namalengkap");
                    vnamalengkap.value = nama;
            }
            
            function simpandata(){
                var idpengguna=$('#idpengguna').val();
                var idperson=$('#idperson').val();
                var namapengguna=$('#namapengguna').val();
                var katakunci=$('#katakunci').val();
                var cmbgruppengguna=$('#cmbgruppengguna').val();
                if(idperson==""){
                    alert("Pengguna belum dipilih");
                    return false;
                }
                if(namapengguna==""){
                    alert("Nama Pengguna belum diisi");
                    return false;
                }
                if(katakunci==""){
                    alert("Password belum diisi");
                    return false;
                }
                if(cmbgruppengguna==""){
                    alert("Grup pengguna belum diisi");
                    return false;
                }
                if(userstat==0 && idpengguna==globIdpengguna){
                    alert("Nama Pengguna '"+namapengguna+"' tidak boleh sama");
                    return false;
                }
                $.ajax({
                   type: "POST",
                   url: "<?php echo base_url().'umpengguna/prosesdata'; ?>",
                   data: {"idpengguna":idpengguna,"idperson":idperson
                           ,"namapengguna":namapengguna,"katakunci":katakunci,"cmbgruppengguna":cmbgruppengguna},
                   success: function(resp){
                           if(resp=='1'){
                               alert("Data "+namapengguna+" Berhasil diproses!");
                               tampil_grid();
                           }else{
                               alert("Data "+namapengguna+" Gagal diproses!");
                               return false;
                           }
                   }
               });   
            };
            
            function hapusdata(idpengguna,namapengguna){
                var r=confirm("Lanjutkan menghapus data '"+namapengguna+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'umpengguna/prosesdata'; ?>",
                            data: {"idpengguna":idpengguna,
                                    "stat":"hapus"},
                            success: function(resp){
                                    if(resp=='1'){
                                        alert("Data "+namapengguna+" Berhasil diproses!");
                                        tampil_grid();
                                    }else{
                                        alert("Data "+namapengguna+" Gagal diproses!");
                                        tampil_grid();
                                    }
                            }
                        });
                  }
            };
    </script>
  </head>

  <body onload="tampil_grid();">

    <div id="wrapper">

        <?php
            $this->load->view('others/nav');
        ?>

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        
        <div class="row">
            <div id="kontenpengguna"></div>
        </div><!-- /.row -->
        
      </div><!-- /#page-wrapper -->
      
    </div><!-- /#wrapper -->
  </body>
</html>
