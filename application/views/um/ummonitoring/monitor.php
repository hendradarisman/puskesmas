<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
      $this->load->view('others/jqwidgets');
    ?>
    
    <!-- Page Specific CSS -->
    <script type="text/javascript">
$(document).ready(function () {	
    var source =
    {
        datatype: "json",
        datafields: [
            { name: 'namapengguna'},
            { name: 'aktifitas'},
            { name: 'komputerip'},
            { name: 'browsertipe'},
            { name: 'perangkattipe'}
        ],
        url: '<?php echo base_url()."ummonitoring/dataummonitoring" ?>',

        filter: function()
        {
            $("#gridmonitor").jqxGrid('updatebounddata', 'filter');
        },
        cache: false
    };		
    var dataAdapter = new $.jqx.dataAdapter(source);

    $("#gridmonitor").jqxGrid(
    {		
        source: dataAdapter,
        width: '100%',
        theme: globTheme,			
        showfilterrow: true,
        filterable: true,
        sortable: true,
        height: 480,
        autorowheight: true,
        autoheight: true,
        pageable: true,
        columnsresize: true,	
        groupable: true,
        altrows: true,	
        columns: [
        { text: 'Pengguna', datafield: 'namapengguna', width: '20%'},
        { text: 'Aktifitas', datafield: 'aktifitas'},
        { text: 'IP komputer', datafield: 'komputerip', width: '10%', cellsalign: 'center', align: 'center' },
        { text: 'Browser', datafield: 'browsertipe', width: '15%', cellsalign: 'center', align: 'center' },
        { text: 'Sistem Operasi', datafield: 'perangkattipe', width: '10%', cellsalign: 'center', align: 'center' }
        ]
    });
});        
    </script>
  </head>

  <body>

    <div id="wrapper">

        <?php
            $this->load->view('others/nav');
        ?>

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>Log Histori Pengguna</h1>                    
                </div>    
          </div>
        </div><!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div style="overflow-y: scroll;overflow-x: hidden; min-width: 700px;">
                    <div id="gridmonitor"></div>
                </div>
            </div>
        </div><!-- /.row -->
        
      </div><!-- /#page-wrapper -->
      
    </div><!-- /#wrapper -->
  </body>
</html>
