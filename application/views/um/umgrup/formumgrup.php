<div class="col-lg-5">
    <form role="form">
      <div class="form-group">
        <label for="namagrup">Nama Grup Pengguna </label>
        <input type="hidden" class="form-control" id="idgrup">
        <input type="text" class="form-control" id="namagrup" placeholder="Masukkan Nama Grup">
      </div>
      <div class="form-group">
        <label for="ketgrup">Keterangan (opsional) </label>
        <textarea style="resize: vertical;" class="form-control" id="ketgrup" placeholder="Masukkan Keterangan"></textarea>
      </div>
        
        <button type="button" class="btn btn-primary" onclick="simpandata();">
            Simpan
        </button>
        <button type="button" class="btn btn-default" onclick="tampil_grid();">
            Batal
        </button>
    </form>    
</div>