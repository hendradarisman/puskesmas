<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->
    <script type="text/javascript">
            function tampil_grid(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'umgrup/gridumgrup'; ?>",
                    success: function(resp){   
                            $("#kontengrup").html(resp);
                    }
                });
            };
            
            function tampil_form(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'umgrup/formumgrup'; ?>",
                    success: function(resp){                            
                            $("#kontengrup").html(resp);
                    }
                });
            };
            
            function isidata(idgrup,namagrup,keterangan){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'umgrup/formumgrup'; ?>",
                    success: function(resp){                            
                            $("#kontengrup").html(resp);
                            var v1 = document.getElementById("idgrup");
                                v1.value=idgrup;
                            var v2 = document.getElementById("namagrup");
                                v2.value=namagrup;
                            var v3 = document.getElementById("ketgrup");
                                v3.value=keterangan;                            
                    }
                });
            };
            
            function simpandata(){
                var idgrup=$('#idgrup').val();
                var namagrup=$('#namagrup').val();
                var ketgrup=$('#ketgrup').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'umgrup/prosesdata'; ?>",
                    data: {"idgrup":idgrup,"namagrup":namagrup
                            ,"ketgrup":ketgrup},
                    success: function(resp){
                            alert(resp);
                            tampil_grid();
                    }
                });
            };
            
            function hapusdata(idgrup,namagrup){
                var r=confirm("Lanjutkan menghapus data '"+namagrup+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'umgrup/prosesdata'; ?>",
                            data: {"idgrup":idgrup,
                                    "stat":"hapus"},
                            success: function(resp){
                                    alert(resp);
                                    tampil_grid();
                            }
                        });
                  }
            };
    </script>
  </head>

  <body onload="tampil_grid();">

    <div id="wrapper">
    <?php
        $this->load->view('others/nav');
    ?>
      <!-- Sidebar -->
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        
        <div class="row">
            <div id="kontengrup"></div>
        </div><!-- /.row -->
        
      </div><!-- /#page-wrapper -->
      
    </div><!-- /#wrapper -->
  </body>
</html>
