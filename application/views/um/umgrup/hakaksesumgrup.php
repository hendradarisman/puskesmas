<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo "Hak Akses - ".$this->apps->name; ?></title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
      
      $grupid = $this->uri->segment(4, 0);
    ?>
    <script>
        function modulact(modul_id,grup_id) {            
            $.ajax({
            type: "POST",
            url: "<?php echo base_url().'umgrup/setakses'; ?>",
            data: {"grup_id":grup_id,"modul_id":modul_id,"stat":"modul"},
            success: function(resp){
                if(resp){
                    gettabel();   
                }else{
                    alert("Permintaan gagal diproses");
                    gettabel();   
                }
              }
            });

        }

        function aksesact(modul_id,grup_id) {
            var T = "";
            var E = "";
            var H = "";
            var C = "";
            var akses = "N,N,N,N";
            if($("#T"+modul_id).prop("checked")) {
              T = "Y";
            } else {
              T = "N";
            }
            if($("#E"+modul_id).is(":checked")) {
              E = "Y";
            } else {
              E = "N";
            }
            if($("#H"+modul_id).is(":checked")) {
              H = "Y";
            } else {
              H = "N";
            }
            if($("#C"+modul_id).is(":checked")) {
              C = "Y";
            } else {
              C = "N";
            }
            akses = T+","+E+","+H+","+C;
            $.ajax({
            type: "POST",
            url: "<?php echo base_url().'umgrup/setakses'; ?>",
            data:{"grup_id":grup_id,"modul_id":modul_id,"akses":akses,"stat":"akses"},
            success: function(resp){
                if(resp){
                    gettabel();   
                }else{
                    alert("Permintaan gagal diproses");
                    gettabel();   
                }
              }
            });
        }
        
        function gettabel(){
            var grupid="<?php print $grupid; ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url().'umgrup/gethakakses'; ?>",
                data: {"grupid":grupid},
                success: function(resp){   
                        $("#tbHakAkses").html(resp);
                }
            });
        };
    </script>
  </head>

  <body onload="javascript:gettabel();">
    <div id="wrapper">
    <?php
        $this->load->view('others/nav');
    ?>

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>Set Hak Akses <?php print str_replace('_', ' ', $this->uri->segment(3, 0));?></h1>
                </div>    
          </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="tbHakAkses"></div>
            <a href="<?=base_url() ?>umgrup" class="btn btn-default">
                Kembali
            </a>                
            </div>
        </div>
      </div>
    </div>
  </body>
</html>