<script type="text/javascript">
// Javascript for menu START //
    var globidmenu="";
    var globnamamenu="";
    function susunmodul(idmenu,namamenu){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'ummenu/gridummodul'; ?>",
            data: {"idmenu":idmenu,"namamenu":namamenu},
            success: function(resp){   
                    $("#kontenmenu").html(resp);
                    globidmenu = idmenu;
                    globnamamenu = namamenu;
            }
        });
    };

    function tampil_grid(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'ummenu/gridummenu'; ?>",
            success: function(resp){   
                    $("#kontenmenu").html(resp);
            }
        });
    };

    function tampil_form(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'ummenu/formummenu'; ?>",
            success: function(resp){                            
                    $("#kontenmenu").html(resp);
            }
        });
    };

    function isidata(idmenu,namamenu,keterangan){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'ummenu/formummenu'; ?>",
            success: function(resp){                            
                    $("#kontenmenu").html(resp);
                    var v1 = document.getElementById("idmenu");
                        v1.value=idmenu;
                    var v2 = document.getElementById("namamenu");
                        v2.value=namamenu;
                    var v3 = document.getElementById("ketmenu");
                        v3.value=keterangan;                            
            }
        });
    };

    function simpandata(){
        var idmenu=$('#idmenu').val();
        var namamenu=$('#namamenu').val();
        var ketmenu=$('#ketmenu').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'ummenu/prosesdata'; ?>",
            data: {"idmenu":idmenu,"namamenu":namamenu
                    ,"ketmenu":ketmenu},
            success: function(resp){
                    alert(resp);
                    tampil_grid();
            }
        });
    };

    function hapusdata(idmenu,namamenu){
        var r=confirm("Lanjutkan menghapus data '"+namamenu+"' ?");
        if (r===true)
          {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'ummenu/prosesdata'; ?>",
                    data: {"idmenu":idmenu,
                            "stat":"hapus"},
                    success: function(resp){
                            alert(resp);
                            tampil_grid();
                    }
                });
          }
    };
// Javascript for menu END //  

// Javascript for modul START //
    function kosongkandatamodul(){
        var v1 = document.getElementById("txtIdModul");
            v1.value="";
        var v2 = document.getElementById("namamodul");
            v2.value="";
        var v3 = document.getElementById("kelasmodul");
            v3.value="";
        var v4 = document.getElementById("ketmodul");
            v4.value="";
    };
    
    function isidatamodul(txtIdModul,namamodul,kelasmodul,ketmodul){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'ummenu/formummenu'; ?>",
            success: function(resp){                            
                    var v1 = document.getElementById("txtIdModul");
                        v1.value=txtIdModul;
                    var v2 = document.getElementById("namamodul");
                        v2.value=namamodul;
                    var v3 = document.getElementById("kelasmodul");
                        v3.value=kelasmodul;
                    var v4 = document.getElementById("ketmodul");
                        v4.value=ketmodul;
            }
        });
    };

    function simpandatamodul(){
        var txtIdModul=$('#txtIdModul').val();
        var txtIdMenu=$('#txtIdMenu').val();
        var namamodul=$('#namamodul').val();
        var kelasmodul=$('#kelasmodul').val();
        var ketmodul=$('#ketmodul').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url().'ummenu/simpanmodul'; ?>",
            data: {"txtIdModul":txtIdModul,"txtIdMenu":txtIdMenu
                    ,"namamodul":namamodul,"kelasmodul":kelasmodul
                    ,"ketmodul":ketmodul},
            success: function(resp){
                    alert(resp);
                    susunmodul(globidmenu,globnamamenu);
                    kosongkandatamodul();
            }
        });
    };

    function hapusdatamodul(txtIdModul,namamodul){
        var r=confirm("Lanjutkan menghapus data '"+namamodul+"' ?");
        if (r===true)
          {
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'ummenu/simpanmodul'; ?>",
                    data: {"txtIdModul":txtIdModul,
                            "stat":"hapus"},
                    success: function(resp){
                            alert(resp);
                            susunmodul(globidmenu,globnamamenu);
                    }
                });
          }
    };
// Javascript for modul END //
</script>

