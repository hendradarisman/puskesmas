<div class="col-lg-5">
    <form role="form">
      <div class="form-group">
        <label for="namamenu">Nama Menu </label>
        <input type="hidden" class="form-control" id="idmenu">
        <input type="text" class="form-control" id="namamenu" placeholder="Masukkan Nama Menu">
      </div>
      <div class="form-group">
        <label for="ketmenu">Keterangan (opsional) </label>
        <textarea style="resize: vertical;" class="form-control" id="ketmenu" placeholder="Masukkan Keterangan"></textarea>
      </div>
        
        <button type="button" class="btn btn-primary" onclick="simpandata();">
            Simpan
        </button>
        <button type="button" class="btn btn-default" onclick="tampil_grid();">
            Batal
        </button>
    </form>    
</div>