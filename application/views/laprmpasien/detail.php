<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
      $kodepasien = $this->uri->segment(3);
    ?>
    
    <!-- Page Specific CSS -->
    <script type="text/javascript">
            var userstat = 0;
            var kodepasien = '<?php echo $kodepasien;?>';
            function tampil_detail(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'laprmpasien/detaillaprmpasien'; ?>",
                    data:{"kodepasien":kodepasien},
                    success: function(resp){   
                        $("#konten").html(resp);
                    }
                });
            };
            
            function tampil_riwayat(idregistrasi,divriwayat){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'laprmpasien/riwayatlaprmpasien'; ?>",
                    data:{"idregistrasi":idregistrasi},
                    success: function(resp){   
                        $("#"+divriwayat).html(resp);
                    }
                });
            };
    </script>
  </head>

  <body onload="tampil_detail();">

    <div id="wrapper">

        <?php
            $this->load->view('others/nav');
        ?>

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <a class="btn btn-danger" href="javascript:void(0);" onclick="window.history.back();">
                            <span class="glyphicon glyphicon-circle-arrow-left"></span> Kembali
                        </a>
                        <?php 
                        $query = "SELECT namalengkap FROM mstpasien WHERE kodepasien='$kodepasien'";
                        $result = mysql_query($query);
                        $row = mysql_fetch_array($result);
                            echo "Riwayat Pasien ".$row[0];
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        
        <div class="row">
            <div class="col-lg-12">
                <div id="konten"></div>
            </div>
        </div><!-- /.row -->
        
      </div><!-- /#page-wrapper -->
      
    </div><!-- /#wrapper -->
  </body>
</html>
