<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->
    <script type="text/javascript">
            function add_pasien(kdpasien){
                var xkdpasien = document.getElementById('kdpasien');
                    xkdpasien.value = kdpasien;
                detailpasien();
            }
            
            function tampil_form(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'tranregistrasi/formtranregistrasi'; ?>",
                    success: function(resp){                            
                            $("#konten").html(resp);
                            $('#kdpasien').focus();
                    }
                });
            };
            
            function detailpasien(){
                var kdpasien=$('#kdpasien').val();
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'tranregistrasi/getdetailpasien'; ?>",
                    data: {"kdpasien":kdpasien},
                    success: function(resp){  
                        document.getElementById('detailpasien').innerHTML=resp;
                    }
                });
            };
            
            function keluhanpasien(idregistrasi,stat){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'tranregistrasi/getkeluhanpasien'; ?>",
                    data: {"idregistrasi":idregistrasi,"stat":stat},
                    success: function(resp){  
                        document.getElementById('keluhanpasien').innerHTML=resp;
                    }
                });
            };
            
            function batalkan(idregistrasi){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'tranregistrasi/batalkanpasien'; ?>",
                    data: {"idregistrasi":idregistrasi},
                    success: function(resp){  
                        tampil_form();
                        location.reload();
                    }
                });
            };
            
            function simpandata(){
                var idpasien=$('#idpasien').val();
                var tgljanji=$('#tgljanji').val();
                var keluhan=$('#keluhan').val();
                var namalengkap=$('#namalengkap').val();
                $.ajax({
                   type: "POST",
                   url: "<?php echo base_url().'tranregistrasi/prosesdata'; ?>",
                   data: {"idpasien":idpasien,"tgljanji":tgljanji,"keluhan":keluhan},
                   success: function(resp){
                           if(resp=='1'){
                               alert("Data "+namalengkap+" Berhasil diproses!");
                               tampil_form();
                           }else{
                               alert("Data "+namalengkap+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                   }
               });   
            };
    </script>
  </head>

  <body onload="tampil_form();">
    <div id="wrapper">
        <?php
            $this->load->view('others/nav');
        ?>
      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        <div class="row">
            <div id="konten"></div>
        </div><!-- /.row -->
      </div><!-- /#page-wrapper -->
    </div><!-- /#wrapper -->
  </body>
</html>
