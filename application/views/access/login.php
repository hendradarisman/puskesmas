<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo $this->owner->favicon; ?>
    
    <title><?php echo $this->apps->name; ?></title>
    <?php
        $this->load->view('others/css');
        $this->load->view('others/js');
        $statregister = $this->session->userdata('statregister');
        $this->session->unset_userdata('statregister');
    ?>
    <style type="text/css">
        body{
            background-color: #012E5F;
        }
    </style>
</head>

<body style="min-width: 200px;">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <span class="glyphicon glyphicon-lock"></span>
                            <?php echo $this->apps->name; ?> - Sign In
                        </h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="<?php echo base_url();?>access/validate">
                            <fieldset>
                                <div class="form-group">
                                    <?php
                                        if($statregister=="sukses"){
                                    ?>
                                        <div class="alert alert-success">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <span class="glyphicon glyphicon-ok-circle"></span>
                                            Pendaftaran berhasil , silahkan cek email anda.
                                        </div>
                                    <?php
                                        }elseif($statregister=="gagal"){
                                    ?>
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <span class="glyphicon glyphicon-exclamation-sign"></span>
                                            Pendaftaran gagal diproses, silahkan ulangi sesaat lagi.
                                        </div>
                                    <?php
                                        }elseif($statregister=="updatedata"){
                                    ?>
                                        <div class="alert alert-info">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <span class="glyphicon glyphicon-exclamation-sign"></span>
                                            Proses aktivasi berhasil, silahkan login dengan akun anda.
                                        </div>
                                    <?php
                                        }elseif($statregister=="updategagal"){
                                    ?>
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <span class="glyphicon glyphicon-exclamation-sign"></span>
                                            Proses aktivasi sudah tidak berlaku.
                                        </div>
                                    <?php
                                        }elseif($statregister=="akunkosong"){
                                    ?>
                                        <div class="alert alert-danger">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                            <span class="glyphicon glyphicon-exclamation-sign"></span>
                                            Login gagal - Email atau password anda salah.
                                        </div>
                                    <?php
                                        }
                                    ?>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" name="username" type="text" autofocus="" required="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="" required="">
                                </div>
                                <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                                <div class="checkbox">
<!--                                    <label>
                                        <input name="remember" type="checkbox" value="remember">Ingatkan saya
                                    </label>-->
                                    <div style="text-align: right;float: right">
                                        <a href="<?php echo base_url();?>account/forget">Tidak bisa masuk?</a>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
