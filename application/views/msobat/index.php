<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>

    <title>
        <?php 
            $mod = $this->uri->segment(1);
            echo $this->apps->titlepage($mod); 
        ?>
    </title>
    <!-- Add custom CSS here -->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    
    <!-- Page Specific CSS -->
    <script type="text/javascript">
            var userstat = 0;
            function tampil_grid(){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'msobat/gridmsobat'; ?>",
                    success: function(resp){   
                            $("#konten").html(resp);
                    }
                });
            };
            
            function tampil_form(idobat){
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url().'msobat/formmsobat'; ?>",
                    data: {"idobat":idobat},
                    success: function(resp){                            
                            $("#konten").html(resp);
                    }
                });
            };
                       
            function simpandata(){
                var idobat=$('#idobat').val();
                var namaobat=$('#namaobat').val();
                var jmlobat=$('#jmlobat').val();
                var satuan=$('#satuan').val();
                var hargajual=$('#hargajual').val();
                $.ajax({
                   type: "POST",
                   url: "<?php echo base_url().'msobat/prosesdata'; ?>",
                   data: {"idobat":idobat,"namaobat":namaobat,"jmlobat":jmlobat
                           ,"hargajual":hargajual,"satuan":satuan},
                   success: function(resp){
                           if(resp=='1'){
                               alert("Data "+namaobat+" Berhasil diproses!");
                               tampil_grid();
                           }else{
                               alert("Data "+namaobat+" Gagal diproses! : Error : "+resp);
                               return false;
                           }
                   }
               });   
            };
            
            function hapusdata(idobat,namaobat){
                var r=confirm("Lanjutkan menghapus data '"+namaobat+"' ?");
                if (r===true)
                  {
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url().'msobat/prosesdata'; ?>",
                            data: {"idobat":idobat,"jmlobat":"0",
                                    "stat":"hapus","hargajual":"0"},
                            success: function(resp){
                                    if(resp=='1'){
                                        alert("Data "+namaobat+" Berhasil diproses!");
                                        tampil_grid();
                                    }else{
                                        alert("Data "+namaobat+" Gagal diproses! : Error : "+resp);
                                        return false;
                                    }
                            }
                        });
                  }
            };
    </script>
  </head>

  <body onload="tampil_grid();">

    <div id="wrapper">

        <?php
            $this->load->view('others/nav');
        ?>

      <div id="page-wrapper">
        <div class="row">
          <div class="col-lg-12">
                <div style="border-bottom: solid 2px #D0D0D0; margin-bottom: 15px;">
                    <h1>
                        <?php 
                            echo $this->apps->modulname();
                        ?>                    
                    </h1>
                </div>    
          </div>
        </div><!-- /.row -->
        
        <div class="row">
            <div id="konten"></div>
            <?php
                if($this->uri->segment(3)){
                    ?>
            <div class="col-lg-12">
                        <a target="_self" href="<?=base_url() ?><?php echo $this->uri->segment(3);?>" 
                            class="btn btn-danger">
                            <span class="glyphicon glyphicon-circle-arrow-left"></span>
                            &nbsp;
                            Kembali ke <?php echo $this->apps->modulsource($this->uri->segment(3)); ?>
                        </a>            
                </div>
                    <?php
                }
            ?>
        </div><!-- /.row -->
        
      </div><!-- /#page-wrapper -->
      
    </div><!-- /#wrapper -->
  </body>
</html>
