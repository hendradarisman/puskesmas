<?php
    $namalaporan="lap.xls";
    $query=$this->input->post('idadmin');
//    header("Content-type: application/vnd.ms-excel");
//    header("Content-disposition: attachment; filename=".$namalaporan);
//    header("Pragma: no-cache");
//    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//    header("Expires: 0");
?>
<link href="<?=base_url() ?>assets/css/bootstrap.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/default.css" rel="stylesheet" />
<style type="text/css">      
    #header{
        margin-top: 10px;
        margin-left: 5px;
        margin-right: 5px;
        margin-bottom: 5px;
    }
    #konten{
        border-top: 1px solid #000;
        padding-top: 5px;
        margin-left: 5px;
        margin-right: 5px;
    }
</style>
<body style="font-size: 10px;">
<div id="header">
<table width="100%" style="font-size: 12px;">
    <tr>
        <td>
            <div class="nama">
                <?php
                    echo "<strong>".$this->owner->nama."</strong>";
                ?>
            </div>
            <div class="detail">
                <?php
                    echo $this->owner->alamat." - ".$this->owner->kota;
                    echo "<br>Telp. ".$this->owner->telp;                
                ?>
            </div>
        </td>
    </tr>
</table>    
</div>    
<div id="konten">
<?php
$data=  mysql_query($query);
$jmlrow= mysql_num_rows($data);
if($jmlrow>0){
?>
<table border="1" width="150%">
    <caption>
       <?php
        echo $namalaporan;
       ?>
    </caption>
<!--
        Menampilkan Nama kolom secara dinamis
-->
<tr>
<?php
for ($i = 1; $i <= mysql_num_fields($data); $i++) {
?>
        <td align="center" width="700">
                <?php
                        echo mysql_field_name($data, $i-1);
                ?>
        </td>
<?php
}
?>
</tr>
<!--
        Menampilkan data berdasarkan kolom secara dinamis
-->
<?php
        while($execstr=mysql_fetch_array($data))
        {
?>
<tr>
        <?php
                for ($i = 0; $i <= (mysql_num_fields($data))-1; $i++) 
                {
        ?> 
        <td align="center">
        <?php
            echo $execstr[$i];
        ?>
        </td>
        <?php
                }
        ?>
</tr>	
<?php		
        }		
?>
</table>
<?php		
    }		
?>    
</div>      
</body>
<?php

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * Website : bmediadata.com
 */