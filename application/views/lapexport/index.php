<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <?php echo $this->owner->favicon; ?>
    
    <title>404 : Page not found</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
<!--    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">-->
    <?php 
      $this->load->view('others/css');
      $this->load->view('others/js');
    ?>
    <!-- Page Specific CSS -->
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
  </head>

  <body>

    <div id="wrapper">

        <?php
            $this->load->view('others/nav');
        ?>

      <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="page-header">
                    Halaman tidak kami temukan
                    <small>
                        <?php 
                            echo $this->apps->name."&nbsp;".$this->apps->ver;
                        ?>
                    </small>
                </h2>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">
            <div class="col-lg-12" align="center">
                <label style="font-size: 75px;">
                    <span class="label label-danger">
                        Page : 404
                    </span>
                </label>
            </div>
            
        </div>
        <div class="row">
            <div class="col-lg-12" style="padding-top: 15px;" align="center">
                <div class="btn-group">
                  <button class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown">Apa yang harus saya lakukan ?</button>
                  <ul class="dropdown-menu">
                    <li><a href="javascript:window.history.back();">
                            <span class="glyphicon glyphicon-arrow-left"></span>
                            Kembali ke halaman sebelumnya
                        </a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="<?=base_url() ?>">
                            <span class="glyphicon glyphicon-th-large"></span>
                            Kembali ke halaman Dashboard
                        </a>
                    </li>
                  </ul>
                </div>
            </div>
        </div>
      </div><!-- /#page-wrapper -->
      
    </div><!-- /#wrapper -->
  </body>
</html>