<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class formtrantindakan extends CI_Model{
    function __construct(){
        parent::__construct();
    }    
    
    public function modalfoto(){
        $idtrangambar = $this->security->xss_clean($this->input->post('idtrangambar'));
        echo "<img style=\"max-height: 400px;max-width: 560px;\" src=\"".base_url()."trantindakan/tampilfoto/$idtrangambar\" />";
    }

    public function tampilfoto() {
        error_reporting(-1);
        $idtrangambar =$this->uri->segment(3);    
        $query2 = "SELECT `foto`, `fototipe` FROM `trangambar` WHERE idtrangambar = '$idtrangambar'";
        $result=  mysql_query($query2);
        $jml = mysql_num_rows($result);
        if($jml>0){
            $row=  mysql_fetch_array($result);
            header("Content-type:$row[1]");
            echo $row[0];
            exit;
        }
    }

    public function getDaftarObat() {
?>
<script>
    $(document).ready(function() {
        $('#tbdaftarobat').dataTable();
        $('#tbdaftarobat').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });
    });
</script>    
<table class="table table-striped table-bordered table-hover" id="tbdaftarobat">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Kode</th>
            <th style="text-align: center;">Nama</th>
            <th style="text-align: center;">Harga Jual</th>
            <th style="text-align: center;">Stock Obat</th>
            <th style="text-align: center;">Pilih</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $query = "SELECT idobat, kodeobat, namaobat, jumlah, hargajual
                      FROM mstobat
                      WHERE `datastat` <> 'DELETED' ORDER BY kodeobat asc";
            $result=  mysql_query($query);
            while($row=mysql_fetch_array($result)){
               echo "<tr>";
               echo "<td style=\"width: 15%;text-align: center\">$row[1]</td>";
               echo "<td style=\"text-align: left\">$row[2]</td>";
               echo "<td style=\"width: 20%;text-align: center\">$row[4]</td>";
               echo "<td style=\"width: 20%;text-align: center\">$row[3]</td>";
               echo "<td style=\"width: 7%;text-align: center\">
                        <a class=\"btn btn-sm btn-success\" data-dismiss=\"modal\" href=\"javascript:void(0);\" onclick=\"add_resep('$row[0]','$row[1]','$row[2]','$row[3]','$row[4]')\">Pilih</a>
                    </td>";
               echo "</tr>";
            }
        ?>
    </tbody>                          
</table>
<?php
    }
    
    public function getDaftarTindakan() {
?>
<script>
    $(document).ready(function() {
        $('#tbdaftartindakan').dataTable();
        $('#tbdaftartindakan').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });
    });
</script>    
<table class="table table-striped table-bordered table-hover" id="tbdaftartindakan">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Kode</th>
            <th style="text-align: center;">Nama</th>
            <th style="text-align: center;">Tarif</th>
            <th style="text-align: center;">Pilih</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $query = "SELECT `idtindakan`,`kodetindakan`,
                        `namatindakan`,`tarif`
                      FROM `msttindakan`
                      WHERE `datastat` <> 'DELETED' ORDER BY kodetindakan asc";
            $result=  mysql_query($query);
            while($row=mysql_fetch_array($result)){
               echo "<tr>";
               echo "<td style=\"width: 15%;text-align: center\">$row[1]</td>";
               echo "<td style=\"text-align: left\">$row[2]</td>";
               echo "<td style=\"width: 20%;text-align: center\">$row[3]</td>";
               echo "<td style=\"width: 7%;text-align: center\">
                        <a class=\"btn btn-sm btn-success\" data-dismiss=\"modal\" href=\"javascript:void(0);\" onclick=\"add_tindakan('$row[0]','$row[1]','$row[2]','$row[3]')\">Pilih</a>
                    </td>";
               echo "</tr>";
            }
        ?>
    </tbody>                          
</table>
<?php
    }
    

    public function gettblfoto() {
         $idregistrasi = $this->security->xss_clean($this->input->post('idregistrasi'));
?>
        <table class="table table-striped table-bordered table-hover" id="tblfoto">
            <thead>
                <tr class="info">
                    <th style="text-align: center;">Foto</th>
                    <th style="text-align: center;">Catatan</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query2 = "SELECT `idtrangambar`, `foto`, `catatan`,fotoname FROM `trangambar` WHERE idregistrasi = '$idregistrasi'
                                ORDER BY fotoname ASC";
                    $result2=  mysql_query($query2);
                    while($row=mysql_fetch_array($result2)){
                       echo "<tr>";
                       echo "<td style=\"width: 15%;text-align: center\">
                                <a href=\"javascript:void(0);\" onclick=\"getfotopasien('$row[0]')\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#myModalFoto\">
                                    <img src=\"".base_url()."trantindakan/tampilfoto/$row[0]\" width='100' height='100'/>
                                </a>
                             </td>";
                       echo "<td style=\"width: 25%;text-align: left\">$row[2]</td>";
                       echo "<td style=\"width: 7%;text-align: center;vertical-align: middle;\">
                                <a class=\"btn btn-warning btn-lg\" href=\"javascript:void(0);\" onclick=\"hapusfoto('$row[0]','$row[3]')\">Batal</a>
                            </td>";
                       echo "</tr>";
                    }
                ?>
            </tbody>                          
        </table>
<?php
    }
    
    public function getantrianpasien() {
?>
<script>
    $(document).ready(function() {
        $('#tbGetAntrianPasien').dataTable();
        $('#tbGetAntrianPasien').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });
    });
</script>

<div style="min-height: 400px;min-width: 500px;">
<table class="table table-striped table-bordered table-hover" id="tbGetAntrianPasien">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Antrian</th>
            <th style="text-align: center;">Kode Pasien</th>
            <th style="text-align: center;">Nama Lengkap</th>
            <th style="text-align: center;">Edit</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $query = "SELECT a.idpasien, a.kodepasien, a.namalengkap, a.alamat,b.`keluhan`,b.`noantrian`
                        FROM mstpasien a 
                            LEFT OUTER JOIN tranregistrasi b ON a.`idpasien` = b.`idpasien`
                        WHERE b.`statregistrasi` = 'NEW' 
                        AND DATE_FORMAT(tglregistrasi,'%Y%m%d') = DATE_FORMAT(NOW(),'%Y%m%d') 
                        and a.datastat<>'DELETED' ORDER BY b.`noantrian` ASC";
            $result=  mysql_query($query);
            while($row=mysql_fetch_array($result)){
               echo "<tr>";
               echo "<td style=\"width: 15%;text-align: center\">$row[5]</td>";
               echo "<td style=\"width: 20%;text-align: center\">$row[1]</td>";
               echo "<td style=\"text-align: left\">$row[2]</td>";
               echo "<td style=\"width: 7%;text-align: center\">
                        <a class=\"btn btn-sm btn-success\" data-dismiss=\"modal\" href=\"javascript:void(0);\" onclick=\"add_pasien('$row[1]')\">Pilih</a>
                    </td>";
               echo "</tr>";
            }
        ?>
    </tbody>                          
</table>
</div>
<?php
    }
    
    public function gettabeltindakan() {
        $idregistrasi = $this->security->xss_clean($this->input->post('idregistrasi'));
?>
        <table class="table table-striped table-bordered table-hover" id="tbtindakan">
            <thead>
                <tr class="danger">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Nama Tindakan</th>
                    <th style="text-align: center;">Keterangan</th>
                    <th style="text-align: center;">Tarif</th>
                    <th style="text-align: center;">Batal</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query2 = "SELECT b.`kodetindakan`,b.`namatindakan`,a.`tarif`,a.`idtrantindakan`,a.keterangan
                                FROM `trantindakan` a INNER JOIN `msttindakan` b ON a.`idtindakan` = b.`idtindakan`	
                                WHERE a.`idregistrasi` = '$idregistrasi' and a.datastat <> 'DELETED' ORDER BY a.`tgltindakan` ASC";
                    //echo $query2;
                    $result2=  mysql_query($query2);
                    while($row=mysql_fetch_array($result2)){
                       echo "<tr>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[0]</td>";
                       echo "<td style=\"width: 25%;text-align: left\">$row[1]</td>";
                       echo "<td style=\"text-align: left\">$row[4]</td>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[2]</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-default\" href=\"javascript:void(0);\" onclick=\"bataltindakan('$row[3]','$row[0]')\">Batal</a>
                            </td>";
                       echo "</tr>";
                    }
                ?>
            </tbody>                          
        </table>
<?php
    }

    public function gettabelresep() {
        $idregistrasi = $this->security->xss_clean($this->input->post('idregistrasi'));
?>
        <table class="table table-striped table-bordered table-hover" id="tbresep">
            <thead>
                <tr class="danger">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Nama Obat</th>
                    <th style="text-align: center;">Jumlah</th>
                    <th style="text-align: center;">Harga</th>
                    <th style="text-align: center;">Batal</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query3 = "SELECT b.`kodeobat`,b.`namaobat`,a.`jmlobat`,(a.`harga`*a.`jmlobat`) harga,a.`idtranresep`
                                FROM tranresep a INNER JOIN mstobat b ON a.`idobat` = b.`idobat`	
                                WHERE a.`idregistrasi` = '$idregistrasi' and a.datastat <> 'DELETED' ORDER BY a.`tglresep` ASC";
                    //echo $query3;
                    $result3=  mysql_query($query3);
                    while($row=mysql_fetch_array($result3)){
                       echo "<tr>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[0]</td>";
                       echo "<td style=\"text-align: left\">$row[1]</td>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[2]</td>";
                       echo "<td style=\"width: 25%;text-align: left\">$row[3]</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-default\" href=\"javascript:void(0);\" onclick=\"batalresep('$row[4]','$row[0]')\">Batal</a>
                            </td>";
                       echo "</tr>";
                    }
                ?>
            </tbody>                          
        </table>
<?php
    }
    
    public function getpasien() {
            $param = $this->security->xss_clean($this->input->post('kdpasien'));
            if(empty($param)){
            ?>
                <div class="alert alert-info" role="alert">
                    <h4>Masukkan kode pasien...</h4>
                </div>
            <?php
            exit;
            }
            $query = "SELECT a.idpasien, a.kodepasien, a.namalengkap, a.alamat,b.`keluhan` ,b.idregistrasi
                        FROM mstpasien a 
                            LEFT OUTER JOIN tranregistrasi b ON a.`idpasien` = b.`idpasien`
                        WHERE b.`statregistrasi` = 'NEW' 
                        AND DATE_FORMAT(tglregistrasi,'%Y%m%d') = DATE_FORMAT(NOW(),'%Y%m%d') 
                        and a.datastat<>'DELETED' 
                        and a.kodepasien ='$param'";
            $result=  mysql_query($query);
            $jml=  mysql_num_rows($result);
            if($jml>0){
            $row=mysql_fetch_array($result);
            ?>  
                <input type="hidden" class="form-control" id="idregistrasi" value="<?php echo $row[5];?>">
                <input type="hidden" class="form-control" id="idpasien" value="<?php echo $row[0];?>">
                <input type="hidden" class="form-control" id="namalengkap" value="<?php echo $row[2];?>">
                <div class="alert alert-info" role="alert">
                    <h4><?php echo $row[1];?></h4>
                    <h4><?php echo $row[2];?></h4>
                    <h5><?php echo $row[3];?></h5>
                </div>
                <div class="alert alert-danger" role="alert">
                    Keluhan : <br> <strong><?php echo $row[4];?></strong>
                </div>
                <button type="button" class="btn btn-info" data-backdrop="static" data-toggle="modal" data-target="#myRMPasien" onclick="tampil_riwayat();">
                    <span class="glyphicon glyphicon-list-alt"></span> Lihat Rekam Medis Pasien
                </button>
            <?php                
            }else{
            ?>     
                <div class="alert alert-danger" role="alert">
                    <h4>Kode Pasien : <strong><?php echo $param; ?></strong></h4>
                    <h4>Tidak kami temukan dalam registrasi, pastikan pasien melakukan <strong>registrasi</strong> terlebih dahulu</h4>
                </div>
            <?php   
            }    
?>
<!--<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Collapsible Group Item #1
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">
        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
      </div>
    </div>
  </div>
</div>-->
<?php
    }
    
    public function formtrantindakan(){
        $idregistrasi = $this->db->escape_str($this->input->post('idregistrasi'));
?>
<!--<h3 style="border-bottom: solid #D1D1D1 1px;padding-bottom: 5px;">
    Tindakan terhadap pasien
</h3>-->           
<form class="form-inline" role="form">
    <input type="hidden" class="form-control" id="tindakanid" >
    <div class="form-group">
        <div class="input-group" style="max-width: 175px;">
            <input type="text" class="form-control" id="kodetindakan" placeholder="Kode Tindakan">
            <div class="input-group-btn">
                <button type="button" class="btn btn-info" data-backdrop="static" data-toggle="modal" data-target="#myModalTindakan"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>                
    </div>
    <div class="form-group">
        <input type="text" class="form-control" id="hargatindakan" placeholder="Tarif">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" id="keterangan" placeholder="Keterangan (optional)">
    </div>
    <button type="button" class="btn btn-primary" onclick="simpantindakan();"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
</form>
<div style="padding: 10px 0px;">
    <div id="nmtindakan"><label class="label label-info">Nama Tindakan : -</label></div>
</div>

<div style="height:230px;overflow-x: hidden;overflow-y: auto;border: solid 1px #D1D1D1;padding: 2px 2px;margin-bottom: 10px;">
    <div id="tabeltindakan">
        <table class="table table-striped table-bordered table-hover" id="tbtindakan">
            <thead>
                <tr class="danger">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Nama Tindakan</th>
                    <th style="text-align: center;">Keterangan</th>
                    <th style="text-align: center;">Tarif</th>
                    <th style="text-align: center;">Batal</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query2 = "SELECT b.`kodetindakan`,b.`namatindakan`,a.`tarif`,a.`idtrantindakan`,a.keterangan
                                FROM `trantindakan` a INNER JOIN `msttindakan` b ON a.`idtindakan` = b.`idtindakan`	
                                WHERE a.`idregistrasi` = '$idregistrasi' and a.datastat <> 'DELETED' ORDER BY a.`tgltindakan` ASC";
                    //echo $query2;
                    $result2=  mysql_query($query2);
                    while($row=mysql_fetch_array($result2)){
                       echo "<tr>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[0]</td>";
                       echo "<td style=\"width: 25%;text-align: left\">$row[1]</td>";
                       echo "<td style=\"text-align: left\">$row[4]</td>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[2]</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-default\" href=\"javascript:void(0);\" onclick=\"bataltindakan('$row[3]','$row[0]')\">Batal</a>
                            </td>";
                       echo "</tr>";
                    }
                ?>
            </tbody>                          
        </table>
    </div>
</div>   

<?php
$querydiagnosis = "SELECT `idtrandiagnosis`, 
                        `diagnosis`,
                        `catatan`,
                        anamnesis
                      FROM `trandiagnosis`
                      WHERE `idregistrasi` = '$idregistrasi'";
$resultdiagnosis=  mysql_query($querydiagnosis);
$xrow=mysql_fetch_array($resultdiagnosis);
$idtrandiagnosis=$xrow[0];
$diagnosis=$xrow[1];
$catatan=$xrow[2];
$anamnesis = $xrow[3];
?>
<center>
    <h3 style="border-bottom: solid #D1D1D1 1px;padding-bottom: 5px;">
        Anamnesis dan Diagnosa Pasien
    </h3>
</center>    
<div class="form-group">
    <textarea style="resize: none;" class="form-control textarea" id="anamnesis" placeholder="Anamnesis Pasien"><?php echo $anamnesis;?></textarea>
</div>
<div class="form-group">
    <input type="hidden" class="form-control" id="idtrandiagnosis" value="<?php echo $idtrandiagnosis;?>">
    <textarea style="resize: none;" class="form-control" id="diagnosis" placeholder="Diagnosa masalah"><?php echo $diagnosis;?></textarea>
</div>
<div class="form-group">
    <textarea style="resize: none;" class="form-control" id="catatan" placeholder="Catatan kepada pasien"><?php echo $catatan;?></textarea>
</div>


<!--Resep Area Start-->
<center>
    <h3 style="border-bottom: solid #D1D1D1 1px;padding-bottom: 5px;">
        Resep Obat
    </h3>
</center>    
<form class="form-inline" role="form">
    <input type="hidden" class="form-control" id="idobat" >
    <div class="form-group">
        <div class="input-group" style="max-width: 175px;">
            <input type="text" class="form-control" id="kodeobat" placeholder="Kode Obat">
            <div class="input-group-btn">
                <button type="button" class="btn btn-info" data-backdrop="static" data-toggle="modal" data-target="#myModalObat"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>                
    </div>
    <div class="form-group">
        <input type="hidden" class="form-control" id="sisaobat">
        <input type="text" class="form-control" id="jumlahobat" placeholder="Jumlah Obat">
    </div>    
    <div class="form-group">
        <input type="text" class="form-control" id="hargaobat" placeholder="Harga">
    </div>
    <button type="button" class="btn btn-primary" onclick="simpanresep();"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
</form>
<div style="padding: 10px 0px;">
    <div id="namaobat"><label class="label label-info">Nama Obat : -</label></div>
</div>

<div style="height:230px;overflow-x: hidden;overflow-y: auto;border: solid 1px #D1D1D1;padding: 2px 2px;margin-bottom: 10px;">
    <div id="tblresep">
        <table class="table table-striped table-bordered table-hover" id="tbresep">
            <thead>
                <tr class="danger">
                    <th style="text-align: center;">Kode</th>
                    <th style="text-align: center;">Nama Obat</th>
                    <th style="text-align: center;">Jumlah</th>
                    <th style="text-align: center;">Harga</th>
                    <th style="text-align: center;">Batal</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $query3 = "SELECT b.`kodeobat`,b.`namaobat`,(a.`harga`*a.`jmlobat`) harga,a.`jmlobat`,a.`idtranresep`
                                FROM tranresep a INNER JOIN mstobat b ON a.`idobat` = b.`idobat`	
                                WHERE a.`idregistrasi` = '$idregistrasi' and a.datastat <> 'DELETED' ORDER BY a.`tglresep` ASC";
                    //echo $query3;
                    $result3=  mysql_query($query3);
                    while($row=mysql_fetch_array($result3)){
                       echo "<tr>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[0]</td>";
                       echo "<td style=\"text-align: left\">$row[1]</td>";
                       echo "<td style=\"width: 15%;text-align: center\">$row[2]</td>";
                       echo "<td style=\"width: 25%;text-align: left\">$row[3]</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-default\" href=\"javascript:void(0);\" onclick=\"batalresep('$row[4]','$row[0]')\">Batal</a>
                            </td>";
                       echo "</tr>";
                    }
                ?>
            </tbody>                          
        </table>    
    </div>
</div> 

<!--Resep Area End-->



<!--Upload Foto Area Start-->
<center>
    <h3 style="border-bottom: solid #D1D1D1 1px;padding-bottom: 5px;">
        Upload Foto
    </h3>
</center>    
<iframe name="upload-frame" id="upload-frame" style="display:none;"></iframe>  
<form class="form-inline" role="form" name="uploadfotopasien" id="uploadfotopasien" action="javascript:simpanfoto();" method="post" enctype="multipart/form-data">
    <div class="form-group">
        <input id="fileupload" name="fileupload" type="file">
    </div>
    <div class="form-group">
        <input type="text" class="form-control" id="txtKeteranganFoto" name="txtKeteranganFoto" placeholder="Keterangan">
    </div>
    <button type="submit" onclick="javascript:simpanfoto();" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span>&nbsp;Upload</button>
</form>
<div style="padding: 10px 0px;">
    <div id="namaobat"><label class="label label-info msg">Status</label></div>
</div>
<div style="height:230px;overflow-x: hidden;overflow-y: auto;border: solid 1px #D1D1D1;padding: 2px 2px;margin-bottom: 10px;">
    <div id="tblfoto">
        <table class="table table-striped table-bordered table-hover" id="tbresep">
            <thead>
                <tr class="danger">
                    <th style="text-align: center;">Foto</th>
                    <th style="text-align: center;">Catatan</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>                  
        </table>    
    </div>
</div> 

<!--Upload Foto End-->



<div style="margin-bottom: 50px;">
    <button type="button" class="btn btn-danger" onclick="submitpasien();">
        Proses Data Pasien
    </button>
    <button type="button" class="btn btn-default" onclick="javascript:location.reload();">
        Batal
    </button>
</div>    

<!--Modal Tindakan area start                -->
<div class="modal fade" id="myModalTindakan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Tindakan</h4>
      </div>
      <div class="modal-body">
            <script>
                $(document).ready(function() {
                    $('#tbdaftartindakan').dataTable();
                    $('#tbdaftartindakan').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div style="min-height: 400px;min-width: 500px;">
<table class="table table-striped table-bordered table-hover" id="tbdaftartindakan">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Kode</th>
            <th style="text-align: center;">Nama</th>
            <th style="text-align: center;">Tarif</th>
            <th style="text-align: center;">Pilih</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $query = "SELECT `idtindakan`,`kodetindakan`,
                        `namatindakan`,`tarif`
                      FROM `msttindakan`
                      WHERE `datastat` <> 'DELETED' ORDER BY kodetindakan asc";
            $result=  mysql_query($query);
            while($row=mysql_fetch_array($result)){
               echo "<tr>";
               echo "<td style=\"width: 15%;text-align: center\">$row[1]</td>";
               echo "<td style=\"text-align: left\">$row[2]</td>";
               echo "<td style=\"width: 20%;text-align: center\">$row[3]</td>";
               echo "<td style=\"width: 7%;text-align: center\">
                        <a class=\"btn btn-sm btn-success\" data-dismiss=\"modal\" href=\"javascript:void(0);\" onclick=\"add_tindakan('$row[0]','$row[1]','$row[2]','$row[3]')\">Pilih</a>
                    </td>";
               echo "</tr>";
            }
        ?>
    </tbody>                          
</table>
</div>
          
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal area end-->

<!--Modal Obat area start                -->
<div class="modal fade" id="myModalObat" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Obat</h4>
      </div>
      <div class="modal-body">
            <script>
                $(document).ready(function() {
                    $('#tbdaftarobat').dataTable();
                    $('#tbdaftarobat').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div style="min-height: 400px;min-width: 500px;">
<table class="table table-striped table-bordered table-hover" id="tbdaftarobat">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Kode</th>
            <th style="text-align: center;">Nama</th>
            <th style="text-align: center;">Harga Jual</th>
            <th style="text-align: center;">Stock Obat</th>
            <th style="text-align: center;">Pilih</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $query = "SELECT idobat, kodeobat, namaobat, jumlah, hargajual
                      FROM mstobat
                      WHERE `datastat` <> 'DELETED' ORDER BY kodeobat asc";
            $result=  mysql_query($query);
            while($row=mysql_fetch_array($result)){
               echo "<tr>";
               echo "<td style=\"width: 15%;text-align: center\">$row[1]</td>";
               echo "<td style=\"text-align: left\">$row[2]</td>";
               echo "<td style=\"width: 20%;text-align: center\">$row[4]</td>";
               echo "<td style=\"width: 20%;text-align: center\">$row[3]</td>";
               echo "<td style=\"width: 7%;text-align: center\">
                        <a class=\"btn btn-sm btn-success\" data-dismiss=\"modal\" href=\"javascript:void(0);\" onclick=\"add_resep('$row[0]','$row[1]','$row[2]','$row[3]','$row[4]')\">Pilih</a>
                    </td>";
               echo "</tr>";
            }
        ?>
    </tbody>                          
</table>
</div>
          
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal area end-->



<!--Modal Obat area start                -->
<div class="modal fade" id="myModalFoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Foto Pasien</h4>
      </div>
      <div class="modal-body">
              <div id="fotopasien"></div>
      </div>  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>    
<!--Modal area end-->
<?php
    }

}
