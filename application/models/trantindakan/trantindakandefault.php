<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class trantindakandefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function hapusfoto() {
        $idtrangambar = $this->db->escape_str($this->input->post('idtrangambar'));
        $query = "DELETE FROM trangambar where idtrangambar = '$idtrangambar'";
            //echo $query;
            $resl = $this->db->query($query); 
             if($resl){
                 return true;
             }else{
                return false;
             }
    }
    
    public function uploadfoto() {
        //error_reporting(0);
        $sesArray = $this->session->all_userdata();
        $idregistrasi = $this->db->escape_str($this->input->post('idregistrasi'));
        $txtKeteranganFoto = $this->db->escape_str($this->input->post('txtKeteranganFoto'));
        $idpengguna = $sesArray['idpengguna'];
        $idperson = $sesArray['idperson'];
        $idpasien = $this->db->escape_str($this->input->post('idpasien'));
        $fileName = $_FILES['fileupload']['name'];
        
        if(!empty($fileName)){
            $size = getimagesize($_FILES['fileupload']['tmp_name']);
            $type = $size['mime'];
            $imgfp = file_get_contents($_FILES['fileupload']['tmp_name']);
            $name = date('YmdHis');//$_FILES['fileupload']['name'];
            $query = "INSERT INTO trangambar
                        (idtrangambar,idregistrasi,foto,fototipe,fotoname,
                            catatan,kdtrantindakan,idpasien,iddokter,idpengguna)
                    VALUES (uuid(),'$idregistrasi','".$this->db->escape_str($imgfp)."','$type','$name',
                            '".str_replace('+',' ',$txtKeteranganFoto)."','','$idpasien','$idperson','$idpengguna')";
            //echo $query;
            $resl = $this->db->query($query); 
             if($resl){
                 return true;
             }else{
                return false;
             }
        }
    }
    
    public function prosesresep() {
        //"idregistrasi":idregistrasi,"idobat":idobat,"jumlahobat":jumlahobat,"hargaobat":hargaobat,"idpasien":idpasien
        $sesArray = $this->session->all_userdata();
            
        $idtranresep = $this->db->escape_str($this->input->post('idtranresep'));
        $idregistrasi = $this->db->escape_str($this->input->post('idregistrasi'));
        $idobat = $this->db->escape_str($this->input->post('idobat'));
        $hargaobat = $this->db->escape_str($this->input->post('hargaobat'));
        $jumlahobat = $this->db->escape_str($this->input->post('jumlahobat'));
        
        $idpengguna = $sesArray['idpengguna'];
        $idpasien = $this->db->escape_str($this->input->post('idpasien'));
        $stat = $this->db->escape_str($this->input->post('stat'));
        
        $query = "call uspTranResep ('$idtranresep','$idregistrasi','$idobat','$hargaobat','$jumlahobat'"
                . " ,'$idpasien','$idpengguna','$stat')";
        //echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             return true;
         }else{
            return false;
         }
        
    }


    public function prosesdata(){        
        $sesArray = $this->session->all_userdata();
            
        $idtrantindakan = $this->db->escape_str($this->input->post('idtrantindakan'));
        $idregistrasi = $this->db->escape_str($this->input->post('idregistrasi'));
        $tindakanid = $this->db->escape_str($this->input->post('tindakanid'));
        $hargatindakan = $this->db->escape_str($this->input->post('hargatindakan'));
        $idpengguna = $sesArray['idpengguna'];
        $idperson = $sesArray['idperson'];
        $keterangan = $this->db->escape_str($this->input->post('keterangan'));
        $idpasien = $this->db->escape_str($this->input->post('idpasien'));
        $stat = $this->db->escape_str($this->input->post('stat'));
        
        $query = "call uspTranTindakan ('$idtrantindakan','$idregistrasi','$tindakanid','$hargatindakan'"
                . " ,'$keterangan','$idpasien','$idperson','$idpengguna','$stat')";
        //echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             return true;
         }else{
            return false;
         }
    }
    
    public function submitpasien(){        
        
        $sesArray = $this->session->all_userdata();
        $idtrandiagnosis = $this->db->escape_str($this->input->post('idtrandiagnosis'));
        $idregistrasi = $this->db->escape_str($this->input->post('idregistrasi'));
        $anamnesis = $this->db->escape_str($this->input->post('anamnesis'));
        $diagnosis = $this->db->escape_str($this->input->post('diagnosis'));
        $catatan = $this->db->escape_str($this->input->post('catatan'));
        $pemeriksaanfisik = $this->db->escape_str($this->input->post('pemeriksaanfisik'));
        
        $edits = $this->db->escape_str($this->input->post('edits'));
        if(!empty($edits) || $edits !=""){
            $edits="trantindakanedit";
        }else{
            $edits="trantindakan";
        }
        if(strlen($anamnesis)<5 || strlen($pemeriksaanfisik)<5 || strlen($diagnosis)<5){
            $this->session->set_userdata('statproses', 'kurang');
            redirect($edits);
        }
        
        $idpengguna = $sesArray['idpengguna'];
        $idperson = $sesArray['idperson'];
        $idpasien = $this->db->escape_str($this->input->post('idpasien'));
        $stat = $this->db->escape_str($this->input->post('stat'));
        
        $query = "call uspTranDiagnosis ('$idtrandiagnosis','$idregistrasi','$anamnesis','$pemeriksaanfisik','$diagnosis','$catatan'"
                . " ,'$idpasien','$idperson','$idpengguna','$stat')";
        //echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             $this->session->set_userdata('statproses', 'ok');
             redirect($edits);
         }else{
             $this->session->set_userdata('statproses', 'gagal');
            redirect($edits);
         }
    }
    
    function getriwayatpasien() {
        $kdpasien = $this->security->xss_clean($this->input->post('kdpasien'));
?>
<table class="table table-bordered table-hover">
    <caption style="font-size:24px;">Diagnosa Pasien</caption>
    <tr class="info">
        <td style="text-align: center;" width="15%">
            Tanggal
        </td>
        <td style="text-align: center;">
            Anamnesis
        </td>
        <td style="text-align: center;">
            Diagnosa
        </td>
        <td style="text-align: center;">
            Catatan Pasien
        </td>
    </tr>
    <?php
        $qry2="SELECT DATE_FORMAT(b.`tglregistrasi`,'%d %M %Y') tglregistrasi,a.`anamnesis`,a.`diagnosis`,a.`catatan` FROM `trandiagnosis` a
                    INNER JOIN `tranregistrasi` b ON a.`idregistrasi` = b.`idregistrasi`
                    INNER JOIN mstpasien c ON b.`idpasien` = c.`idpasien`
                WHERE c.`kodepasien`='$kdpasien'";
        $result2 = mysql_query($qry2);
        while($row2 = mysql_fetch_array($result2)){
?>
    <tr>
        <td style="text-align: center;">
            <?php echo $row2[0]; ?>
        </td>
        <td>
            <?php echo $row2[1]; ?>
        </td>
        <td>
            <?php echo $row2[2]; ?>
        </td>
        <td>
            <?php echo $row2[3]; ?>
        </td>
    </tr>    
<?php
        }
    ?>
</table>  
<a class="btn btn-danger" href="<?=base_url()?>laprmpasien/detail/<?php echo $kdpasien;?>" target="_blank">
    Detail Rekam Medis Pasien
</a>
<?php
    }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */
