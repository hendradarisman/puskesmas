<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridtranbayar extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
<script>
    $(document).ready(function() {
        $('#tbMsTindakan').dataTable({
            "order": [[ 0, "desc" ],[ 5, "asc" ]],
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?=base_url() ?>tranbayar/getDataTranBayar",
            "columns": [
                {"width": "10%" },
                {"width": "17%" },
                {"width": "11%" },
                null,
                {"width": "10%" },
                {"width": "7%" }
            ],
            "oLanguage": {
                "sProcessing": "<div class=\"col-lg-12\"><h5><label class=\"label label-danger\">Silahkan tunggu, sedang mengambil data</label><h5></div>"
            }
        });
        $('#tbMsTindakan').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });
    });
</script>

<div class="col-lg-12">
<table class="table table-striped table-bordered table-hover" id="tbMsTindakan">
    <thead>
        <tr class="danger">
            <th style="width: 15%;text-align: center;">Kode Transaksi</th>
            <th style="width: 15%;text-align: center;">Tanggal</th>
            <th style="width: 11%;text-align: center;">Kode Pasien</th>
            <th style="text-align: center;">Nama Pasien</th>
            <th style="width: 10%;text-align: center;">Biaya</th>
            <th style="width: 7%;text-align: center;">Aksi</th>
        </tr>
    </thead>                         
</table>
</div>

<!--Modal2 area start                -->
<div class="modal fade" id="modPembayaran" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Transaksi</h4>
      </div>
      <div class="modal-body">
          <div id="bayarpasien"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal2 area end-->            
        <?php
    }
}