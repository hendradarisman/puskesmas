<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class laptranbayar extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function cetaknota(){
        $idregistrasi = $this->uri->segment(3);
?>
<link href="<?=base_url() ?>assets/css/bootstrap.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/default.css" rel="stylesheet" />
<style type="text/css">      
    #nota{
        border: 1px solid black;
        width:  9cm;
        height: auto;
    }
    #header{
        margin-top: 10px;
        margin-left: 5px;
        margin-right: 5px;
        margin-bottom: 5px;
    }
    #konten{
        border-top: 1px solid #000;
        padding-top: 5px;
        margin-left: 5px;
        margin-right: 5px;
    }
    #footer{
        padding: 10px 5px;
    }
</style>
<!---->
<body style="font-size: 10px;" onload="window.print();">
<div id="nota">
<div id="header">
<table width="100%" style="font-size: 12px;">
    <tr>
        <td width="30%">
            <?php
                if(empty($this->owner->logo) || $this->owner->logo ==""){
            ?>
                    <img class="img-rounded" src="<?=base_url() ?>assets/images/nologo.png" />
            <?php
                }else{
            ?>
                    <img class="img-rounded" width="50" height="50" src="<?php echo base_url().$this->owner->logo; ?>" />
            <?php
                }
            ?>
        </td>
        <td>
            <div class="nama">
                <?php
                    echo "<strong>".$this->owner->nama."</strong>";
                ?>
            </div>
            <div class="detail">
                <?php
                    echo $this->owner->alamat." - ".$this->owner->kota;
                    echo "<br>Telp. ".$this->owner->telp;                
                ?>
            </div>
        </td>
    </tr>
</table>    
</div>    
<div id="konten">
<table class="table" style="font-size: 10px;">
    <?php
        $qry0="SELECT b.`namalengkap`,b.`alamat`,a.`kdtrantindakan`,c.`namalengkap`
                ,d.`namapengguna`,date_format(a.`tglbayar`,'%d/%m/%y %H:%i') FROM `tranpembayaran` a
                    INNER JOIN mstpasien b ON a.`idpasien` = b.`idpasien`
                    INNER JOIN mstdokter c ON a.`iddokter` = c.`iddokter`
                    INNER JOIN mstpengguna d ON a.`idpengguna` = d.`idpengguna`
                WHERE a.`idregistrasi`='$idregistrasi'";
            $result0 = mysql_query($qry0);
            while($row = mysql_fetch_array($result0)){
    ?>
    <tr>
        <td width="25%">
            Kasir
        </td>
        <td>
            <?php echo $row[4];?>
        </td>
        <td width="20%">
            No. Faktur
        </td>
        <td>
            <?php echo $row[2];?>
        </td>
    </tr>
    <tr>
        <td>
            Nama Pasien
        </td>
        <td>
            <?php echo $row[0];?>
        </td>
        <td>
            Dokter
        </td>
        <td>
            <?php echo $row[3];?>
        </td>
    </tr>
    <tr>
        <td>
            Alamat
        </td>
        <td>
            <?php echo $row[1];?>
        </td>
        <td>
            Tgl. Bayar
        </td>
        <td>
            <?php echo $row[5];?>
        </td>
    </tr>
    <?php
            }
    ?>
</table>
    
<table class="table table-bordered" style="font-size: 10px;">
    <caption style="font-size:10px;">Biaya</caption>
    <tr>
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td style="text-align: center;">
            Nama Tindakan
        </td>
        <td width="25%" style="text-align: center;">
            Biaya
        </td>
    </tr>
    <?php
        $qry="SELECT b.`namatindakan`,a.`tarif` FROM `trantindakan` a
                INNER JOIN `msttindakan` b ON a.`idtindakan` = b.`idtindakan`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY a.`tgltindakan`";
            $result = mysql_query($qry);
            $num = 1;
            $total=0;
            while($row = mysql_fetch_array($result)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num;?>
        </td>
        <td>
            <?php echo $row[0];?>
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $row[1];?>
        </td>
    </tr>
    <?php
                $num++;
                $total = $total + $row[1];
            }
    ?>
    <tr>
        <td style="text-align: right;" colspan="2">
            Total Biaya
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $total;?>
        </td>
    </tr>
</table>

<table class="table table-bordered" style="font-size: 10px;">
    <caption style="font-size:10px;">Resep Obat</caption>
    <tr>
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td style="text-align: center;">
            Nama Obat
        </td>
        <td style="text-align: center;">
            Jumlah
        </td>
        <!-- <td width="25%" style="text-align: center;">
            Sub Total
        </td> -->
    </tr>
    <?php
        $qry3="SELECT b.`namaobat`,CONCAT(a.`jmlobat`, ' ',b.`satuan`) obat FROM `tranresep` a
                INNER JOIN mstobat b ON a.`idobat` = b.`idobat`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY b.`namaobat`";
            $result3 = mysql_query($qry3);
            $num3 = 1;
           // $totalobat=0;
            while($row3 = mysql_fetch_array($result3)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num3;?>
        </td>
        <td width="55%">
            <?php echo $row3[0];?>
        </td>
        <td>
            <?php echo $row3[1];?>
        </td>
    </tr>
    <?php
                $num3++;
            }
    ?>
    <tr>
      
    </tr>
</table> 
<div align="right">
    <strong>Total Biaya : Rp. <?php echo $total;?></strong>
</div>        
<br>
Terbilang :
<br>
<?php echo strtoupper(terbilang($total)." Rupiah");?>        
</div>
<div id="footer" align="center">
    Terima Kasih
    <br>
    Semoga Lekas Sembuh
</div>      
</div>  
</body>
<?php
    }

}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * Website : bmediadata.com
 */

