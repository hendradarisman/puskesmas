<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class laptranbayar extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function cetaknota(){
        $idregistrasi = $this->uri->segment(3);
?>
<link href="<?=base_url() ?>assets/css/bootstrap.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/default.css" rel="stylesheet" />
<style type="text/css">      
    #nota1{
        border: 1px solid black;
        width:  10cm;
        height: auto;
    }
    #nota{
        border: 1px solid black;
        width:  10cm;
        height: auto;
    }
    #header{
        margin-top: 10px;
        margin-left: 5px;
        margin-right: 5px;
        margin-bottom: 5px;
    }
    #header1{
        margin-top: 9px;
        margin-left: 5px;
        margin-right: 5px;
        margin-bottom: 5px;
    }
    #konten{
        border-top: 1px solid #000;
        padding-top: 10px;
        margin-left: 10px;
        margin-right: 10px;
    }
    #footer{
        padding: 10px 5px;
    }
</style>
<!---->
<body style="font-size: 10px;" onload="window.print();">

<div id="nota1">
<div id="header">
<table width="100%" style="font-size: 12px;">
    <tr>
        <td width="30%">
            <?php
                if(empty($this->owner->logo) || $this->owner->logo ==""){
            ?>
                    <img class="img-rounded" src="<?=base_url() ?>assets/images/nologo.png" />
            <?php
                }else{
            ?>
                    <img class="img-rounded" width="50" height="50" src="<?php echo base_url().$this->owner->logo; ?>" />
            <?php
                }
            ?>
        </td>
        <td>
            <div class="nama">
                <?php
                    echo "<strong>".$this->owner->nama."</strong>";
                ?>
            </div>
            <div class="detail">
                <?php
                    echo $this->owner->alamat." - ".$this->owner->kota;
                    echo "<br>Telp. ".$this->owner->telp;                
                ?>
            </div>
        </td>
    </tr>
</table>    
</div>    
<div id="konten">
<table class="table" style="font-size: 10px;">
    <?php
        $qry0="SELECT c.`namalengkap`, d.`alamat`,d.`namalengkap`  FROM `trantindakan` a
        INNER JOIN `mstdokter` c ON a.`iddokter` = c.`iddokter`
        INNER JOIN mstpasien d ON a.`idpasien` = d.`idpasien`
        WHERE a.`idregistrasi`='$idregistrasi'
        GROUP BY c.`namalengkap`, d.`alamat`,d.`namalengkap` ORDER BY a.`tgltindakan`";
            $result0 = mysql_query($qry0);
            while($row = mysql_fetch_array($result0)){
    ?>
    <tr>
       
    </tr>
    <tr>
        <td>
            Nama Pasien :
        </td>
        <td>
            <?php echo $row[2];?>
        </td>
        <td>
            Dokter :
        </td>
        <td>
            <?php echo $row[0];?>
        </td>
    </tr>
    <tr>
        <td>
            Alamat :
        </td>
        <td>
            <?php echo $row[1];?>
        </td>
    
    </tr>
    <?php
            }
    ?>
</table>
    
<table class="table table-bordered" style="font-size: 10px;">
    <caption style="font-size:10px;">Biaya</caption>
    <tr>
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td style="text-align: center;">
            Nama Tindakan
        </td>
        <td width="25%" style="text-align: center;">
            Biaya
        </td>
    </tr>
    <?php
        $qry="SELECT b.`namatindakan`,a.`tarif` FROM `trantindakan` a
                INNER JOIN `msttindakan` b ON a.`idtindakan` = b.`idtindakan`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY a.`tgltindakan`";
            $result = mysql_query($qry);
            $num = 1;
            $total=0;
            while($row = mysql_fetch_array($result)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num;?>
        </td>
        <td>
            <?php echo $row[0];?>
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $row[1];?>
        </td>
    </tr>
    <?php
                $num++;
                $total = $total + $row[1];
            }
    ?>
    <tr>
        <td style="text-align: right;" colspan="2">
            Total Biaya
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $total;?>
        </td>
    </tr>
</table>
<div align="right">
    <strong>Total Biaya : Rp. <?php echo $total;?></strong>
</div>        
<br>
Terbilang :
<br>
<?php echo strtoupper(terbilang($total)." Rupiah");?>        
</div>
<div id="footer" align="center">
    Terima Kasih
    <br>
    Semoga Lekas Sembuh
</div>   

<div id="nota">
<div id="header1">

<table width="100%" style="font-size: 12px;">
    <tr>
        <td width="30%">
            <?php
                if(empty($this->owner->logo) || $this->owner->logo ==""){
            ?>
                    <img class="img-rounded" src="<?=base_url() ?>assets/images/nologo.png" />
            <?php
                }else{
            ?>
                    <img class="img-rounded" width="50" height="50" src="<?php echo base_url().$this->owner->logo; ?>" />
            <?php
                }
            ?>
        </td>
        <td>
            <div class="nama">
                <?php
                    echo "<strong>".$this->owner->nama."</strong>";
                ?>
            </div>
            <div class="detail">
                <?php
                    echo $this->owner->alamat." - ".$this->owner->kota;
                    echo "<br>Telp. ".$this->owner->telp;                
                ?>
            </div>
        </td>
    </tr>
</table>    
</div>    
<div id="konten">
<table class="table" style="font-size: 10px;">
    <?php
        $qry0="SELECT c.`namalengkap`, d.`alamat`,d.`namalengkap`  FROM `trantindakan` a
        INNER JOIN `mstdokter` c ON a.`iddokter` = c.`iddokter`
        INNER JOIN mstpasien d ON a.`idpasien` = d.`idpasien`
        WHERE a.`idregistrasi`='$idregistrasi'
        GROUP BY c.`namalengkap`, d.`alamat`,d.`namalengkap` ORDER BY a.`tgltindakan`";
            $result0 = mysql_query($qry0);
            while($row = mysql_fetch_array($result0)){
    ?>
    <tr>
       
    </tr>
    <tr>
        <td>
            Nama Pasien :
        </td>
        <td>
            <?php echo $row[2];?>
        </td>
        <td>
            Dokter :
        </td>
        <td>
            <?php echo $row[0];?>
        </td>
    </tr>
    <tr>
        <td>
            Alamat :
        </td>
        <td>
            <?php echo $row[1];?>
        </td>
    
    </tr>
    <?php
            }
    ?>
</table>

<table class="table table-bordered" style="font-size: 10px;">
    <caption style="font-size:10px;">Resep Obat</caption>
    <tr>
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td style="text-align: center;">
            Nama Obat
        </td>
        <td style="text-align: center;">
            Jumlah
        </td>
        <td style="text-align: center;">
            Keterangan
        </td>
    </tr>
    <?php
        $qry3="SELECT b.`namaobat`,CONCAT(a.`jmlobat`, ' ',b.`satuan`) obat,a.`keteranganresep` FROM `tranresep` a
                INNER JOIN mstobat b ON a.`idobat` = b.`idobat`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY b.`namaobat`";
            $result3 = mysql_query($qry3);
            $num3 = 1;
            while($row3 = mysql_fetch_array($result3)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num3;?>
        </td>
        <td width="55%">
            <?php echo $row3[0];?>
        </td>
        <td>
            <?php echo $row3[1];?>
        </td>
        <td>
            <?php echo $row3[2];?>
        </td>
    </tr>
    <?php
                $num3++;
            }
    ?>
    <tr>
      
    </tr>
</table> 
<div align="right">
    <i>Terima Kasih</i>
    </br>
   
    </br>
    </br>
    </br>
    </br>
    <i>Puskesmas Mari Pari</i>
    </br>
    </br>
    </br>
    </br>
</div>  

</div>
</div>
   
</div>  
</body>
<?php
    }

}

/* 
 * Created by Pikamedia
 * Email : info@pikamedia.id
 * Website : bmediadata.com
 */

