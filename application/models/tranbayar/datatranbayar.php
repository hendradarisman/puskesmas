<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class datatranbayar extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function datatranbayar(){        
        $idregistrasi = $this->security->xss_clean($this->input->post('idregistrasi'));
?>
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Detail  (Klik untuk melihat detail )
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">
<table class="table table-bordered table-hover table-striped">
    <caption style="font-size:16px;">Biaya</caption>
    <tr class="info">
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td style="text-align: center;">
            Nama Tindakan
        </td>
        <td width="25%" style="text-align: center;">
            Biaya
        </td>
    </tr>
    <?php
        $qry="SELECT b.`namatindakan`,a.`tarif` FROM `trantindakan` a
                INNER JOIN `msttindakan` b ON a.`idtindakan` = b.`idtindakan`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY a.`tgltindakan`";
            $result = mysql_query($qry);
            $num = 1;
            $total=0;
            while($row = mysql_fetch_array($result)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num;?>
        </td>
        <td>
            <?php echo $row[0];?>
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $row[1];?>
        </td>
    </tr>
    <?php
                $num++;
                $total = $total + $row[1];
            }
    ?>
    <tr class="danger">
        <td style="text-align: right;" colspan="2">
            Total Biaya Jasa Dokter
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $total;?>
        </td>
    </tr>
</table>

<table class="table table-bordered table-hover table-striped">
    <caption style="font-size:16px;">Biaya Obat</caption>
    <tr class="info">
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td style="text-align: center;">
            Nama Obat
        </td>
        <td style="text-align: center;">
            Jumlah
        </td>
       
    </tr>
    <?php
        $qry3="SELECT b.`namaobat`,CONCAT(a.`jmlobat`, ' ',b.`satuan`) obat FROM `tranresep` a
                INNER JOIN mstobat b ON a.`idobat` = b.`idobat`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY b.`namaobat`";
            $result3 = mysql_query($qry3);
            $num3 = 1;
           // $totalobat=0;
            while($row3 = mysql_fetch_array($result3)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num3;?>
        </td>
        <td width="55%">
            <?php echo $row3[0];?>
        </td>
        <td>
            <?php echo $row3[1];?>
        </td>
        
    </tr>
    <?php
                $num3++;
              
            }
    ?>
</table>        
      </div>
    </div>
  </div>
</div>
<h3>
    Total biaya medis : Rp. <?php echo $total;?>
</h3>
<h4>
    Terbilang :
    <br>
    <?php echo strtoupper(terbilang($total)." Rupiah");?>
</h4>

<a href="<?php echo base_url()."tranbayar/cetaknota/".$idregistrasi; ?>" class="btn btn-warning" target="_blank">
    Cetak Nota
</a>
<?php       
    }
    
    public function getdetailtagihan() {
    $idregistrasi = $this->security->xss_clean($this->input->post('idregistrasi'));
?>
<div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
          Detail Tagihan (Klik untuk melihat detail tagihan)
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
      <div class="panel-body">
<table class="table table-bordered table-hover table-striped">
    <caption style="font-size:16px;">Biaya Jasa Dokter</caption>
    <tr class="info">
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td style="text-align: center;">
            Nama Tindakan
        </td>
        <td width="25%" style="text-align: center;">
            Biaya
        </td>
    </tr>
    <?php
        $qry="SELECT b.`namatindakan`,a.`tarif` FROM `trantindakan` a
                INNER JOIN `msttindakan` b ON a.`idtindakan` = b.`idtindakan`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY a.`tgltindakan`";
            $result = mysql_query($qry);
            $num = 1;
            $total=0;
            while($row = mysql_fetch_array($result)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num;?>
        </td>
        <td>
            <?php echo $row[0];?>
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $row[1];?>
        </td>
    </tr>
    <?php
                $num++;
                $total = $total + $row[1];
            }
    ?>
    <tr class="danger">
        <td style="text-align: right;" colspan="2">
            Total Biaya 
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $total;?>
        </td>
    </tr>
</table>

<table class="table table-bordered table-hover table-striped">
    <caption style="font-size:16px;"> Obat</caption>
    <tr class="info">
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td style="text-align: center;">
            Nama Obat
        </td>
        <td style="text-align: center;">
            Jumlah
        </td>
       
    </tr>
    <?php
        $qry3="SELECT b.`namaobat`,CONCAT(a.`jmlobat`, ' ',b.`satuan`) obat,(a.`jmlobat` * a.harga) subtotal FROM `tranresep` a
                INNER JOIN mstobat b ON a.`idobat` = b.`idobat`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY b.`namaobat`";
            $result3 = mysql_query($qry3);
            $num3 = 1;
            $totalobat=0;
            while($row3 = mysql_fetch_array($result3)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num3;?>
        </td>
        <td width="25%">
            <?php echo $row3[0];?>
        </td>
        <td>
            <?php echo $row3[1];?>
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $row3[2];?>
        </td>
    </tr>
    <?php
                $num3++;
                $totalobat = $totalobat + $row3[2];
            }
    ?>
    <tr class="danger">
        <td style="text-align: right;" colspan="3">
            Total Biaya Obat
        </td>
        <td style="text-align: right;">
            Rp. <?php echo $totalobat;?>
        </td>
    </tr>
</table>        
      </div>
    </div>
  </div>
</div>

<form class="form-horizontal" role="form">
  <div class="form-group">
    <label for="totalbiaya" class="col-sm-3 control-label">Total biaya medis</label>
    <div class="col-sm-5">
        <input type="text" class="form-control" id="totalbiaya" placeholder="Total Biaya" readonly="" value="Rp. <?php echo $totalobat+$total;?>">
    </div>
  </div>
  <div class="form-group">
    <label for="pasienbayar" class="col-sm-3 control-label">Bayar</label>
    <div class="col-sm-5">
        <input type="text" class="form-control" id="pasienbayar" placeholder="Masukkan Uang Pembayaran" onkeyup="javascript:hitungselisih('<?php echo $totalobat+$total;?>');">
    </div>
  </div>
  <div class="form-group">
    <label for="kembali" class="col-sm-3 control-label">Kembali</label>
    <div class="col-sm-5">
        <input type="text" class="form-control" id="kembali" placeholder="Kembali" readonly="">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-3">
    </div>
    <div class="col-sm-5">
        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="submitpembayaran('<?php echo $idregistrasi?>');">Bayar</button>
    </div>
  </div>
<div id="dvterbilang">
    Terbilang : 
    <br>
    <h4>
        <?php 
            echo strtoupper(terbilang($totalobat+$total)." rupiah"); 
        ?>
    </h4>
</div>
</form>
<?php
    }
}