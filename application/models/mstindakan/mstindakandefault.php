<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class mstindakandefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
        
    public function prosesdata(){   
        $idtindakan = $this->db->escape_str($this->input->post('idtindakan'));
        $namatindakan = $this->db->escape_str($this->input->post('namatindakan'));
        $tarif = $this->db->escape_str($this->input->post('tarif'));
        $keterangan = $this->db->escape_str($this->input->post('keterangan'));
        
        $idpengguna = $this->session->userdata('idpengguna');
        
        $stat = $this->db->escape_str($this->input->post('stat'));
        
        $query = "call uspMstTindakan ('$idtindakan','$namatindakan','$tarif','$keterangan','$idpengguna','$stat')";
        //echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             return true;
         }else{
            return false;
         }
    }
}

/* generate by mr.pudyasto */
/* kerabatnama : mr.pudyasto@gmail.com */