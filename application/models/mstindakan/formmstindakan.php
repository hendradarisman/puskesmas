<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class formmstindakan extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function loadform(){
        error_reporting(0);
        $vidtindakan = $this->security->xss_clean($this->input->post('idtindakan'));
        $this->db->where('idtindakan', $vidtindakan);
        $this->db->where('datastat != ', 'DELETED');
        $query = $this->db->get("msttindakan");
        if($query->num_rows>0)
        {          
            $row = $query->row();
            $idtindakan = $row->idtindakan;
            $kodetindakan = $row->kodetindakan;
            $namatindakan = $row->namatindakan;
            $tarif = $row->tarif;
            $keterangan = $row->keterangan;
        }
?>
<form role="form">
    <div class="col-lg-10">
            <div class="form-group">
                <?php
                    if($kodetindakan=="" || empty($kodetindakan)){
                ?>
                        <h1>Kode Tindakan : <span class="label label-info">Otomatis dari sistem</span></h1>
                <?php
                    }else{
                ?>
                        <h1>Kode Tindakan : <span class="label label-info"><?php echo $kodetindakan; ?></span></h1>
                <?php
                    }
                ?>
            </div>
        <hr>
    </div>
    <div class="col-lg-4">
            <div class="form-group">
                <label for="namatindakan">Nama Tindakan </label>
                <input type="hidden" class="form-control" id="idtindakan" value="<?php echo $idtindakan;?>">
                <input required="" maxlength="200" type="text" class="form-control" id="namatindakan" placeholder="Masukkan Nama Tidakan" value="<?php echo $namatindakan;?>">
            </div>
        
            <div class="form-group">
                <label for="tarif">Tarif Rp. </label>
                <input maxlength="200" type="text" class="form-control" id="tarif" placeholder="Masukkan Tarif" value="<?php echo $tarif;?>">
            </div>
            
            <div class="form-group">
                <label for="keterangan">Keterangan <small><font style="color: #ff0000">*optional</font> </small> </label>
                <textarea maxlength="500" style="resize: vertical;min-height: 104px;" class="form-control" id="keterangan" placeholder="Keterangan (optional)"><?php echo $keterangan;?></textarea>
            </div>
        
            <button type="button" class="btn btn-primary" onclick="simpandata();">
                Simpan
            </button>
            <button type="button" class="btn btn-default" onclick="tampil_grid();">
                Batal
            </button>
    </div>
</form>
<?php
    }

}
