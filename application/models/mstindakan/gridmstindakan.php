<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridmstindakan extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
            <script>
                $(document).ready(function() {
                    $('#tbMsTindakan').dataTable();
                    $('#tbMsTindakan').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div class="col-lg-12">
<table class="table table-striped table-bordered table-hover" id="tbMsTindakan">
    <thead>
        <tr class="danger">
            <th style="width: 15%;text-align: center;">Kode Tindakan</th>
            <th style="text-align: center;">Nama Tindakan</th>
            <th style="width: 10%;text-align: center;">Tarif</th>
            <th style="text-align: center;">Keterangan</th>
            <th style="width: 7%;text-align: center;">Edit</th>           
            <th style="width: 7%;text-align: center;">Hapus</th>
        </tr>
    </thead>
    <tbody>
        <?php
            //$sesArray = $this->session->all_userdata();
            
            //$sesIdpengguna = $sesArray['idpengguna'];
            $data = array(
                'idtindakan',
                'kodetindakan',
                'namatindakan',
                'tarif',
                'keterangan',
                'tglupdate',
                'datastat',
                'idpengguna'
            );

            $this->db->select($data);
   
            $this->db->where('datastat !=', 'DELETED'); 
            $this->db->order_by("kodetindakan", "asc");
            $query = $this->db->get("msttindakan");
            if ($query->num_rows() > 0)
            {
                 foreach ($query->result() as $row)
                    {
                       echo "<tr>";
                       echo "<td style=\"width: 15%;text-align: left\">$row->kodetindakan</td>";
                       echo "<td style=\"width: 30%;text-align: left\">$row->namatindakan</td>";
                       echo "<td style=\"width: 10%;text-align: left\">$row->tarif</td>";
                       echo "<td style=\"text-align: left\">$row->keterangan</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-info\" href=\"javascript:void(0);\" onclick=\"tampil_form('$row->idtindakan')\">Edit</a>
                            </td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-danger\" onclick=\"hapusdata('$row->idtindakan','$row->kodetindakan')\" href=\"javascript:void(0);\">Hapus</a></td>";
                       echo "</tr>";
                    }
            }
        ?>
    </tbody>                          
</table>
</div>

<div class="col-lg-12 input-control">
    <button type="button" class="btn btn-primary" onclick="tampil_form();">
        Tambah Data
    </button>
    <button type="button" class="btn btn-default" onclick="tampil_grid();">
        Refresh
    </button>
</div>            
        <?php
    }
}