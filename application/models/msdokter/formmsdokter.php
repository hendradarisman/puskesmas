<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class formmsdokter extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function loadform(){
        error_reporting(0);
        $viddokter = $this->security->xss_clean($this->input->post('iddokter'));
//        $wheres = "iddokter = '".$viddokter."' AND datastat != 'DELETED'";
//        $this->db->where($wheres);
        $this->db->where('iddokter', $viddokter);
        $this->db->where('datastat != ', 'DELETED');
        $query = $this->db->get("mstdokter");
//        var_dump($this->db->where());
////        echo $this->db->query_times[0];
        if($query->num_rows>0)
        {
            $row = $query->row();
            $iddokter = $row->iddokter;
            $namalengkap = $row->namalengkap;
            $tempatlahir = $row->tempatlahir;
            $tanggallahir = $row->tanggallahir;            
            $alamat = $row->alamat;
            $telp = $row->telp;
            $hp = $row->hp;
            $email = $row->email;
            $sip = $row->sip;
            $spesial = $row->spesial;
        }
?>
<form role="form">
    <div class="col-lg-10">
            <div class="form-group">
                <label for="namalengkap">Nama Lengkap </label>
                <input type="hidden" class="form-control" id="iddokter" value="<?php echo $iddokter;?>">
                <input required="" maxlength="200" type="text" class="form-control" id="namalengkap" placeholder="Masukkan Nama Lengkap" value="<?php echo $namalengkap;?>">
            </div>        
    </div>
    <div class="col-lg-4">
            <div class="form-group">
                <label for="cmbkelamin">Jenis Kelamin</label>
                <select class="form-control" id="cmbkelamin">
                    <option selected="selected" value="">-- Pilih Jenis Kelamin --</option>
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </div>
        
            <div class="form-group">
                <label for="tmplahir">Tempat Lahir </label>
                <input maxlength="200" type="text" class="form-control" id="tmplahir" placeholder="Masukkan Tempat Lahir" value="<?php echo $tempatlahir;?>">
            </div>
        
            <div class="form-group">
                <label for="tgllahir">Tanggal Lahir <small>(YYYY-MM-DD)</small></label>
                <input type="text" class="form-control" id="tgllahir" data-date-format="YYYY-MM-DD" placeholder="Masukkan Tanggal" value="<?php echo $tanggallahir;?>"/>
              <script type="text/javascript">
                  $(function () {
                        $('#tgllahir').datetimepicker();
                    });
              </script>
            </div>
        
            <div class="form-group">
                <label for="alamat">Alamat </label>
                <textarea maxlength="500" style="resize: vertical;min-height: 108px;" class="form-control" id="alamat" placeholder="Alamat"><?php echo $alamat;?></textarea>
            </div>
    </div>
    <div class="col-lg-4">
            <div class="form-group">
                <label for="telp">No. Telp </label>
                <input maxlength="20" type="text" class="form-control" id="telp" placeholder="Masukkan No. Telepon" value="<?php echo $telp;?>">
            </div>
        
            <div class="form-group">
                <label for="hp">No. HP </label>
                <input maxlength="20" type="text" class="form-control" id="hp" placeholder="Masukkan No. HP" value="<?php echo $hp;?>">
            </div>
        
            <div class="form-group">
                <label for="email">Email </label>
                <input maxlength="200" type="text" class="form-control" id="email" placeholder="Masukkan Email" value="<?php echo $email;?>">
            </div>
        
            <div class="form-group">
                <label for="sip">S.I.P </label>
                <input maxlength="20" type="text" class="form-control" id="sip" placeholder="Masukkan No. S.I.P" value="<?php echo $sip;?>">
            </div>
        
            <div class="form-group">
                <label for="spesial">Spesialisasi </label>
                <input maxlength="100" type="text" class="form-control" id="spesial" placeholder="Masukkan Spesialisasi" value="<?php echo $spesial;?>">
            </div>
 
            <button type="button" class="btn btn-primary" onclick="simpandata();">
                Simpan
            </button>
            <button type="button" class="btn btn-default" onclick="tampil_grid();">
                Batal
            </button>

    </div>
</form>
<?php
    }

}
