<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class msdokterdefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
        
    public function prosesdata(){        
        $iddokter = $this->db->escape_str($this->input->post('iddokter'));
        $namalengkap = $this->db->escape_str($this->input->post('namalengkap'));
        $cmbkelamin = $this->db->escape_str($this->input->post('cmbkelamin'));
        $tmplahir = $this->db->escape_str($this->input->post('tmplahir'));
        $tgllahir = $this->db->escape_str($this->input->post('tgllahir'));
        $alamat = $this->db->escape_str($this->input->post('alamat'));
        
        $telp = $this->db->escape_str($this->input->post('telp'));
        $hp = $this->db->escape_str($this->input->post('hp'));
        $email = $this->db->escape_str($this->input->post('email'));
        $sip = $this->db->escape_str($this->input->post('sip'));
        $spesial = $this->db->escape_str($this->input->post('spesial'));
        
        $stat = $this->db->escape_str($this->input->post('stat'));
        
        $query = "call uspMstDokter ('$iddokter','$namalengkap','$cmbkelamin','$tmplahir','$tgllahir','$alamat'"
                . " ,'$telp','$hp','$email','$sip','$spesial','$stat')";
        //echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             return true;
         }else{
            return false;
         }
    }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */