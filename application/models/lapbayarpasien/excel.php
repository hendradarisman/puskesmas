<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class excel extends CI_Model{
    function __construct(){
        parent::__construct();
    }
        public function export(){
    $namalaporan="Laporan_Data_Pembayaran_".date('d_M_Y').".xls";
    
//    header("Content-type: application/vnd.ms-excel");
//    header("Content-disposition: attachment; filename=".$namalaporan);
//    header("Pragma: no-cache");
//    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//    header("Expires: 0");
?>
<link href="<?=base_url() ?>assets/css/bootstrap.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/default.css" rel="stylesheet" />
<style type="text/css">      
    #header{
        margin-top: 10px;
        margin-left: 5px;
        margin-right: 5px;
        margin-bottom: 5px;
    }
    #konten{
        border-top: 1px solid #000;
        padding-top: 5px;
        margin-left: 5px;
        margin-right: 5px;
    }
</style>
<body style="font-size: 10px;">
<div id="header">
<table width="100%" style="font-size: 12px;">
    <tr>
        <td>
            <div class="nama">
                <?php
                    echo "<strong>".$this->owner->nama."</strong>";
                ?>
            </div>
            <div class="detail">
                <?php
                    echo $this->owner->alamat." - ".$this->owner->kota;
                    echo "<br>Telp. ".$this->owner->telp;                
                ?>
            </div>
        </td>
    </tr>
</table>    
</div>    
<div id="konten">   
<?php
// Menampilan grup per dokter
$tglawal = $_POST['tglawal'];
$tglakhir = $_POST['tglakhir'];
if(!empty($_POST['check_list'])) {
$checked_count = count($_POST['check_list']);
foreach($_POST['check_list'] as $selected) {
        $qrydokter="SELECT b.`namalengkap` FROM `tranpembayaran` a "
                . " INNER JOIN `mstdokter` b ON a.`iddokter` = b.`iddokter` WHERE a.`iddokter`='$selected' ";
        $resdokter=mysql_query($qrydokter);
        if(mysql_num_rows($resdokter)>0){
        $rowdokter=  mysql_fetch_array($resdokter);
?>
    <table class="table table-bordered">
        <caption>Rekap <?php echo $rowdokter[0]; ?></caption>
        <tr>
            <td width="10" align="center">
                No.
            </td>
            <td align="center">
                Nama Pasien
            </td>
            <td width="200" align="center">
                Tanggal
            </td>
            <td align="center">
                Jumlah
            </td>
        </tr>
<?php
        $no=1;
        $total=0;
        $qrydetail="SELECT c.`namalengkap`,date_id(a.`tglbayar`,' ') tgl, a.`jmlbayar` FROM `tranpembayaran` a 
                        INNER JOIN `mstdokter` b ON a.`iddokter` = b.`iddokter`
                        INNER JOIN mstpasien c ON a.`idpasien` = c.`idpasien`
                    WHERE a.`iddokter`='$selected' AND (DATE_FORMAT(tglbayar,'%Y-%m-%d') BETWEEN '$tglawal' AND '$tglakhir')";
        $resdetail=mysql_query($qrydetail);    
        while($rowdetail=  mysql_fetch_array($resdetail)){
?>
        <tr>
            <td width="10" align="center">
                <?php echo $no; ?>
            </td>
            <td align="left">
                <?php echo $rowdetail[0]; ?>
            </td>
            <td width="200" align="left">
                <?php echo $rowdetail[1]; ?>
            </td>
            <td width="300" align="right">
                <?php echo "Rp. ".$rowdetail[2]; ?>
            </td>
        </tr>        
<?php       
            $total=$total+$rowdetail[2];
            $no++;
        }
?>
        <tr>
            <td align="Right" colspan="3">
                Total
            </td>
            <td align="right" colspan="2">
                <?php echo "Rp. ".$total; ?>
            </td>
        </tr>        
    </table>     
<?php    
        }
    }
}
?>
</div>      
</body>
<?php
    }
}
/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * Website : bmediadata.com
 */