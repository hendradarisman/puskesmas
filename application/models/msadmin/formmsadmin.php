<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class formmsadmin extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function loadform(){
        error_reporting(0);
        $vidadmin = $this->security->xss_clean($this->input->post('idadmin'));
        $this->db->where('idadmin', $vidadmin);
        $this->db->where('datastat != ', 'DELETED');
        $query = $this->db->get("mstadmin");
        if($query->num_rows>0)
        {
            $row = $query->row();
            $idadmin = $row->idadmin;
            $namalengkap = $row->namalengkap;          
            $alamat = $row->alamat;
            $telp = $row->telp;
            $hp = $row->hp;
            $email = $row->email;
        }
?>
<form role="form">
    <div class="col-lg-10">
            <div class="form-group">
                <label for="namalengkap">Nama Lengkap </label>
                <input type="hidden" class="form-control" id="idadmin" value="<?php echo $idadmin;?>">
                <input required="" maxlength="200" type="text" class="form-control" id="namalengkap" placeholder="Masukkan Nama Lengkap" value="<?php echo $namalengkap;?>">
            </div>        
    </div>
    <div class="col-lg-4">
            <div class="form-group">
                <label for="cmbkelamin">Jenis Kelamin</label>
                <select class="form-control" id="cmbkelamin">
                    <option selected="selected" value="">-- Pilih Jenis Kelamin --</option>
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </div>
                
            <div class="form-group">
                <label for="alamat">Alamat </label>
                <textarea maxlength="500" style="resize: vertical;min-height: 108px;" class="form-control" id="alamat" placeholder="Alamat"><?php echo $alamat;?></textarea>
            </div>
    </div>
    <div class="col-lg-4">
            <div class="form-group">
                <label for="telp">No. Telp </label>
                <input maxlength="20" type="text" class="form-control" id="telp" placeholder="Masukkan No. Telepon" value="<?php echo $telp;?>">
            </div>
        
            <div class="form-group">
                <label for="hp">No. HP </label>
                <input maxlength="20" type="text" class="form-control" id="hp" placeholder="Masukkan No. HP" value="<?php echo $hp;?>">
            </div>
        
            <div class="form-group">
                <label for="email">Email </label>
                <input maxlength="200" type="text" class="form-control" id="email" placeholder="Masukkan Email" value="<?php echo $email;?>">
            </div>
        
            <button type="button" class="btn btn-primary" onclick="simpandata();">
                Simpan
            </button>
            <button type="button" class="btn btn-default" onclick="tampil_grid();">
                Batal
            </button>

    </div>
</form>
<?php
    }

}
