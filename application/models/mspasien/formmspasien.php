<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class formmspasien extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function loadform(){
        error_reporting(0);
        $vidpasien = $this->security->xss_clean($this->input->post('idpasien'));
        $this->db->where('idpasien', $vidpasien);
        $this->db->where('datastat != ', 'DELETED');
        $query = $this->db->get("mstpasien");
        if($query->num_rows>0)
        {          
            $row = $query->row();
            $kodepasien    = $row->kodepasien;
            $idpasien      = $row->idpasien;
            $namalengkap   = $row->namalengkap;
            $tempatlahir   = $row->tempatlahir;
            $tanggallahir  = $row->tanggallahir;            
            $alamat        = $row->alamat;
            $telp          = $row->telp;
            $hp            = $row->hp;
            $kerabatstatus = $row->kerabatstatus;
            $kerabatnama   = $row->kerabatnama;
            $kerabattelp   = $row->kerabattelp;
        }
?>
<form role="form">
    <div class="col-lg-10">
            <div class="form-group">
                <?php
                    if($kodepasien=="" || empty($kodepasien)){
                ?>
                        <h1>Kode Pasien : <span class="label label-info">Otomatis dari sistem</span></h1>
                <?php
                    }else{
                ?>
                        <h1>Kode Pasien : <span class="label label-info"><?php echo $kodepasien; ?></span></h1>
                <?php
                    }
                ?>
            </div>
        <hr>
    </div>
    <div class="col-lg-4">
      <div class="form-group">
            <label for="jenis">Jenis Pendaftaran</label>
             <select class="form-control" id="jenis">
                <option>Pilih</option>
                <option>Umum</option>
                <option>BPJS</option>
             </select>
      </div>

      <div class="form-group">
         <label for="usr">Nomor BPJS</label>
         <input type="text" class="form-control" id="nmrbpjs">
      </div>

            <div class="form-group">
                <label for="namalengkap">Nama Lengkap </label>
                <input type="hidden" class="form-control" id="idpasien" value="<?php echo $idpasien;?>">
                <input required="" maxlength="200" type="text" class="form-control" id="namalengkap" placeholder="Masukkan Nama Lengkap" value="<?php echo $namalengkap;?>">
            </div>
        
            <div class="form-group">
                <label for="cmbkelamin">Jenis Kelamin</label>
                <select class="form-control" id="cmbkelamin">
                    <option selected="selected" value="">-- Pilih Jenis Kelamin --</option>
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </div>
        
            <div class="form-group">
                <label for="tmplahir">Tempat Lahir </label>
                <input maxlength="200" type="text" class="form-control" id="tmplahir" placeholder="Masukkan Tempat Lahir" value="<?php echo $tempatlahir;?>">
            </div>
        
            <div class="form-group">
                <label for="tgllahir">Tanggal Lahir <small>(YYYY-MM-DD)</small></label>
                <input type="text" class="form-control" id="tgllahir" data-date-format="YYYY-MM-DD" placeholder="Masukkan Tanggal" value="<?php echo $tanggallahir;?>"/>
              <script type="text/javascript">
                  $(function () {
                        $('#tgllahir').datetimepicker();
                    });
              </script>
            </div>
        
            <div class="form-group">
                <label for="alamat">Alamat </label>
                <textarea maxlength="500" style="resize: vertical;min-height: 104px;" class="form-control" id="alamat" placeholder="Alamat"><?php echo $alamat;?></textarea>
            </div>
    </div>
    <div class="col-lg-4">        
            <div class="form-group">
                <label for="telp">No. Telp </label>
                <input maxlength="20" type="text" class="form-control" id="telp" placeholder="Masukkan No. Telepon" value="<?php echo $telp;?>">
            </div>
        
            <div class="form-group">
                <label for="hp">No. HP </label>
                <input maxlength="20" type="text" class="form-control" id="hp" placeholder="Masukkan No. HP" value="<?php echo $hp;?>">
            </div>
        
            <div class="form-group">
                <label for="kerabatnama">Nama Kerabat </label>
                <input maxlength="200" type="text" class="form-control" id="kerabatnama" placeholder="Masukkan Nama Kerabat" value="<?php echo $kerabatnama;?>">
            </div>
        
            <div class="form-group">
                <label for="kerabatstatus">Status Keluarga Kerabat <br><small>(Ayah, Ibu, Suami, Istri, Saudara)</small></label>
                <input maxlength="50" type="text" class="form-control" id="kerabatstatus" placeholder="Masukkan Status Keluarga Kerabat" value="<?php echo $kerabatstatus;?>">
            </div>
        
            <div class="form-group">
                <label for="kerabattelp">No. Telp Kerabat </label>
                <input maxlength="20" type="text" class="form-control" id="kerabattelp" placeholder="Masukkan No. Telp Kerabat" value="<?php echo $kerabattelp;?>">
            </div>

 
            <button type="button" class="btn btn-primary" onclick="simpandata();">
                Simpan
            </button>
            <button type="button" class="btn btn-default" onclick="tampil_grid();">
                Batal
            </button>

    </div>
</form>
<?php
    }

}
