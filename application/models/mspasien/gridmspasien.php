<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridmspasien extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
            <script>
                $(document).ready(function() {
                    $('#tbMsPasien').dataTable();
                    $('#tbMsPasien').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div class="col-lg-12">
<table class="table table-striped table-bordered table-hover" id="tbMsPasien">
    <thead>
        <tr class="danger">
            <th style="width: 15%;text-align: center;">Kartu Pasien</th>
            <th style="width: 15%;text-align: center;">Kode Pasien</th>
            <th style="text-align: center;">Nama</th>
            <th style="text-align: center;">Alamat</th>
            <th style="width: 10%;text-align: center;">Telp</th>
            <th style="width: 10%;text-align: center;">No. HP</th>
            <th style="width: 7%;text-align: center;">Edit</th>           
            <th style="width: 7%;text-align: center;">Hapus</th>
        </tr>
    </thead>
    <tbody>
        <?php
            //$sesArray = $this->session->all_userdata();
            
            //$sesIdpengguna = $sesArray['idpengguna'];
            $data = array(
                'idpasien',
                'kodepasien',
                'namalengkap',
                'alamat',
                'telp',
                'hp',
                'kelamin'
            );

            $this->db->select($data);
   
            $this->db->where('datastat !=', 'DELETED'); 
            $this->db->order_by("namalengkap", "asc");
            $query = $this->db->get("mstpasien");
            if ($query->num_rows() > 0)
            {
                 foreach ($query->result() as $row)
                    {
                       echo "<tr>";
                       echo "<td style=\"width: 11%;text-align: center\">
                                <a class=\"btn btn-sm btn-success\" onclick=\"getkartupasien('".base_url()."lappasien/idcard/$row->idpasien');\" href=\"javascript:void(0);\">"
                               . "<span class='glyphicon glyphicon-file'></span></a></td>";
                       echo "<td style=\"width: 15%;text-align: left\">$row->kodepasien</td>";
                       echo "<td style=\"width: 30%;text-align: left\">$row->namalengkap</td>";
                       echo "<td style=\"text-align: left\">$row->alamat</td>";
                       echo "<td style=\"width: 10%;text-align: left\">$row->telp</td>";
                       echo "<td style=\"width: 10%;text-align: left\">$row->hp</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-info\" href=\"javascript:void(0);\" onclick=\"tampil_form('$row->idpasien','$row->kelamin')\">Edit</a>
                            </td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-danger\" onclick=\"hapusdata('$row->idpasien','$row->namalengkap')\" href=\"javascript:void(0);\">Hapus</a></td>";
                       echo "</tr>";
                    }
            }
        ?>
    </tbody>                          
</table>
</div>

<div class="col-lg-12 input-control">
    <button type="button" class="btn btn-primary" onclick="tampil_form();">
        Tambah Data
    </button>
    <button type="button" class="btn btn-default" onclick="tampil_grid();">
        Refresh
    </button>
</div>            
        <?php
    }
}