<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class mspasiendefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
        
    public function prosesdata(){        
        $idpasien = $this->db->escape_str($this->input->post('idpasien'));
        $namalengkap = $this->db->escape_str($this->input->post('namalengkap'));
        $cmbkelamin = $this->db->escape_str($this->input->post('cmbkelamin'));
        $tmplahir = $this->db->escape_str($this->input->post('tmplahir'));
        $tgllahir = $this->db->escape_str($this->input->post('tgllahir'));
        $alamat = $this->db->escape_str($this->input->post('alamat'));
        
        $telp = $this->db->escape_str($this->input->post('telp'));
        $hp = $this->db->escape_str($this->input->post('hp'));
        $kerabatnama = $this->db->escape_str($this->input->post('kerabatnama'));
        $kerabatstatus = $this->db->escape_str($this->input->post('kerabatstatus'));
        $kerabattelp = $this->db->escape_str($this->input->post('kerabattelp'));
        
        $jenis = $this->db->escape_str($this->input->post('jenis'));
        $nmrbpjs = $this->db->escape_str($this->input->post('nmrbpjs'));
        $beratbadan = $this->db->escape_str($this->input->post('beratbadan'));
        $tinggibadan = $this->db->escape_str($this->input->post('tinggibadan'));
        $idpengguna = $this->session->userdata('idpengguna');
        
        $stat = $this->db->escape_str($this->input->post('stat'));
        
        // var_dump($tinggibadan);
        // exit();               
         $query = "call uspMstPasien ('$idpasien','$namalengkap','$cmbkelamin','$tmplahir','$tgllahir','$alamat'"
               . " ,'$telp','$hp','$kerabatnama','$kerabatstatus','$kerabattelp','$idpengguna','$jenis','$nmrbpjs','$beratbadan','$tinggibadan','$stat')";
               
        // echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             return true;
         }else{
            return false;
         }
    }
}
