<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class mspasiendefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
        
    public function prosesdata(){        
        $idpasien = $this->db->escape_str($this->input->post('idpasien'));
        $namalengkap = $this->db->escape_str($this->input->post('namalengkap'));
        $cmbkelamin = $this->db->escape_str($this->input->post('cmbkelamin'));
        $tmplahir = $this->db->escape_str($this->input->post('tmplahir'));
        $tgllahir = $this->db->escape_str($this->input->post('tgllahir'));
        $alamat = $this->db->escape_str($this->input->post('alamat'));
        
        $telp = $this->db->escape_str($this->input->post('telp'));
        $hp = $this->db->escape_str($this->input->post('hp'));
        $kerabatnama = $this->db->escape_str($this->input->post('kerabatnama'));
        $kerabatstatus = $this->db->escape_str($this->input->post('kerabatstatus'));
        $kerabattelp = $this->db->escape_str($this->input->post('kerabattelp'));
        
        $idpengguna = $this->session->userdata('idpengguna');
        
        $stat = $this->db->escape_str($this->input->post('stat'));
        
        $query = "call uspMstPasien ('$idpasien','$namalengkap','$cmbkelamin','$tmplahir','$tgllahir','$alamat'"
                . " ,'$telp','$hp','$kerabatnama','$kerabatstatus','$kerabattelp','$idpengguna','$stat')";
        //echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             return true;
         }else{
            return false;
         }
    }
}

/* generate by mr.pudyasto */
/* kerabatnama : mr.pudyasto@gmail.com */