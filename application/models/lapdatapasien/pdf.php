<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PDF extends FPDF
{
	function Header()
	{
            $owners = new owner();
            $this->setFont('Arial','',16);
            $this->Image(base_url().$owners->logo, '10','10', '15','13');
            $this->text(30,15,$owners->nama);
            $this->setFont('Arial','',12);
            $this->text(30,22,$owners->alamat.", ".$owners->kota);

            $this->Ln(20);
            $this->Line(10,25,210,25);
            
            $this->setFont('Arial','',12);
            $this->setFillColor(255,255,255);
            $this->cell(200,6,'LAPORAN DATA PASIEN',0,1,'C',1);
            $this->setFont('Arial','',9);
            $this->cell(200,6,'Periode '.date('M Y'),0,1,'C',1);
            
            $this->setFont('Arial','',7);
            $this->setFillColor(200,200,200);
            $this->cell(10,6,'NO.',1,0,'C',1);
            $this->cell(20,6,'KODE PASIEN',1,0,'C',1);
            $this->cell(40,6,'NAMA PASIEN',1,0,'C',1);
            $this->cell(80,6,'ALAMAT',1,0,'C',1);
            $this->cell(25,6,'NO. TELP',1,0,'C',1);
            $this->cell(25,6,'NO. HP',1,1,'C',1);
                
	}

	function Content()
	{
            $ya = 46;
            $row = 6;
            $no = 1;
            $query ="SELECT kodepasien, namalengkap,alamat,telp, hp FROM mstpasien";	
            $result=  mysql_query($query);
            while($rMK = mysql_fetch_array($result)){
                $this->setFont('Arial','',7);
                $this->setFillColor(255,255,255);	
                $this->cell(10,6,$no,1,0,'C',1);
                $this->cell(20,6,$rMK[0],1,0,'C',1);
                $this->cell(40,6,$rMK[1],1,0,'L',1);
                $this->cell(80,6,$rMK[2],1,0,'L',1);
                $this->cell(25,6,$rMK[3],1,0,'C',1);
                $this->cell(25,6,$rMK[4],1,1,'C',1);
                $ya = $ya + $row;
                $no++;
            }
	}

	function Footer()
	{
            $owners = new owner();
            $this->SetY(-15);
            $this->Line(10,$this->GetY(),210,$this->GetY());
            $this->SetFont('Arial','I',9);
            $this->Cell(0,10,$owners->nama,0,0,'L');
            $this->Cell(0,10,'Halaman '.$this->PageNo().' dari {nb}',0,0,'R');
	}
}

$pdf = new PDF();
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->Content();
$pdf->Output();
/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * Website : bmediadata.com
 */