<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridlapdatapasien extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
            <script>
                $(document).ready(function() {
                    $('#tbgridlapdatapasien').dataTable({
                        "bProcessing": true,
                        "bServerSide": true,
                        "sAjaxSource": "<?=base_url() ?>lapdatapasien/datapasien",
                        "columns": [
                            {"width": "15%" },
                            null,
                            null,
                            {"width": "10%" },
                            {"width": "10%" }
                        ],
                        "oLanguage": {
                            "sProcessing": "<div class=\"col-lg-12\"><h5><label class=\"label label-danger\">Silahkan tunggu, sedang mengambil data</label><h5></div>"
                        }
                    });
                    $('#tbgridlapdatapasien').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div class="col-lg-12">
    <div style="margin-bottom: 20px;">
<a href="<?=base_url() ?>lapdatapasien/excel" title="Cetak ke Excel" class="exporticon" target="_blank">
    <?php echo $this->icons->xls('24','24');?> Cetak ke Excel
</a>      
<a href="<?=base_url() ?>lapdatapasien/pdf" title="Cetak ke Excel" class="exporticon" target="_blank">
    <?php echo $this->icons->pdf('24','24');?> Cetak ke Pdf
</a>         
    </div>    
<table class="table table-striped table-bordered table-hover" id="tbgridlapdatapasien">
    <thead>
        <tr class="danger">
            <th style="width: 15%;text-align: center;">Kode Pasien</th>
            <th style="text-align: center;">Nama Pasien</th>
            <th style="width: 10%;text-align: center;">Alamat</th>
            <th style="width: 10%;text-align: center;">Telp.</th>
            <th style="width: 10%;text-align: center;">No. HP</th>
        </tr>
    </thead>                        
</table>   
</div>           
        <?php
    }
}