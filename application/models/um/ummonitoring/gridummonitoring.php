<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridummonitoring extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
<script>
    $(document).ready(function() {
        $('#tbummonitor').dataTable();
        $('#tbummonitor').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });
    });
</script>

<div class="col-lg-12">
<table class="table table-striped table-bordered table-hover" id="tbummonitor">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Nama Pengguna</th>
            <th style="text-align: center;">IP Adress</th>       
            <th style="text-align: center;">Browser</th> 
            <th style="text-align: center;">Platform</th> 
            <th style="text-align: center;">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
            error_reporting(0);
            $data = array(
                'session_id',
                'ip_address',
                'user_agent',
                'last_activity',
                'user_data'
            );
            $this->db->select($data);
            $query = $this->db->get("db_sessions");
            //$query = $this->db->select('user_data')->get('ci_sessions');

        foreach ($query->result() as $row)
        {
            $udata = unserialize($row->user_data);
            if(!empty($udata['namapengguna'])){
                echo "<tr>";
                echo "<td style=\"width: 15%;text-align: left\">".$udata['namapengguna']."</td>";
                echo "<td style=\"width: 10%;text-align: left\">".$row->ip_address."</td>";
                echo "<td style=\"text-align: left\">".$udata['browser']."</td>";
                echo "<td style=\"width: 15%;text-align: left\">".$udata['platform']."</td>";
                echo "<td style=\"width: 7%;text-align: center\"><a class=\"btn btn-sm btn-danger\" href=\"javascript:void(0);\" onclick=\"logout('$row->session_id');\">Logout</a></td>";
                echo "</tr>";   
            }
            /* put data in array using username as key */
        }
        ?>
    </tbody>                          
</table>
</div>

<div class="col-lg-12 input-control">
    <button type="button" class="btn btn-default" onclick="tampil_grid();">
        Refresh
    </button>
</div>            
        <?php
    }
}