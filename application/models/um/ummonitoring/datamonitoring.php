<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class datamonitoring extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){        
        $query ="SELECT al.* FROM (SELECT a.`pengguna`,CONCAT(a.`aktifitas`,'@',a.tanggallog) aktifitas,a.`komputerip`,a.`browsertipe`,a.`perangkattipe` FROM `logpengguna` a 
                ORDER BY a.tanggallog DESC) al ";
	if (isset($_GET['filterscount']))
	{
		$filterscount = $_GET['filterscount'];
		if ($filterscount > 0)
		{
			$where = " WHERE (";
			$tmpdatafield = "";
			$tmpfilteroperator = "";
			for ($i=0; $i < $filterscount; $i++)
		    {
				$filtervalue = $_GET["filtervalue" . $i];
				$filtercondition = $_GET["filtercondition" . $i];
				$filterdatafield = $_GET["filterdatafield" . $i];
				$filteroperator = $_GET["filteroperator" . $i];
				if ($tmpdatafield == "")
				{
					$tmpdatafield = $filterdatafield;			
				}
				else if ($tmpdatafield <> $filterdatafield)
				{
					$where .= ")AND(";
				}
				else if ($tmpdatafield == $filterdatafield)
				{
					if ($tmpfilteroperator == 0)
					{
						$where .= " AND ";
					}
					else $where .= " OR ";	
				}
				switch($filtercondition)
				{
					case "CONTAINS":
						$where .= " " . $filterdatafield . " LIKE '%" . $filtervalue ."%'";
						break;
					case "DOES_NOT_CONTAIN":
						$where .= " " . $filterdatafield . " NOT LIKE '%" . $filtervalue ."%'";
						break;
					case "EQUAL":
						$where .= " " . $filterdatafield . " = '" . $filtervalue ."'";
						break;
					case "NOT_EQUAL":
						$where .= " " . $filterdatafield . " <> '" . $filtervalue ."'";
						break;
					case "GREATER_THAN":
						$where .= " " . $filterdatafield . " > '" . $filtervalue ."'";
						break;
					case "LESS_THAN":
						$where .= " " . $filterdatafield . " < '" . $filtervalue ."'";
						break;
					case "GREATER_THAN_OR_EQUAL":
						$where .= " " . $filterdatafield . " >= '" . $filtervalue ."'";
						break;
					case "LESS_THAN_OR_EQUAL":
						$where .= " " . $filterdatafield . " <= '" . $filtervalue ."'";
						break;
					case "STARTS_WITH":
						$where .= " " . $filterdatafield . " LIKE '" . $filtervalue ."%'";
						break;
					case "ENDS_WITH":
						$where .= " " . $filterdatafield . " LIKE '%" . $filtervalue ."'";
						break;
				}
								
				if ($i == $filterscount - 1)
				{
					$where .= ")";
				}
				$tmpfilteroperator = $filteroperator;
				$tmpdatafield = $filterdatafield;			
			}
			$query = "SELECT al.* FROM (SELECT a.`pengguna` namapengguna,CONCAT(a.`aktifitas`,'@',a.tanggallog) aktifitas,a.`komputerip`,a.`browsertipe`,a.`perangkattipe` FROM `logpengguna` a 
                                    ORDER BY a.tanggallog DESC) al " . $where;			
		}
	}
	$result = mysql_query($query);
	$orders = array();
	while ($row = mysql_fetch_array($result)) {
		$orders[] = array(
			'namapengguna' => $row['0'],
			'aktifitas' => $row['1'],
			'komputerip' => $row['2'],
                        'browsertipe' => $row['3'],
			'perangkattipe' => $row['4']
		  );
	}
  
	$data=json_encode($orders);
	print json_encode($orders);        
    }
}