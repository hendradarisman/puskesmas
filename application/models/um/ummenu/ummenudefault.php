<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ummenudefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function prosesdata(){
        $idmenu = $this->security->xss_clean($this->input->post('idmenu'));
        $namamenu = $this->security->xss_clean($this->input->post('namamenu'));
        $ketmenu = $this->security->xss_clean($this->input->post('ketmenu'));
        $stat = $this->security->xss_clean($this->input->post('stat'));
        
        //$idpengguna = $this->session->userdata('idpengguna');
        
        $this->db->simple_query("START TRANSACTION;"); 
        $query = "call uspMstMenu ('$idmenu','$namamenu','$ketmenu','$stat');";
        $resl = $this->db->simple_query($query); 
         if($resl){
             $this->db->simple_query("COMMIT;"); 
             return true;
         }else{
            $this->db->simple_query("ROLLBACK;");
            return false;
         }
    }
    
    public function simpanmodul(){
        $txtIdModul = $this->security->xss_clean($this->input->post('txtIdModul'));
        $txtIdMenu = $this->security->xss_clean($this->input->post('txtIdMenu'));
        $namamodul = $this->security->xss_clean($this->input->post('namamodul'));
        $kelasmodul = $this->security->xss_clean($this->input->post('kelasmodul'));
        $ketmodul = $this->security->xss_clean($this->input->post('ketmodul'));
        $stat = $this->security->xss_clean($this->input->post('stat'));
        
        //$idpengguna = $this->session->userdata('idpengguna');
        
        $this->db->simple_query("START TRANSACTION;"); 
        $query = "call uspMstModul ('$txtIdModul','$txtIdMenu','$namamodul','$kelasmodul','$ketmodul','$stat');";
        $resl = $this->db->simple_query($query); 
         if($resl){
             $this->db->simple_query("COMMIT;"); 
             return true;
         }else{
            $this->db->simple_query("ROLLBACK;");
            return false;
         }
    }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */