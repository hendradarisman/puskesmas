<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridummodul extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
            $idmenu = $this->security->xss_clean($this->input->post('idmenu'));
            $namamenu = $this->security->xss_clean($this->input->post('namamenu'));
        ?>
            <script>
                $(document).ready(function() {
                    $('#tbUmModul').dataTable();
                    $('#tbUmModul').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>
<input type="hidden" id="idMenu-UmModul" value="<?php print $idmenu;?>" />
<div class="col-lg-12">  
<div class="bs-callout bs-callout-info">
    <h4>
        <strong>Menu <?php print $namamenu; ?></strong>
    </h4>
    <h5>
        Susun modul pada menu <?php print $namamenu; ?>.
    </h5>
</div>
<table class="table table-striped table-bordered table-hover" id="tbUmModul">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Nama Modul</th>
            <th style="text-align: center;">Kelas Modul</th>
            <th style="text-align: center;">Keterangan</th>
            <th style="text-align: center;">Edit</th>           
            <th style="text-align: center;">Hapus</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $data = array(
                'idmodul',
                'namamodul',
                'kelasmodul',
                'keterangan'
            );
            
            $this->db->select($data);
            $this->db->where('datastat !=', 'DELETED'); 
            $this->db->where('idmenu =', $idmenu);
            $query = $this->db->get("mstmodul");
            if ($query->num_rows() > 0)
            {
                 foreach ($query->result() as $row)
                    {
                       echo "<tr>";
                       echo "<td style=\"width: 30%;text-align: left\">$row->namamodul</td>";
                       echo "<td style=\"width: 10%;text-align: left\">$row->kelasmodul</td>";
                       echo "<td>$row->keterangan</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-info\" href=\"javascript:void(0);\" data-backdrop=\"static\" data-keyboard=\"false\" data-toggle=\"modal\" data-target=\"#mdModul\" 
                                onclick=\"isidatamodul('$row->idmodul','$row->namamodul','$row->kelasmodul','$row->keterangan')\">Edit</a>
                            </td>";
                       echo "<td  style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-danger\" onclick=\"hapusdatamodul('$row->idmodul','$row->namamodul')\" href=\"javascript:void(0);\">Hapus</a></td>";
                       echo "</tr>";
                    }
            }
        ?>
    </tbody>                          
</table>
</div>

<div class="col-lg-12 input-control">
    <button type="button" class="btn btn-primary" onclick="tampil_form_modul();" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#mdModul">
        Tambah Modul
    </button>
    <button type="button" class="btn btn-default" onclick="tampil_grid();">
        Batal
    </button>
    <button type="button" class="btn btn-default" onclick="susunmodul('<?php print $idmenu;?>','<?php print $namamenu;?>');">
        Refresh
    </button>
</div>     

<div class="modal fade" id="mdModul" tabindex="-1" role="dialog" aria-labelledby="mdModulLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="mdModulLabel">Tambah Modul Sistem</h4>
      </div>
      <div class="modal-body">
                 <form role="form">
                  <div class="form-group">
                    <label for="namamenu">Nama Modul </label>
                    <input type="hidden" class="form-control" id="txtIdModul" />
                    <input type="hidden" class="form-control" id="txtIdMenu" value="<?php print $idmenu;?>" />
                    <input type="text" class="form-control" id="namamodul" placeholder="Masukkan Nama Modul">
                  </div>
                    <div class="form-group">
                    <label for="namamenu">Kelas Modul </label>
                    <input type="text" class="form-control" id="kelasmodul" placeholder="Masukkan Nama Kelas Modul">
                  </div>
                  <div class="form-group">
                    <label for="ketmenu">Keterangan (opsional) </label>
                    <textarea style="resize: none;" class="form-control" id="ketmodul" placeholder="Masukkan Keterangan"></textarea>
                  </div>
                </form>         
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="simpandatamodul();" data-dismiss="modal">Simpan</button>
          <button type="button" class="btn btn-default" onclick="kosongkandatamodul();" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>
        <?php
    }
}