<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridumgrup extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
            <script>
                $(document).ready(function() {
                    $('#tbUmGrup').dataTable();
                    $('#tbUmGrup').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div class="col-lg-12">
<table class="table table-striped table-bordered table-hover" id="tbUmGrup">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Hak Akses</th>
            <th style="text-align: center;">Nama Grup</th>
            <th style="text-align: center;">Keterangan</th>
            <th style="text-align: center;">Edit</th>           
            <th style="text-align: center;">Hapus</th>
        </tr>
    </thead>
    <tbody>
        <?php
            //$sesArray = $this->session->all_userdata();
            //$sesIdpengguna = $sesArray['idpengguna'];
            $data = array(
                'idgrup',
                'namagrup',
                'keterangan'
            );
            
            $this->db->select($data);
            $this->db->where('datastat !=', 'DELETED'); 
            $this->db->order_by("namagrup", "asc");
            $query = $this->db->get("mstgrup");
            if ($query->num_rows() > 0)
            {
                 foreach ($query->result() as $row)
                    {
                       echo "<tr>";
                       
                       echo "<td style=\"width: 15%;text-align: center\">"
                                . "<a title=\"Set hak akses pada grup ".$row->namagrup."\" "
                               . "href=\"".base_url()."umgrup/hakakses/".str_replace(' ', '_', $row->namagrup)."/".$row->idgrup."\" class=\"btn btn-success btn-sm\" >Hak Akses</a>"
                          . "</td>";
                       
                       echo "<td style=\"width: 30%;text-align: left\">$row->namagrup</td>";
                       echo "<td>$row->keterangan</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-info\" href=\"javascript:void(0);\" onclick=\"isidata('$row->idgrup','$row->namagrup','$row->keterangan')\">Edit</a>
                            </td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-danger\" onclick=\"hapusdata('$row->idgrup','$row->namagrup')\" href=\"javascript:void(0);\">Hapus</a></td>";
                       echo "</tr>";
                    }
            }
        ?>
    </tbody>                          
</table>
</div>

<div class="col-lg-12 input-control">
    <button type="button" class="btn btn-primary" onclick="tampil_form();">
        Tambah Data
    </button>
    <button type="button" class="btn btn-default" onclick="tampil_grid();">
        Refresh
    </button>
</div>            
        <?php
    }
}