<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class hakasesumgrup extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
?>
<script>
    $(document).ready(function() {
        $('#tbHakaksesUmGrup').dataTable({
            "bLengthChange": false,
            "bPaginate": false
        });
        $('#tbHakaksesUmGrup').tooltip({
            selector: "[data-toggle=tooltip]",
            container: "body"
        });
    });
</script>
<table class="table table-striped table-bordered table-hover" id="tbHakaksesUmGrup">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Pilih</th>
            <th style="text-align: center;">Nama Menu</th>
            <th style="text-align: center;">Nama Modul</th>
            <th style="text-align: center;">Tambah</th>
            <th style="text-align: center;">Edit</th>           
            <th style="text-align: center;">Hapus</th>
            <th style="text-align: center;">Cetak</th>
        </tr>
    </thead>
    <tbody>
        <?php
        
            $grupid = $this->security->xss_clean($this->input->post('grupid'));            
            $data = array(
                'mstmodul.idmodul',
                'mstmenu.namamenu',
                'mstmodul.namamodul'
            );
            
            $this->db->select($data);
            $this->db->join('mstmodul', 'mstmodul.idmenu = mstmenu.idmenu');
            $this->db->order_by("mstmenu.namamenu", "asc");
            $this->db->order_by("mstmodul.namamodul", "asc");
            $query = $this->db->get("mstmenu");
            if ($query->num_rows() > 0)
            {
                 foreach ($query->result() as $row)
                    {
                       echo "<tr>";
                       echo "<td style=\"width: 7%;text-align: center\">"
                            . "<input type=\"checkbox\" ".$this->hakasesumgrup->cekmodul($grupid,$row->idmodul)." "
                            . "id=\"".$row->idmodul."\" "
                            . "name=\"".$row->idmodul."\" "
                            . "onclick=\"modulact('".$row->idmodul."','".$grupid."');\"/>"
                            . "</td>";
                       echo "<td style=\"width: 20%;text-align: left\">$row->namamenu</td>";
                       echo "<td>$row->namamodul</td>";
                       
                       $akses = $this->hakasesumgrup->cekakses($grupid,$row->idmodul);
                       
                       echo "<td style=\"width: 75px;text-align: center\">"
                            . "<input type=\"checkbox\" ".$this->hakasesumgrup->cektambah($akses)." "
                            . "id=\"T".$row->idmodul."\" "
                            . "name=\"T".$row->idmodul."\" "
                            . "onclick=\"aksesact('".$row->idmodul."','".$grupid."');\"/>"
                            . "</td>";
                       echo "<td style=\"width: 75px;text-align: center\">"
                            . "<input type=\"checkbox\" ".$this->hakasesumgrup->cekedit($akses)." "
                            . "id=\"E".$row->idmodul."\" "
                            . "name=\"E".$row->idmodul."\" "
                            . "onclick=\"aksesact('".$row->idmodul."','".$grupid."');\"/>"
                            . "</td>";
                       echo "<td style=\"width: 75px;text-align: center\">"
                            . "<input type=\"checkbox\" ".$this->hakasesumgrup->cekhapus($akses)." "
                            . "id=\"H".$row->idmodul."\" "
                            . "name=\"H".$row->idmodul."\" "
                            . "onclick=\"aksesact('".$row->idmodul."','".$grupid."');\"/>"
                            . "</td>";
                       echo "<td style=\"width: 75px;text-align: center\">"
                            . "<input type=\"checkbox\" ".$this->hakasesumgrup->cekcetak($akses)." "
                            . "id=\"C".$row->idmodul."\" "
                            . "name=\"C".$row->idmodul."\" "
                            . "onclick=\"aksesact('".$row->idmodul."','".$grupid."');\"/>"
                            . "</td>";
                       echo "</tr>";
                    }
            }
        ?>
    </tbody>                          
</table>   
<?php
    }
      
    public function cekmodul($idgrup,$idmodul){
          $data = array(
                'idtranaksesgrup'
            );
            $this->db->select($data);
            $this->db->where('idgrup',$idgrup);
            $this->db->where('idmodul',$idmodul);            
            $query = $this->db->get("tranaksesgrup");
            if ($query->num_rows() > 0)
            {
                return "checked";
            }else{
                return "";
            }
      }
      
      public function cekakses($idgrup,$idmodul){
          $data = array(
                'hakakses'
            );
            $this->db->select($data);
            $this->db->where('idgrup',$idgrup);
            $this->db->where('idmodul',$idmodul);            
            $query = $this->db->get("tranaksesgrup");
            if ($query->num_rows() > 0)
            {
                foreach ($query->result() as $row)
                    {
                        return $row->hakakses;
                    }
            }else{
                return "";
            }
      }
      
      public function cektambah($hakses){
          if(substr($hakses,0,1)=="Y"){
               return "checked";
           }else{
               return "";
           }
      }
      
      public function cekedit($hakses){
          if(substr($hakses,2,1)=="Y"){
               return "checked";
           }else{
               return "";
           }
      }
      
      public function cekhapus($hakses){
          if(substr($hakses,4,1)=="Y"){
               return "checked";
           }else{
               return "";
           }
      }
      
      public function cekcetak($hakses){
          if(substr($hakses,6,1)=="Y"){
               return "checked";
           }else{
               return "";
           }
      }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */