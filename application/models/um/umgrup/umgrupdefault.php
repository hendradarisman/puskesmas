<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class umgrupdefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function prosesdata(){
        $idgrup = $this->security->xss_clean($this->input->post('idgrup'));
        $namagrup = $this->security->xss_clean($this->input->post('namagrup'));
        $ketgrup = $this->security->xss_clean($this->input->post('ketgrup'));
        $stat = $this->security->xss_clean($this->input->post('stat'));
        
        //$idpengguna = $this->session->userdata('idpengguna');
        
        $this->db->simple_query("START TRANSACTION;"); 
        $query = "call uspMstGrup ('$idgrup','$namagrup','$ketgrup','$stat');";
        $resl = $this->db->simple_query($query); 
         if($resl){
             $this->db->simple_query("COMMIT;"); 
             return true;
         }else{
            $this->db->simple_query("ROLLBACK;");
            return false;
         }
    }
     
    public function setakses(){
        $grup_id = $this->security->xss_clean($this->input->post('grup_id'));
        $modul_id = $this->security->xss_clean($this->input->post('modul_id'));
        $akses = $this->security->xss_clean($this->input->post('akses'));
        $stat = $this->security->xss_clean($this->input->post('stat'));
        
        //$idpengguna = $this->session->userdata('idpengguna');
        $this->db->simple_query("START TRANSACTION;"); 
        $query = "call uspTranAksesGrup ('$grup_id','$modul_id','$akses','$stat');";
        $resl = $this->db->simple_query($query); 
         if($resl){
             $this->db->simple_query("COMMIT;"); 
             return true;
         }else{
            $this->db->simple_query("ROLLBACK;");
            return false;
         }
    }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */