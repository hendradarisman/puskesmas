<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridumpengguna extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
            <script>
                $(document).ready(function() {
                    $('#tbUmPengguna').dataTable();
                    $('#tbUmPengguna').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div class="col-lg-12">
<table class="table table-striped table-bordered table-hover" id="tbUmPengguna">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Nama Grup</th>
            <th style="text-align: center;">Nama Pengguna</th>
            <th style="text-align: center;">Edit</th>           
            <th style="text-align: center;">Hapus</th>
        </tr>
    </thead>
    <tbody>
        <?php
            //$sesArray = $this->session->all_userdata();
            
            //$sesIdpengguna = $sesArray['idpengguna'];
            $data = array(
                'mstpengguna.idpengguna',
                'mstpengguna.namapengguna',
                'mstpengguna.katakunci',
                'mstpengguna.idgrup',
                'mstgrup.namagrup'
            );

            $this->db->select($data);
            $this->db->join('mstgrup', 'mstgrup.idgrup = mstpengguna.idgrup');        
            $this->db->where('mstpengguna.stataktif !=', 'D'); 
            $this->db->where('mstpengguna.namapengguna !=', 'sadmin'); 
            $this->db->order_by("mstpengguna.namapengguna", "asc");
            $query = $this->db->get("mstpengguna");
            if ($query->num_rows() > 0)
            {
                 foreach ($query->result() as $row)
                    {
                       echo "<tr>";
                       echo "<td style=\"width: 30%;text-align: left\">$row->namagrup</td>";
                       echo "<td>$row->namapengguna</td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-info\" href=\"javascript:void(0);\" onclick=\"tampil_form('$row->idpengguna')\">Edit</a>
                            </td>";
                       echo "<td style=\"width: 7%;text-align: center\">
                                <a class=\"btn btn-sm btn-danger\" onclick=\"hapusdata('$row->idpengguna','$row->namapengguna')\" href=\"javascript:void(0);\">Hapus</a></td>";
                       echo "</tr>";
                    }
            }
        ?>
    </tbody>                          
</table>
</div>

<div class="col-lg-12 input-control">
    <button type="button" class="btn btn-primary" onclick="tampil_form();">
        Tambah Data
    </button>
    <button type="button" class="btn btn-default" onclick="tampil_grid();">
        Refresh
    </button>
</div>            
        <?php
    }
}