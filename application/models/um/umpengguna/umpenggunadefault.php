<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class umpenggunadefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function getpassword(){
        $idpengguna = $this->security->xss_clean($this->input->post('idpengguna'));
        $this->db->where('idpengguna', $idpengguna);
        $this->db->where('datastat !=', 'DELETED');
        $query = $this->db->get("mstpengguna");
        if($query->num_rows == 1)
        {
            $row = $query->row();
            $msg = $row->katakunci;
            $pass = $this->encrypt->decode($msg);
        }
        print $pass;
    }
    
    public function prosesdata(){        
        $idpengguna = $this->security->xss_clean($this->input->post('idpengguna'));
        $idperson = $this->security->xss_clean($this->input->post('idperson'));
        $namapengguna = $this->security->xss_clean($this->input->post('namapengguna'));
        $katakunci = $this->encrypt->encode($this->input->post('katakunci'));//md5($this->input->post('katakunci')); //
        $cmbgruppengguna = $this->security->xss_clean($this->input->post('cmbgruppengguna'));
        $stat = $this->security->xss_clean($this->input->post('stat'));
        
        //$idpengguna = $this->session->userdata('idpengguna');
        $query = "call uspMstPengguna ('$idpengguna','$cmbgruppengguna','$idperson','$namapengguna','$katakunci','$stat')";
        //echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             return true;
         }else{
            return false;
         }
    }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */