<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class formumpengguna extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function getname($param) {
        $query = "SELECT a.* FROM ( SELECT `iddokter` idperson, 'Dokter' kategori, `namalengkap` FROM `mstdokter` 
                                    UNION 
                                    SELECT `idadmin` idperson, 'Admin' kategori, `namalengkap` FROM `mstadmin`  ) a where a.idperson = '$param'";
        $result=  mysql_query($query);
        $nrow=  mysql_num_rows($result);
        if($nrow>0){
            $row=mysql_fetch_array($result);
            return $row[2];
        }
        else{
            return false;
        }
    }
    
    public function loadform(){
        error_reporting(0);
        $idpengguna = $this->security->xss_clean($this->input->post('idpengguna'));
        $idgrup = "";
        $this->db->where('idpengguna', $idpengguna);
        $this->db->where('datastat !=', 'DELETED');
        $query = $this->db->get("mstpengguna");
        if($query->num_rows>0)
        {
            $row = $query->row();
            $idpengguna = $row->idpengguna;
            $idgrup = $row->idgrup;
            $idperson = $row->idperson;
            $namapengguna = $row->namapengguna;
            $msg = $row->katakunci;
            $pass = $this->encrypt->decode($msg);
        }
?>
<form role="form">
    <div class="col-lg-4">
            <div class="form-group">
                <label for="namadepan">Nama Lengkap </label>
                <input type="hidden" class="form-control" id="idpengguna" value="<?php echo $idpengguna;?>">
                <input type="hidden" class="form-control" id="idperson" value="<?php echo $idperson;?>">
                <?php
                    if(empty($idpengguna)){
                ?>
                <div class="input-group">
                    <input type="text" disabled="" class="form-control" id="namalengkap" placeholder="Pilih pengguna">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-info" data-backdrop="static" data-toggle="modal" data-target="#myModal">Cari</button>
                    </div>
                </div>                
                <?php
                    }else{
                ?>
                <input type="text" disabled="" class="form-control" id="namalengkap" placeholder="Pilih pengguna" value="<?php echo $this->formumpengguna->getname($idperson);?>">
                <?php
                    }
                ?>
            </div>


            <div class="form-group">
                <label for="namapengguna">Nama Pengguna </label>
                <input type="text" class="form-control" id="namapengguna" placeholder="Masukkan Nama Pengguna" value="<?php echo $namapengguna;?>">
                <div id="result0"></div>
            </div>

            <div class="form-group">
                <label for="katakunci">Password </label>
                <input style="max-width: 250px;" type="password" class="form-control" id="katakunci" placeholder="Masukkan Password" value="<?php echo $pass;?>">
                <div id="result1"></div>
            </div>

            <div class="form-group">
                <label for="cmbgruppengguna">Grup Pengguna</label>
                <select style="max-width: 250px;" class="form-control" id="cmbgruppengguna">
            <?php
                if(empty($idgrup)){
                    echo "<option selected=\"selected\" value=\"\">-- Pilih Grup Pengguna --</option>";
                }else{
                    $data = array(
                    'idgrup',
                    'namagrup'
                    );

                    $this->db->select($data);           
                    $this->db->where('datastat !=', 'DELETED'); 
                    $this->db->where('idgrup', $idgrup); 
                    $this->db->order_by("namagrup", "asc");
                    $query = $this->db->get("mstgrup");
                    if ($query->num_rows() > 0)
                    {
                         foreach ($query->result() as $row)
                            {
                               echo "<option selected=\"selected\" value=\"".$row->idgrup."\">".$row->namagrup."</option>";
                            }
                    }
                    
                }
                $sesArray = $this->session->all_userdata();
                $sidgrup = $sesArray['idgrup'];
                $data = array(
                    'idgrup',
                    'namagrup'
                );

                $this->db->select($data);           
                $this->db->where('datastat !=', 'DELETED'); 
                $this->db->where('idgrup !=', $idgrup); 
                $this->db->order_by("namagrup", "asc");
                $query = $this->db->get("mstgrup");
                if ($query->num_rows() > 0)
                {
                     foreach ($query->result() as $row)
                        {
                           echo "<option value=\"".$row->idgrup."\">".$row->namagrup."</option>";
                        }
                }
            ?>              
                </select>
            </div>

            <button type="button" class="btn btn-primary" onclick="simpandata();">
                Simpan
            </button>
            <button type="button" class="btn btn-default" onclick="tampil_grid();">
                Batal
            </button>

    </div>
</form>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Dokter atau Admin Pengguna</h4>
      </div>
      <div class="modal-body">

          
<script>
                $(document).ready(function() {
                    $('#tbMsDokter').dataTable();
                    $('#tbMsDokter').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div style="min-height: 400px;min-width: 500px;">
<table class="table table-striped table-bordered table-hover" id="tbMsDokter">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Kategori</th>
            <th style="text-align: center;">Nama Lengkap</th>
            <th style="text-align: center;">Edit</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $query = "SELECT a.* FROM ( SELECT `iddokter` idperson, 'Dokter' kategori, `namalengkap` FROM `mstdokter` 
                                        UNION 
                                        SELECT `idadmin` idperson, 'Admin' kategori, `namalengkap` FROM `mstadmin` ) a order by a.namalengkap asc";
            $result=  mysql_query($query);
            while($row=mysql_fetch_array($result)){
               echo "<tr>";
               echo "<td style=\"width: 20%;text-align: left\">$row[1]</td>";
               echo "<td style=\"text-align: left\">$row[2]</td>";
               echo "<td style=\"width: 7%;text-align: center\">
                        <a class=\"btn btn-sm btn-success\" data-dismiss=\"modal\" href=\"javascript:void(0);\" onclick=\"add_pengguna('$row[0]','".str_replace("'", "\'", $row[2])."')\">Pilih</a>
                    </td>";
               echo "</tr>";
            }
        ?>
    </tbody>                          
</table>
</div>
          
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>

<script>
    $(document).ready(function() {
        $('#namapengguna').blur(function(){
            if($('#namapengguna').val()!==""){
                var nmuser=$('#namapengguna').val();
                var idpengguna=$('#idpengguna').val();
                $.ajax({
                        type: "POST",
                        url: "<?php echo base_url().'umpengguna/cekpengguna'; ?>",
                        data: {"nmuser":nmuser,"idpengguna":idpengguna},
                        success: function(resp){
                            if(resp==1){
                                userstat = 0;
                                $('#result0').html('<span class="label label-danger">Nama pengguna sudah digunakan</span>');
                            }else{
                                userstat = 1;
                                $('#result0').html('<span class="label label-primary">Nama pengguna Tersedia</span>');
                            }    
                        }
                });
            }else{
                $('#result0').html('');
            }
        });
        
        $('#katakunci').keyup(function(){
                if($('#katakunci').val()!==""){
                    $('#result1').html(checkStrength($('#katakunci').val()));
                }else{
                    $('#result1').html("");
                }
            });
            
            
        function checkStrength(password){
            var strength = 0;
            if (password.length > 1) strength += 1;
            if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1;
            if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1;
            if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1;
            if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/)) strength += 1;
            if (strength < 2 ) {
                $('#result').removeClass();
                $('#result').addClass('weak');
                return '<span class="label label-danger">Lemah</span>';           
            } else if (strength == 2 ) {
                $('#result').removeClass();
                $('#result').addClass('good');
                return '<span class="label label-primary">Cukup</span>';       
            } else {
                $('#result').removeClass();
                $('#result').addClass('strong');
                return '<span class="label label-success">Kuat</span>';
            }
        }
    })
</script>
<?php
    }

}
