<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class cekpengguna extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function validate(){
        $username = $this->security->xss_clean($this->input->post('nmuser'));
        $idpengguna = $this->security->xss_clean($this->input->post('idpengguna'));
        $this->db->where('namapengguna', $username);
        $this->db->where('idpengguna !=', $idpengguna);
        $query = $this->db->get("mstpengguna");
        if($query->num_rows == 1)
        {
            return true;
        }
        return false;
    }
}