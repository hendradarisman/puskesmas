<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class tranregistrasidefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function batalkanpasien(){        
        $sesArray = $this->session->all_userdata();
        
        $idregistrasi = $this->db->escape_str($this->input->post('idregistrasi'));
        $idpengguna = $sesArray['idpengguna'];
        
        $query = "UPDATE `tranregistrasi`
                    SET `tglupdate` = NOW(),
                      `idpengguna` = '$idpengguna',
                      `statregistrasi` = 'CANCELED'
                    WHERE `idregistrasi` = '$idregistrasi'";
        //echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             return true;
         }else{
            return false;
         }
    }
    
    public function prosesdata(){        
        $sesArray = $this->session->all_userdata();
        
        $idregistrasi = $this->db->escape_str($this->input->post('idregistrasi'));
        $idpasien = $this->db->escape_str($this->input->post('idpasien'));
        $tgljanji = $this->db->escape_str($this->input->post('tgljanji'));
        $keluhan = $this->db->escape_str($this->input->post('keluhan'));
        $idpengguna = $sesArray['idpengguna'];
        $statregistrasi = $this->db->escape_str($this->input->post('statregistrasi'));
        
        $query = "call uspTranRegistrasi ('$idregistrasi','$tgljanji','$keluhan','$idpasien','$idpengguna','$statregistrasi')";
        //echo $query;
        $resl = $this->db->simple_query($query); 
         if($resl){
             return true;
         }else{
            return false;
         }
    }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */