<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class formtranregistrasi extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function getkeluhanpasien() {
            $param = $this->security->xss_clean($this->input->post('idregistrasi'));
            $stat = $this->security->xss_clean($this->input->post('stat'));
            $query = "SELECT  b.`namalengkap`,b.`alamat`,b.`hp`,a.`noantrian`,DATE_FORMAT(a.tglregistrasi,'%d %M %Y %H:%i') tglregistrasi,a.`keluhan`
                        FROM `tranregistrasi` a INNER JOIN mstpasien b ON a.`idpasien` = b.`idpasien`
                      WHERE idregistrasi = '$param'";
            $result=  mysql_query($query);
            $jml=  mysql_num_rows($result);
            if($jml>0){
            $row=mysql_fetch_array($result);
                if($stat == 'batalkan'){
                    ?>
                    <center>
                        <h4>Batalkan antrian untuk pasien <?php echo $row[0]; ?> ?</h4>
                        <button onclick="batalkan('<?php echo $param;?>');" type="button" class="btn btn-warning btn-sm" data-dismiss="modal">
                            Ya
                        </button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                            Tidak
                        </button>
                    </center>
                    <?php
                }else{
                    echo "<h2><label class=\"label label-info\">Nomor Antrian : $row[3]</label></h2>";
                                echo "<h3>$row[0]</h3>";
                                echo "<h4>$row[2]</h4>";
                                echo "<h5>$row[1]</h5>";
                    ?>
                    <div class="panel panel-danger">
                      <div class="panel-heading">
                        <h3 class="panel-title">Keluhan Pasien</h3>
                      </div>
                      <div class="panel-body">
                        <h4><?php echo $row[5];?></h4>
                      </div>
                    </div>        
                    <?php       
                    echo "<label class=\"label label-danger\">Tanggal Registrasi : ".$row[4]."</label>";                
                }
            }
    }
    
    public function getdetailpasien() {
            $param = $this->security->xss_clean($this->input->post('kdpasien'));
            $query = "SELECT idpasien, kodepasien, namalengkap, alamat
                        FROM mstpasien WHERE datastat<>'DELETED' and kodepasien ='$param'";
            $result=  mysql_query($query);
            $jml=  mysql_num_rows($result);
            if($jml>0){
            $row=mysql_fetch_array($result);
            ?>            
                <input type="hidden" class="form-control" id="idpasien" value="<?php echo $row[0];?>">
                <input type="hidden" class="form-control" id="namalengkap" value="<?php echo $row[2];?>">
                <label for="detailpasien">Detail Pasien </label>
                <div class="alert alert-info" role="alert">
                    <h4><?php echo $row[1];?></h4>
                    <h4><?php echo $row[2];?></h4>
                    <h5><?php echo $row[3];?></h5>
                </div>
            <?php                
            }else{
            ?>     
                <label for="detailpasien">Detail Pasien </label>
                <div class="alert alert-danger" role="alert">
                    <h4>Kode "<?php echo $param?>" tidak ada.</h4>
                    Jika pasien baru silahkan klik 
                    <a target="_self" href="<?=base_url() ?>mspasien/index/tranregistrasi" class="btn btn-info btn-sm">Daftar Baru</a>
                </div>
            <?php   
            }
    }
    
    public function loadform(){
?>
<form role="form">
    <div class="col-lg-4">
            <div class="form-group">
                <label for="kdpasien">Pilih Pasien </label>
                <div class="input-group">
                    <input type="text" onkeyup="detailpasien()" class="form-control" id="kdpasien" placeholder="Masukkan Kode Pasien">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-info" data-backdrop="static" data-toggle="modal" data-target="#myModal">Cari</button>
                    </div>
                </div>                
            </div>
            
            <div class="form-group">
                <div id="detailpasien">
                    <input type="hidden" class="form-control" id="idpasien">
                    <label for="detailpasien">Detail Pasien </label>
                    <div class="alert alert-info" role="alert">
                        <h4>Kode Pasien</h4>
                        <h4>Nama Pasien</h4>
                        <h5>Alamat</h5>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
<!--                <label for="tgljanji">Tanggal Janji <small>(YYYY-MM-DD HH:MM) <font style="color: #ff0000">*optional</font> </small></label>-->
                <input type="hidden" class="form-control" id="tgljanji" data-date-format="YYYY-MM-DD HH:mm" placeholder="Masukkan Tanggal Janji"/>
                <script type="text/javascript">
                    $(function () {
                          $('#tgljanji').datetimepicker();
                      });
                </script>
            </div>
            <div class="form-group">

            <label for="jenis">Jenis Antrian</label>
             <select class="form-control" id="jenis">
                <option>Pilih</option>
                <option>IGD</option>
                <option>Umum</option>
             </select>
            </div>
            <div class="form-group">
                <label for="keluhan">Keluhan Pasien</label>
                <textarea maxlength="500" style="resize: vertical;min-height: 108px;" class="form-control" id="keluhan" placeholder="Masukkan Keluhan Pasien"></textarea>
            </div>
            <button type="button" class="btn btn-primary" onclick="simpandata();">
                Simpan
            </button>
            <button type="reset" class="btn btn-default" onclick="tampil_form();">
                Reset Form
            </button>
    </div>
</form>

<!--Modal area start                -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Pilih Pasien</h4>
      </div>
      <div class="modal-body">

          
            <script>
                $(document).ready(function() {
                    $('#tbregistrasi').dataTable();
                    $('#tbregistrasi').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div style="min-height: 400px;min-width: 500px;">
<table class="table table-striped table-bordered table-hover" id="tbregistrasi">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Kategori</th>
            <th style="text-align: center;">Nama Lengkap</th>
            <th style="text-align: center;">Edit</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $query = "SELECT idpasien, kodepasien, namalengkap, alamat
                        FROM mstpasien WHERE datastat<>'DELETED' order by kodepasien asc";
            $result=  mysql_query($query);
            while($row=mysql_fetch_array($result)){
               echo "<tr>";
               echo "<td style=\"width: 20%;text-align: left\">$row[1]</td>";
               echo "<td style=\"text-align: left\">$row[2]</td>";
               echo "<td style=\"width: 7%;text-align: center\">
                        <a class=\"btn btn-sm btn-success\" data-dismiss=\"modal\" href=\"javascript:void(0);\" onclick=\"add_pasien('$row[1]')\">Pilih</a>
                    </td>";
               echo "</tr>";
            }
        ?>
    </tbody>                          
</table>
</div>
          
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal area end-->


<!--Modal2 area start                -->
<div class="modal fade" id="moddetailpasien" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Detail Keluhan Pasien</h4>
      </div>
      <div class="modal-body">
          <div id="keluhanpasien"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>
<!--Modal2 area end-->

<div class="col-lg-8">
<script>
                $(document).ready(function() {
                    $('#tbantrian').dataTable();
                    $('#tbantrian').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div style="min-height: 400px;min-width: 500px;">
<table class="table table-striped table-bordered table-hover" id="tbantrian">
    <thead>
        <tr class="danger">
            <th style="text-align: center;">Antrian</th>
            <th style="text-align: center;">Nama Lengkap</th>
            <th style="text-align: center;">Keluhan</th>
            <th style="text-align: center;">Detail</th>
            <th style="text-align: center;">Batal</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $query = "SELECT a.`idregistrasi`,a.`noantrian`,b.`namalengkap`,a.`keluhan` FROM `tranregistrasi` a 
                            INNER JOIN `mstpasien` b ON a.`idpasien` = b.`idpasien`
                        WHERE DATE_FORMAT(tglregistrasi,'%Y%m%d') = DATE_FORMAT(NOW(),'%Y%m%d')
                            AND a.`statregistrasi`='NEW'
                        ORDER BY a.`noantrian` ASC";
            $result=  mysql_query($query);
            while($row=mysql_fetch_array($result)){
               echo "<tr>";
               echo "<td style=\"width: 15%;text-align: center;\">$row[1]</td>";
               echo "<td style=\"width: 20%;text-align: left;\">$row[2]</td>";
               echo "<td style=\"text-align: left;\">$row[3]</td>";
               echo "<td style=\"width: 7%;text-align: center;\">
                        <a class=\"btn btn-sm btn-info\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#moddetailpasien\" href=\"javascript:void(0);\" onclick=\"keluhanpasien('$row[0]')\">Detail</a>
                    </td>";
               echo "<td style=\"width: 7%;text-align: center;\">
                        <a class=\"btn btn-sm btn-warning\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#moddetailpasien\" href=\"javascript:void(0);\" onclick=\"keluhanpasien('$row[0]','batalkan')\">Batal</a>
                    </td>";
               echo "</tr>";
            }
        ?>
    </tbody>      
</div>
<?php
    }

}
