<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridmsobat extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
            <script>
                $(document).ready(function() {
                    $('#tbMsObat').dataTable({
                        "bProcessing": true,
                        "bServerSide": true,
                        "sAjaxSource": "<?=base_url() ?>msobat/jsonlapdataobat",
                        "columns": [
                            {"width": "15%" },
                            null,
                            {"width": "10%" },
                            {"width": "10%" },
                            {"width": "7%" },
                            {"width": "7%" }
                        ],
                        "oLanguage": {
                            "sProcessing": "<div class=\"col-lg-12\"><h5><label class=\"label label-danger\">Silahkan tunggu, sedang mengambil data</label><h5></div>"
                        }
                    });
                    $('#tbMsObat').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div class="col-lg-12">
<table class="table table-striped table-bordered table-hover" id="tbMsObat">
    <thead>
        <tr class="danger">
            <th style="width: 15%;text-align: center;">Kode Obat</th>
            <th style="text-align: center;">Nama Obat</th>
            <th style="width: 10%;text-align: center;">Jml Stok</th>
            <th style="width: 10%;text-align: center;">Satuan</th>
            <th style="width: 7%;text-align: center;">Edit</th>           
            <th style="width: 7%;text-align: center;">Hapus</th>
        </tr>
    </thead>
                          
</table>
</div>

<div class="col-lg-12 input-control">
    <button type="button" class="btn btn-primary" onclick="tampil_form();">
        Tambah Data
    </button>
    <button type="button" class="btn btn-default" onclick="tampil_grid();">
        Refresh
    </button>
</div>            
        <?php
    }
}