<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class formmsobat extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function loadform(){
        error_reporting(0);
        $vidobat = $this->security->xss_clean($this->input->post('idobat'));
        $this->db->where('idobat', $vidobat);
        $this->db->where('datastat != ', 'DELETED');
        $query = $this->db->get("mstobat");
        if($query->num_rows>0)
        {          
            $row = $query->row();
            $idobat = $row->idobat;
            $kodeobat = $row->kodeobat;
            $namaobat = $row->namaobat;
            $jumlah = $row->jumlah;
            $satuan = $row->satuan;
            $hargajual = $row->hargajual;
        }
?>
<form role="form">
    <div class="col-lg-10">
            <div class="form-group">
                <?php
                    if($kodeobat=="" || empty($kodeobat)){
                ?>
                        <h1>Kode Obat : <span class="label label-info">Otomatis dari sistem</span></h1>
                <?php
                    }else{
                ?>
                        <h1>Kode Obat : <span class="label label-info"><?php echo $kodeobat; ?></span></h1>
                <?php
                    }
                ?>
            </div>
        <hr>
    </div>
    <div class="col-lg-4">
            <div class="form-group">
                <label for="namaobat">Nama Obat </label>
                <input type="hidden" class="form-control" id="idobat" value="<?php echo $idobat;?>">
                <input required="" maxlength="4000" type="text" class="form-control" id="namaobat" placeholder="Masukkan Nama Obat" value="<?php echo $namaobat;?>">
            </div>
        
            <div class="form-group">
                <label for="jmlobat">Jumlah Obat <small><font style="color:#FF0000">(Hanya Angka)</font></small></label>
                <input maxlength="11" type="text" class="form-control" id="jmlobat" placeholder="Masukkan Jumlah Obat" value="<?php echo $jumlah;?>">
            </div>
        
            <div class="form-group">
                <label for="satuan">Satuan</label>
                <input type="text" class="form-control" id="satuan" placeholder="Masukkan Satuan Obat" value="<?php echo $satuan;?>"/>
            </div>
            
            <div class="form-group">
                <label for="hargajual">Harga Jual <small><font style="color:#FF0000">(Hanya Angka)</font></small></label>
                <input type="text" class="form-control" id="hargajual" placeholder="Masukkan Harga Jual Obat" value="<?php echo $hargajual;?>"/>
            </div>
            
            <button type="button" class="btn btn-primary" onclick="simpandata();">
                Simpan
            </button>
            <button type="button" class="btn btn-default" onclick="tampil_grid();">
                Batal
            </button>
    </div>
</form>
<?php
    }

}
