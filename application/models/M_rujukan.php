<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_rujukan extends CI_Model {
    function __construct()
    {
        parent::__construct();
    //    $this->db = $this->load->database('faabula',true);        
    }
    
    function getRujukan()
    {
        $query = $this->db->get('m_rujukan');
        return $query;
    }
    function AddRujukan($data)
    {
        $query = $this->db->insert('m_rujukan',$data);
        return $query;
    }
    function EditRujukan($id,$data)
    {
        $this->db->where('id_rujukan',$id);
        $query = $this->db->update('m_rujukan',$data);
        return $query;
    }
    function DeleteRujukan($id)
    {   
        $this->db->where('id_rujukan',$id);
        $query = $this->db->delete('m_rujukan');
        return $query;
    }
}