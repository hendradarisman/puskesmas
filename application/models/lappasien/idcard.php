<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class idcard extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    public function personal(){
        $idpasien = $this->uri->segment(3);
        $this->db->where('idpasien',$idpasien);
        $query = $this->db->get("mstpasien");
        if($query->num_rows>0)
        {
            $row = $query->row();
            $kodepasien = $row->kodepasien;
            $namalengkap = $row->namalengkap;          
            $alamat = $row->alamat;
            $telp = $row->telp;
        }
?>
<link href="<?=base_url() ?>assets/css/bootstrap.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet" />
<link href="<?=base_url() ?>assets/css/default.css" rel="stylesheet" />
<style type="text/css">       
    .card{
        border: 1px solid black;
        width:  9cm;
        height: 5.5cm;
    }
    .header{
        height: 55px;
        margin-top: 10px;
        margin-left: 5px;
        margin-right: 5px;
        border-bottom: 1px solid #000;
    }
    .content{
        margin-top: 1px;
        margin-left: 5px;
        margin-right: 5px;
        border-top: 1px solid #000;
        height: 105px;
    }
    .isi{
        margin-top: 5px;
        font-size: 10px;
    }
    .nama{
        font-family: segoeui;
    }
    .detail{
        font-family: segoeui;
        font-size: 10px;
    }
    .barcode{
        margin: 5px 5px;
    }
</style>
<body onload="window.print();">
<div class="card">
    <div class="header">
        <table width="100%">
                <tr>
                    <td width="20%">
                        <?php
                            if(empty($this->owner->logo) || $this->owner->logo ==""){
                        ?>
                                <img class="img-rounded" src="<?=base_url() ?>assets/images/nologo.png" />
                        <?php
                            }else{
                        ?>
                                <img class="img-rounded" width="50" height="50" src="<?php echo base_url().$this->owner->logo; ?>" />
                        <?php
                            }
                        ?>
                    </td>
                    <td>
                        <div class="nama">
                            <?php
                                echo "<strong>".$this->owner->nama."</strong>";
                            ?>
                        </div>
                        <div class="detail">
                            <?php
                                echo $this->owner->alamat." - ".$this->owner->kota;
                                echo "<br>Telp. ".$this->owner->telp;                
                            ?>
                        </div>
                    </td>
                </tr>
        </table>
<!--        <div class="col-lg-2 logo">
            <?php
                if(empty($this->owner->logo) || $this->owner->logo ==""){
            ?>
                    <img class="img-rounded" src="<?=base_url() ?>assets/images/nologo.png" />
            <?php
                }else{
            ?>
                    <img class="img-rounded" width="50" height="50" src="<?php echo base_url().$this->owner->logo; ?>" />
            <?php
                }
            ?>
        </div>
        <div class="col-md-9">
            <div class="nama">
                <?php
                    echo "<strong>".$this->owner->nama."</strong>";
                ?>
            </div>
            <div class="detail">
                <?php
                    echo $this->owner->alamat." - ".$this->owner->kota;
                    echo "<br>Telp. ".$this->owner->telp;                
                ?>
            </div>
        </div>-->
    </div>
    <div class="content">
        <div class="isi">
            <table width="100%">
                <tr>
                    <td width="35%" style="font-size: 14px;">
                        Kode Pasien
                    </td>
                    <td style="font-size: 14px;">
                        : <?php echo $kodepasien; ?>
                    </td>
                </tr>
                <tr>
                    <td width="35%" style="font-size: 14px;">
                        Nama Pasien
                    </td>
                    <td style="font-size: 14px;">
                        : <?php echo $namalengkap; ?>
                    </td>
                </tr>
                <tr>
                    <td width="35%" style="font-size: 14px;">
                        No. Telp
                    </td>
                    <td style="font-size: 14px;">
                        : <?php echo $telp; ?>
                    </td>
                </tr>
                <tr>
                    <td width="35%" style="font-size: 14px;">
                        Alamat
                    </td>
                    <td style="font-size: 14px;" rowspan="2">
                        : <?php echo $alamat; ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="barcode">
                            <font style="font-family:free3of9; font-size: 42px;">
                                <?php echo "*$kodepasien*"; ?>
                            </font>                        
                        </div>
                        
                    </td>
                </tr>
            </table>
<!--            <div class="col-lg-4">
                Kode Pasien
            </div>
            <div class="col-lg-8">
                : <?php echo $kodepasien; ?>
            </div>
            <div class="col-lg-4">
                Nama Pasien
            </div>
            <div class="col-lg-8">
                : <?php echo $namalengkap; ?>
            </div>
            <div class="col-lg-4">
                No. Telp
            </div>
            <div class="col-lg-8">
                : <?php echo $telp; ?>
            </div>
            <div class="col-lg-4">
                Alamat
            </div>
            <div class="col-lg-8">
                : <?php echo $alamat; ?>
            </div>            -->
        </div>
    </div>
<!--    <div class="barcode">
        <div class="col-lg-4" style="margin-top: -22px;">
            <font style="font-family:free3of9; font-size: 35px;">
                <?php echo "*$kodepasien*"; ?>
            </font>
        </div>
    </div>  -->
</div>
</body>

<?php
    }

}