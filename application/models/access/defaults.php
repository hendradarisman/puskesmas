<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class defaults extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function login(){
        $this->load->library('encrypt');
        $this->load->library('user_agent');
        // grab user input
        if ($this->agent->is_browser())
        {
            $agent = $this->agent->browser().' '.$this->agent->version();
        }
        elseif ($this->agent->is_robot())
        {
            $agent = $this->agent->robot();
        }
        elseif ($this->agent->is_mobile())
        {
            $agent = $this->agent->mobile();
        }
        else
        {
            $agent = 'Unidentified User Agent';
        }
        
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));

        $this->db->where('namapengguna', $username);
        //$this->db->where('katakunci', $password);
        $this->db->where('stataktif', 'A');
        $this->db->or_where('stataktif', 'B');
        
        $query = $this->db->get("mstpengguna");
        if($query->num_rows == 1)
        {
            $row = $query->row();
            $xpass = $row->katakunci;
            $pass = $this->encrypt->decode($xpass);
            if($row->namapengguna != "sadmin"){
                if($password != $pass){
                    $query = "call usplogpengguna ('$username','LOGIN GAGAL : Password atau pengguna salah, atau akun tidak terdaftar','".$this->input->ip_address()."','$agent','".$this->agent->platform()."');";
                    $resl = $this->db->simple_query($query);
                    $this->session->set_userdata('statregister', 'akunkosong');
                    return false;
                }
            }
            $this->load->helper('cookie');
            $this->input->set_cookie('cookUserName', "cel", "USERNAME");
            $this->input->set_cookie('cookIdUser', "ciu", "PUDYASTO");
            $data = array(
                    'idpengguna' => $row->idpengguna,
                    'idgrup' => $row->idgrup,
                    'idperson' => $row->idperson,
                    'namapengguna' => $row->namapengguna,
                    'platform' => $this->agent->platform(),
                    'browser' => $agent,
                    'logged_in' => true
                    );
            $this->session->set_userdata($data);
            $query = "call usplogpengguna ('$username','LOGIN','".$this->input->ip_address()."','$agent','".$this->agent->platform()."');";
            $resl = $this->db->simple_query($query);
            return true;
        }
        $query = "call usplogpengguna ('$username','LOGIN GAGAL : Password atau pengguna salah, atau akun tidak terdaftar','".$this->input->ip_address()."','$agent','".$this->agent->platform()."');";
        $resl = $this->db->simple_query($query);
        $this->session->set_userdata('statregister', 'akunkosong');
        return false;
    }
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */