<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class detaillaprmpasien extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function riwayatlaprmpasien() {
        $idregistrasi = $this->security->xss_clean($this->input->post('idregistrasi'));
?>
<script type="text/javascript">   
function getfotopasien(idtrangambar){
    $("#fotopasien").html("<h3><label class=\"label label-info\">Mengambil gambar ...</label></h3>");
    $.ajax({
        type: "POST",
        url: "<?php echo base_url().'trantindakan/modalfoto'; ?>",
        data:{"idtrangambar":idtrangambar},
        success: function(resp){                            
                $("#fotopasien").html(resp);
        }
    });
}
</script>            
<table class="table table-bordered table-hover table-striped">
    <caption style="font-size:24px;">Tindakan yang dilakukan</caption>
    <tr class="info">
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td width="25%" style="text-align: center;">
            Nama Tindakan
        </td>
        <td style="text-align: center;">
            Keterangan
        </td>
    </tr>
    <?php
        $qry="SELECT b.`namatindakan`,a.`keterangan` FROM `trantindakan` a
                INNER JOIN `msttindakan` b ON a.`idtindakan` = b.`idtindakan`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY a.`tgltindakan`";
            $result = mysql_query($qry);
            $num = 1;
            while($row = mysql_fetch_array($result)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num;?>
        </td>
        <td width="25%">
            <?php echo $row[0];?>
        </td>
        <td>
            <?php echo $row[1];?>
        </td>
    </tr>
    <?php
                $num++;
            }
    ?>
</table>


<?php
        $qry2="SELECT  `anamnesis`,`diagnosis`,`catatan`,pemeriksaanfisik FROM `trandiagnosis`
                WHERE `idregistrasi`='$idregistrasi'";
            $result2 = mysql_query($qry2);
            $row2 = mysql_fetch_array($result2);
    ?>
<table class="table table-bordered table-hover table-striped">
    <caption style="font-size:24px;">Diagnosa Pasien</caption>
    <tr class="info">
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td width="25%" style="text-align: center;">
            Diagnosa
        </td>
        <td style="text-align: center;">
             Keterangan
        </td>
    </tr>
    <tr>
        <td width="3%" style="text-align: center;">
            1.
        </td>
        <td width="25%">
            Anamnesis
        </td>
        <td>
             <?php echo $row2[0];?>
        </td>
    </tr>
    <tr>
        <td width="3%" style="text-align: center;">
            2.
        </td>
        <td width="25%">
            Pemeriksaan Fisik
        </td>
        <td>
            <?php echo $row2[3];?>
        </td>
    </tr>
    <tr>
        <td width="3%" style="text-align: center;">
            3.
        </td>
        <td width="25%">
            Diagnosis
        </td>
        <td>
            <?php echo $row2[1];?>
        </td>
    </tr>
    <tr>
        <td width="3%" style="text-align: center;">
            4.
        </td>
        <td width="25%">
            Catatan Pasien
        </td>
        <td>
            <?php echo $row2[2];?>
        </td>
    </tr>
</table>    

<table class="table table-bordered table-hover table-striped">
    <caption style="font-size:24px;">Resep yang diberikan</caption>
    <tr class="info">
        <td width="3%" style="text-align: center;">
            No.
        </td>
        <td width="25%" style="text-align: center;">
            Nama Obat
        </td>
        <td style="text-align: center;">
            Jumlah Obat
        </td>
    </tr>
    <?php
        $qry3="SELECT b.`namaobat`,CONCAT(a.`jmlobat`, ' ',b.`satuan`) obat FROM `tranresep` a
                INNER JOIN mstobat b ON a.`idobat` = b.`idobat`
                WHERE a.`idregistrasi`='$idregistrasi'
                ORDER BY b.`namaobat`";
            $result3 = mysql_query($qry3);
            $num3 = 1;
            while($row3 = mysql_fetch_array($result3)){
    ?>
    <tr>
        <td width="3%" style="text-align: center;">
            <?php echo $num3;?>
        </td>
        <td width="25%">
            <?php echo $row3[0];?>
        </td>
        <td>
            <?php echo $row3[1];?>
        </td>
    </tr>
    <?php
                $num3++;
            }
    ?>
</table>


<table class="table table-bordered table-hover table-striped">
    <caption style="font-size:24px;">Foto Rekam Medis</caption>
    <thead>
        <tr class="info">
            <th style="text-align: center;">Foto</th>
            <th style="text-align: center;">Catatan</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $query2 = "SELECT `idtrangambar`, `foto`, `catatan`,fotoname FROM `trangambar` WHERE idregistrasi = '$idregistrasi'
                        ORDER BY fotoname ASC";
            $result2=  mysql_query($query2);
            while($row=mysql_fetch_array($result2)){
               echo "<tr>";
               echo "<td style=\"width: 15%;text-align: center\">
                        <a href=\"javascript:void(0);\" onclick=\"getfotopasien('$row[0]')\" data-backdrop=\"static\" data-toggle=\"modal\" data-target=\"#myModalFoto\">
                            <img src=\"".base_url()."trantindakan/tampilfoto/$row[0]\" width='100' height='100'/>
                        </a>
                     </td>";
               echo "<td style=\"text-align: left\">$row[2]</td>";
               echo "</tr>";
            }
        ?>
    </tbody>   
</table>

<!--Modal Obat area start                -->
<div class="modal fade" id="myModalFoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Foto Pasien</h4>
      </div>
      <div class="modal-body">
              <div id="fotopasien"></div>
      </div>  
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
      </div>
    </div>
  </div>
</div>    
<!--Modal area end-->
<?php
    }


    public function accordion(){
            echo "<div class=\"panel-group\" id=\"accordion\">";
            $kodepasien = $this->security->xss_clean($this->input->post('kodepasien'));
            $qry="SELECT DATE_FORMAT(b.`tglregistrasi`,'%d %M %Y'),b.`idregistrasi` tgl FROM `mstpasien` a
                    INNER JOIN `tranregistrasi` b ON a.`idpasien` = b.`idpasien` 
                    where a.`kodepasien` = '$kodepasien' and b.`statregistrasi` = 'FINISH'
                    ORDER BY b.`tglregistrasi` DESC";
            $result = mysql_query($qry);
            $num = 1;
            while($rowx = mysql_fetch_array($result)){
            ?>
                  <div class="panel panel-default">
                      <div class="panel-heading" onclick="tampil_riwayat('<?php echo $rowx[1];?>','<?php echo 'rwt'.$num;?>');">
                      <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $num;?>">
                          Tanggal : <?php echo $rowx[0];?>
                        </a>
                      </h4>
                    </div>
                    <div id="collapse<?php echo $num;?>" class="panel-collapse collapse">
                      <div class="panel-body">
                          <div id="rwt<?php echo $num;?>"></div>
                      </div>
                    </div>
                  </div>      
            <?php
            $num++;
        }
        echo "</div>";
    }
}