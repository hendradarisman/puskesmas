<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridlaprmpasien extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
            <script>
                $(document).ready(function() {
                    $('#tbMsPasien').dataTable();
                    $('#tbMsPasien').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div class="col-lg-12">
<table class="table table-striped table-bordered table-hover" id="tbMsPasien">
    <thead>
        <tr class="danger">
            <th style="width: 15%;text-align: center;">Kode Pasien</th>
            <th style="text-align: center;">Nama</th>
            <th style="text-align: center;">Alamat</th>
            <th style="width: 10%;text-align: center;">Telp</th>
            <th style="width: 10%;text-align: center;">No. HP</th>
            <th style="width: 15%;text-align: center;">Terakhir Periksa</th>
            <th style="width: 15%;text-align: center;">Riwayat Pasien</th>
        </tr>
    </thead>
    <tbody>
        <?php
            $data = array(
                'idpasien',
                'kodepasien',
                'namalengkap',
                'alamat',
                'telp',
                'hp',
                "DATE_FORMAT(funcGetLastCheckIn(idpasien),'%d %M %Y') lastcheckin"
            );

            $this->db->select($data);
            $this->db->where('datastat !=', 'DELETED'); 
            $this->db->order_by("funcGetLastCheckIn(idpasien)", "desc");
            $query = $this->db->get("mstpasien");
            if ($query->num_rows() > 0)
            {
                 foreach ($query->result() as $row)
                    {
                       echo "<tr>";
                       echo "<td style=\"width: 15%;text-align: left\">$row->kodepasien</td>";
                       echo "<td style=\"text-align: left\">$row->namalengkap</td>";
                       echo "<td style=\"width: 30%;text-align: left\">$row->alamat</td>";
                       echo "<td style=\"width: 10%;text-align: left\">$row->telp</td>";
                       echo "<td style=\"width: 10%;text-align: left\">$row->hp</td>";
                       echo "<td style=\"width: 14%;text-align: left\">$row->lastcheckin</td>";
                       echo "<td style=\"width: 12%;text-align: center\">
                                <a class=\"btn btn-info\" href=\"".  base_url()."laprmpasien/detail/$row->kodepasien\">
                                <span class=\"glyphicon glyphicon-file\"></span>
                                </a>
                            </td>";
                       echo "</tr>";
                    }
            }
        ?>
    </tbody>                          
</table>
</div>            
        <?php
    }
}