<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class gridlapdatatindakan extends CI_Model{
    function __construct(){
        parent::__construct();
    }
    
    public function loadgrid(){
        ?>
            <script>
                $(document).ready(function() {
                    $('#tbgridlapdatatindakan').dataTable({
                        "bProcessing": true,
                        "bServerSide": true,
                        "sAjaxSource": "<?=base_url() ?>lapdatatindakan/datatindakan",
                        "columns": [
                            {"width": "15%" },
                            {"width": "20%" },
                            {"width": "10%" },
                            null
                        ],
                        "oLanguage": {
                            "sProcessing": "<div class=\"col-lg-12\"><h5><label class=\"label label-danger\">Silahkan tunggu, sedang mengambil data</label><h5></div>"
                        }
                    });
                    $('#tbgridlapdatatindakan').tooltip({
                        selector: "[data-toggle=tooltip]",
                        container: "body"
                    });
                });
            </script>

<div class="col-lg-12">
    <div style="margin-bottom: 20px;">
<a href="<?=base_url() ?>lapdatatindakan/excel" title="Cetak ke Excel" class="exporticon" target="_blank">
    <?php echo $this->icons->xls('24','24');?> Cetak ke Excel
</a>      
<a href="<?=base_url() ?>lapdatatindakan/pdf" title="Cetak ke Excel" class="exporticon" target="_blank">
    <?php echo $this->icons->pdf('24','24');?> Cetak ke Pdf
</a>         
    </div>    
<table class="table table-striped table-bordered table-hover" id="tbgridlapdatatindakan">
    <thead>
        <tr class="danger">
            <th style="width: 15%;text-align: center;">Kode Tindakan</th>
            <th style="text-align: center;">Nama Tindakan</th>
            <th style="width: 10%;text-align: center;">Tarif</th>
            <th style="text-align: center;">Keterangan</th>
        </tr>
    </thead>                        
</table>   
</div>           
        <?php
    }
}