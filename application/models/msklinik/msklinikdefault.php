<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class msklinikdefault extends CI_Model{
    function __construct(){
        parent::__construct();
    }
        
    public function prosesdata(){        
        $nama = $this->db->escape_str($this->input->post('nama'));
        $noijin = $this->db->escape_str($this->input->post('noijin'));
        $alamat = $this->db->escape_str($this->input->post('alamat'));
        $kota = $this->db->escape_str($this->input->post('kota'));
        $website = $this->db->escape_str($this->input->post('website'));
        $fax = $this->db->escape_str($this->input->post('fax'));
        $telp = $this->db->escape_str($this->input->post('telp'));
        $email = $this->db->escape_str($this->input->post('email'));
        $pjlab = $this->db->escape_str($this->input->post('pjlab'));
        $pjklinik = $this->db->escape_str($this->input->post('pjklinik'));
        
        $fileName = $_FILES['logoklinik']['name'];
//        $fileSize = $_FILES['logoklinik']['size'];
//        $fileError = $_FILES['logoklinik']['error'];
        $tipefile = $_FILES['logoklinik']['type'];
//        $pathfile= $_FILES['logoklinik']['tmp_name'];
        $extension = pathinfo($_FILES['logoklinik']['name'], PATHINFO_EXTENSION);
        
        if(!empty($fileName)){
            $lokasi = "assets/images/".$fileName;
            $logo = "assets/images/logoklinik.".$extension;
            $favicon="assets/images/favicon.png";
            move_uploaded_file($_FILES['logoklinik']['tmp_name'], $lokasi);
            rename($lokasi, $logo);   
            
            if ($tipefile == "image/jpeg")
            {
                    $orig_image = imagecreatefromjpeg($logo);
                    $image_info = getimagesize($logo); 
                    $width_orig  = $image_info[0]; // Mendapatkan ukuran panjang foto asli
                    $height_orig = $image_info[1]; // Mendapatkan ukuran lebar foto asli
                    $width = 350; // Ukuran Panjang
                    $height = 350; // Ukuran Lebar
                    $destination_image = imagecreatetruecolor($width, $height);
                    imagecopyresampled($destination_image, $orig_image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
                    imagejpeg($destination_image, $logo, 100);
            }
            imagepng(imagecreatefromstring(file_get_contents($logo)), $favicon);
        }
        $query = "call uspMstKlinik ('$nama','$noijin','$alamat','$telp','$fax','$email','$pjklinik','$pjlab','$kota','$website','$logo')";
        $resl = $this->db->simple_query($query); 
        //echo $query;
         if($resl){
             return true;
         }else{
            return false;
         }
    }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */