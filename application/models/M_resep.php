<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_resep extends CI_Model {
    function __construct()
    {
        parent::__construct();
    //    $this->db = $this->load->database('faabula',true);        
    }
    
    function getResep()
    {
        //$query = $this->db->get('m_rujukan');
        return $this->db->query("select a.idregistrasi,a.idpasien,a.idobat,b.namaobat,a.jmlobat,c.namalengkap,a.keteranganresep,a.tglresep from tranresep a
        inner join mstobat b on a.idobat=b.idobat 
        inner join mstpasien c on a.idpasien=c.idpasien
        GROUP BY  a.idregistrasi,a.idpasien,a.idobat,b.namaobat,a.jmlobat,c.namalengkap,a.keteranganresep,a.tglresep")->result();
        // $query = '';
        // return $query;
    }
    function AddRujukan($data)
    {
        $query = $this->db->insert('m_rujukan',$data);
        return $query;
    }
    function EditRujukan($id,$data)
    {
        $this->db->where('id_rujukan',$id);
        $query = $this->db->update('m_rujukan',$data);
        return $query;
    }

    function DeleteRujukan($id)
    {   
        $this->db->where('id_rujukan',$id);
        $query = $this->db->delete('m_rujukan');
        return $query;
    }

    function Cetak($id)
    {   
        $this->db->select('*');
        $this->db->from('m_rujukan a');
        $this->db->join('mstpasien b','a.pasien_id = b.idpasien');
        $this->db->where('a.id_rujukan', $id);
        $query= $this->db->get();
        return $query->result();
		
    }
}