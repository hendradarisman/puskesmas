<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Msobat extends CI_Controller {
        public function __construct()
            {
                parent::__construct();
                $this->load->model('access/defaults');
                $this->load->library(array('form_validation','session'));
                $this->load->database();
                $this->load->helper('url');
                
                $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            }
            
	public function index()
	{
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }else{
                    $this->load->view('msobat/index');
                }
            
	}
        
        public function gridmsobat(){
            $this->load->model('msobat/gridmsobat');
            $result = $this->gridmsobat->loadgrid();
            print $result;
        }
        
        public function formmsobat(){
            $this->load->model('msobat/formmsobat');
            $result = $this->formmsobat->loadform();
            print $result;
        }
        
        public function prosesdata(){
            $this->load->model('msobat/msobatdefault');
            $result = $this->msobatdefault->prosesdata();
            if($result){
                print "1";
            }else{
                print "0";
            }
        }
        
        public function jsonlapdataobat()
	{
            $this->load->model('msobat/json');
            $result = $this->json->dataobat();
            print $result;
            
	}
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

