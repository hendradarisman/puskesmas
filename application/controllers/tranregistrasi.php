<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tranregistrasi extends CI_Controller {
        public function __construct()
            {
                parent::__construct();
                $this->load->model('access/defaults');
                $this->load->library(array('form_validation','session'));
                $this->load->database();
                $this->load->helper('url');
                
                $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            }
            
	public function index()
	{
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }else{
                    $this->load->view('tranregistrasi/index');
                }
            
	}
        
        public function formtranregistrasi(){
            $this->load->model('tranregistrasi/formtranregistrasi');
            $result = $this->formtranregistrasi->loadform();
            print $result;
        }
        
        public function getdetailpasien(){
            $this->load->model('tranregistrasi/formtranregistrasi');
            $result = $this->formtranregistrasi->getdetailpasien();
            print $result;
        }
        
        public function getkeluhanpasien(){
            $this->load->model('tranregistrasi/formtranregistrasi');
            $result = $this->formtranregistrasi->getkeluhanpasien();
            print $result;
        }
        
        
        public function batalkanpasien(){
            $this->load->model('tranregistrasi/tranregistrasidefault');
            $result = $this->tranregistrasidefault->batalkanpasien();
            if($result){
                print "1";
            }else{
                print "0";
            }
        }

        public function prosesdata(){
            $this->load->model('tranregistrasi/tranregistrasidefault');
            $result = $this->tranregistrasidefault->prosesdata();
            if($result){
                print "1";
            }else{
                print "0";
            }
        }
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

