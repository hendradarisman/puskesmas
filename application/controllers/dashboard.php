<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
         public function __construct()
            {
                parent::__construct();
                $this->load->helper('url');
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }
            }
            
	public function index()
            {
                clearstatcache();
                $this->load->helper('cookie');
                $this->load->view('dashboard/index');
            }
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */