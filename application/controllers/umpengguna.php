<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Umpengguna extends CI_Controller {
        public function __construct()
            {
                parent::__construct();
                $this->load->library('encrypt');
                $this->load->model('access/defaults');
                $this->load->library(array('form_validation','session'));
                $this->load->database();
                $this->load->helper('url');
                
                $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            }
            
	public function index()
	{
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }else{
                    $this->load->view('um/umpengguna/index');
                }
            
	}
        
        public function gridumpengguna(){
            $this->load->model('um/umpengguna/gridumpengguna');
            $result = $this->gridumpengguna->loadgrid();
            print $result;
        }
        
        public function formumpengguna(){
            $this->load->model('um/umpengguna/formumpengguna');
            $result = $this->formumpengguna->loadform();
            print $result;
        }
        
        public function prosesdata(){
            $this->load->model('um/umpengguna/umpenggunadefault');
            $result = $this->umpenggunadefault->prosesdata();
            if($result){
                print "1";
            }else{
                print "0";
            }
        }
        
        public function getpassword(){
            $this->load->model('um/umpengguna/umpenggunadefault');
            $result = $this->umpenggunadefault->getpassword();
            print $result;
        }
        
        public function cekpengguna(){
            $this->load->model('um/umpengguna/cekpengguna');
            $result = $this->cekpengguna->validate();
            print $result;       
        }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */