	
    
    <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rujukan extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
    $this->load->model('M_rujukan');
    }
    // rujukan
	public function index()
	{
		$this->data['page_name'] = "rujukan";
		$this->data['rujukan'] = $this->M_rujukan->getRujukan();
     //   $this->template->load('template_home','master/rujukan',$this->data);
        $this->load->view('rujukan/rujukan',$this->data);
    }
    
	public function rujukan_add()
	{
		$data = $this->input->post();
		$this->M_rujukan->AddRujukan($data);
		redirect('rujukan/rujukan','refresh');
    }
    
	public function rujukan_edit($id)
	{
		$data = $this->input->post();
		$this->M_rujukan->EditRujukan($id,$data);
		redirect('rujukan/rujukan','refresh');
	}
	public function rujukan_delete($id)
	{
		$this->M_rujukan->DeleteRujukan($id);
		redirect('rujukan/rujukan','refresh');
	}

}