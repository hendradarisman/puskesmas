<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Access extends CI_Controller {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('url');
            $this->load->library('encrypt');
        }
        
	public function index()
	{
            $query = $this->db->get("mstklinik");
            if($query->num_rows == 1)
            {
                $row = $query->row();
                $nama = $row->nama;
                $noijin = $row->noijin;
                $alamat = $row->alamat;
                $telp = $row->telp;
                if($nama == '' || empty($nama) 
                        || $noijin == '' || empty($noijin) 
                        || $alamat == '' || empty($alamat)
                        || $telp   == '' || empty($telp))
                    {
                         redirect("msklinik");
                    }else{
                        $logged_in = $this->session->userdata('logged_in');
                        if($logged_in){
                            redirect("Dashboard");
                            // var_dump($logged_in);
                            // exit;
                        }else{
                            // var_dump($logged_in);
                            // exit;
                            $this->load->view('access/login');
                        }                
                    }
            }else{
                // var_dump("oy");
                // exit;
                 redirect("Msklinik");
            }
            
	}
        
	public function login()
	{
            $this->load->view('access/login');
	}
        
        public function validate()
	{
            $this->load->model('access/defaults');
            $result = $this->defaults->login();
            if(!$result){
                
            $this->load->view('access/login');

            }else{
                echo "login sukses";
                redirect('Dashboard');
            }      
	}
        public function logout(){
            $this->load->library('encrypt');
            $this->load->library('user_agent');
            // grab user input
            if ($this->agent->is_browser())
            {
                $agent = $this->agent->browser().' '.$this->agent->version();
            }
            elseif ($this->agent->is_robot())
            {
                $agent = $this->agent->robot();
            }
            elseif ($this->agent->is_mobile())
            {
                $agent = $this->agent->mobile();
            }
            else
            {
                $agent = 'Unidentified User Agent';
            }
            $namapengguna = $this->session->userdata('namapengguna');
            $query = "call usplogpengguna ('$namapengguna','LOGOUT','".$this->input->ip_address()."','$agent','".$this->agent->platform()."');";
            $resl = $this->db->simple_query($query);
            $this->session->sess_destroy();
            $this->load->view('access/login');
            
            // redirect(base_url(),refresh);  '
                
        }
}
