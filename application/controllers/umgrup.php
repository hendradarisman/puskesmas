<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Umgrup extends CI_Controller {
        public function __construct()
            {
                parent::__construct();
                $this->load->model('access/defaults');
                $this->load->library(array('form_validation','session'));
                $this->load->database();
                $this->load->helper('url');
                
                $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            }
            
	public function index()
	{
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }else{
                    $this->load->view('um/umgrup/index');
                }
            
	}
        
        public function gridumgrup(){
            $this->load->model('um/umgrup/gridumgrup');
            $result = $this->gridumgrup->loadgrid();
            print $result;
        }
        
        public function formumgrup(){
            $this->load->view('um/umgrup/formumgrup');
        }
        
        public function prosesdata(){
            $this->load->model('um/umgrup/umgrupdefault');
            $result = $this->umgrupdefault->prosesdata();
            if($result){
                print "Data Berhasil diproses";
            }else{
                print "Data Gagal diproses";
            }
        }
        
        public function hakakses(){
            $this->load->view('um/umgrup/hakaksesumgrup');
        }
        
        public function gethakakses(){
            $this->load->model('um/umgrup/hakasesumgrup');
            $result = $this->hakasesumgrup->loadgrid();
            print $result;
        }
        
        public function setakses(){
            $this->load->model('um/umgrup/umgrupdefault');
            $result = $this->umgrupdefault->setakses();
            print $result;
        }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */