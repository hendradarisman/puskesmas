<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Account extends CI_Controller {
        public function __construct()
        {
            parent::__construct();
            $this->load->helper('url');
            $this->load->library('encrypt');
        }
        
	public function index()
	{
            $logged_in = $this->session->userdata('logged_in');
            if($logged_in){
                redirect("dashboard");
            }else{
                $this->load->view('account/register');
            }
	}
        
        public function register()
	{
            $this->load->view('account/register');
	}
        
        public function forget()
	{
            $this->load->view('account/forget');
	}
        
        public function cekmail(){
            $this->load->model('/account/defaults');
            $result = $this->defaults->cekmail();
            print $result;       
        }
        
        public function submit(){
            $this->load->model('/account/defaults');
            $result = $this->defaults->submit();
            print $result;
        }
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */