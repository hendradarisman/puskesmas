<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tranbayar extends CI_Controller {
        public function __construct()
            {
                parent::__construct();
                $this->load->model('access/defaults');
                $this->load->library(array('form_validation','session'));
                $this->load->database();
                $this->load->helper('url');
                
                $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            }
            
	public function index()
	{
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }else{
                    $this->load->view('tranbayar/index');
                }
            
	}
        
        public function gridtranbayar() {
            $this->load->model('tranbayar/gridtranbayar');
            $result = $this->gridtranbayar->loadgrid();
            print $result;
        }
        
        public function datatranbayar() {
            $this->load->model('tranbayar/datatranbayar');
            $result = $this->datatranbayar->datatranbayar();
            print $result;
        }
        
        public function getdetailtagihan() {
            $this->load->model('tranbayar/datatranbayar');
            $result = $this->datatranbayar->getdetailtagihan();
            print $result;
        }
        
        public function submitpembayaran() {
            $this->load->model('tranbayar/defaulttranbayar');
            $result = $this->defaulttranbayar->submitpembayaran();
            print $result;
        }
        
        public function cetaknota() {
            $this->load->model('tranbayar/laptranbayar');
            $result = $this->laptranbayar->cetaknota();
            print $result;
        }
        
        public function getDataTranBayar() {
            $this->load->model('tranbayar/json');
            $result = $this->json->getDataTranBayar();
            print $result;
        }
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

