<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ummonitoring extends CI_Controller {
        public function __construct()
            {
                parent::__construct();
                $this->load->model('access/defaults');
                $this->load->library(array('form_validation','session'));
                $this->load->database();
                $this->load->helper('url');
                
                $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            }
            
	public function index()
	{
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }else{
                    $this->load->view('um/ummonitoring/index');
                }
            
	}
        
        public function loadhistori()
	{
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }else{
                    $this->load->view('um/ummonitoring/monitor');
                }
            
	}
        
        public function dataummonitoring(){
            $this->load->model('um/ummonitoring/datamonitoring'); //gridummonitoring
            $result = $this->datamonitoring->loadgrid();
            return $result;
        }
        
        public function gridummonitoring(){
            $this->load->model('um/ummonitoring/gridummonitoring');
            $result = $this->gridummonitoring->loadgrid();
            return $result;
        }
        
        public function logout(){
            $this->load->model('um/ummonitoring/ummonitoringdefault');
            $result = $this->ummonitoringdefault->logout();
        }
        
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */