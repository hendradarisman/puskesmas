<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trantindakanedit extends CI_Controller {
        public function __construct()
            {
                parent::__construct();
                $this->load->model('access/defaults');
                $this->load->library(array('form_validation','session'));
                $this->load->database();
                $this->load->helper('url');
                
                $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            }
            
	public function index()
	{
            $logged_in = $this->session->userdata('logged_in');
            if(!$logged_in){
                header("location: ".base_url());
            }else{
                $this->load->view('trantindakanedit/index');
            }
	}
        
        public function jsondaftarpasien()
	{
            $this->load->model('trantindakanedit/json');
            $result = $this->json->datapasien();
            print $result;
            
	}
        
        public function formtrantindakan(){
            $logged_in = $this->session->userdata('logged_in');
            if(!$logged_in){
                header("location: ".base_url());
            }else{
                $this->load->view('trantindakanedit/tindakanpasien');
            }            
        }
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

