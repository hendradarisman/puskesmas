<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
         public function __construct()
            {
                parent::__construct();
                $this->load->helper('url');
              
            }
            
	public function index()
            {
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    
// redirect(base_url(),refresh);
$this->load->view('access/login');
                }
                clearstatcache();
                $this->load->helper('cookie');
                $this->load->view('dashboard/index');
            }
}

/* 
 * Created by Pikamedia
 * Email : info@pikamedia.id
 * info@pikamedia.id
 */