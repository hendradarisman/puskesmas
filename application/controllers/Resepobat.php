	
    
    <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resepobat extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
    $this->load->model('M_resep');
    }
    // rujukan
	public function index()
	{
		$this->data['page_name'] = "rujukan";
		$this->data['resep'] = $this->M_resep->getResep();
     //   $this->template->load('template_home','master/rujukan',$this->data);
        $this->load->view('resepobat/index',$this->data);
    }
    
	public function rujukan_add()
	{
		$data = $this->input->post();
		$this->M_resep->AddRujukan($data);
		redirect('Rujukan/rujukan','refresh');
    }
	
	public function cetak($id)
	{
		//$this->data['page_name'] = "rujukan";
		 $data['tampil'] = $this->M_resep->Cetak($id);
		//print_r($data['tampil']);
		$this->load->view('Rujukan/testing',$data);
	}
	
	public function rujukan_edit($id)
	{
		$data = $this->input->post();
		$this->M_resep->EditRujukan($id,$data);
		redirect('Rujukan/rujukan','refresh');
	}

	public function rujukan_delete($id)
	{
		$this->M_resep->DeleteRujukan($id);
		redirect('Rujukan/rujukan','refresh');
	}

}