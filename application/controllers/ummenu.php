<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ummenu extends CI_Controller {
        public function __construct()
            {
                parent::__construct();
                $this->load->model('access/defaults');
                $this->load->library(array('form_validation','session'));
                $this->load->database();
                $this->load->helper('url'); 
                
                $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            }
            
	public function index()
	{
            $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }else{
                    $this->load->view('um/ummenu/index');
                }
	}
        
        public function gridummodul(){
            $this->load->model('um/ummenu/gridummodul');
            $result = $this->gridummodul->loadgrid();
            print $result;
        }
        
        public function gridummenu(){
            $this->load->model('um/ummenu/gridummenu');
            $result = $this->gridummenu->loadgrid();
            print $result;
        }
        
        public function formummenu(){
            $this->load->view('um/ummenu/formummenu');
        }
        
        public function prosesdata(){
            $this->load->model('um/ummenu/ummenudefault');
            $result = $this->ummenudefault->prosesdata();
            if($result){
                print "Data Berhasil diproses";
            }else{
                print "Data Gagal diproses";
            }
        }
        
        public function simpanmodul(){
            $this->load->model('um/ummenu/ummenudefault');
            $result = $this->ummenudefault->simpanmodul();
            if($result){
                print "Data Berhasil diproses";
            }else{
                print "Data Gagal diproses";
            }
        }
}

/* generate by mr.pudyasto */
/* email : mr.pudyasto@gmail.com */