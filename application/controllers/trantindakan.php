<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trantindakan extends CI_Controller {
        public function __construct()
            {
                parent::__construct();
                $this->load->model('access/defaults');
                $this->load->library(array('form_validation','session'));
                $this->load->database();
                $this->load->helper('url');
                
                $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
                $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
                $this->output->set_header('Pragma: no-cache');
                $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); 
            }
            
	public function index()
	{
                $logged_in = $this->session->userdata('logged_in');
                if(!$logged_in){
                    header("location: ".base_url());
                }else{
                    $this->load->view('trantindakan/index');
                }
            
	}
        
        public function formtrantindakan(){
            $logged_in = $this->session->userdata('logged_in');
            if(!$logged_in){
                header("location: ".base_url());
            }else{
                $this->load->view('trantindakan/tindakanpasien');
            }            
//            $this->load->model('trantindakan/formtrantindakan');
//            $result = $this->formtrantindakan->formtrantindakan();
//            print $result;
        }
        
        public function gettabelresep(){
            $this->load->model('trantindakan/formtrantindakan');
            $result = $this->formtrantindakan->gettabelresep();
            print $result;
        }
        
        public function gettabeltindakan(){
            $this->load->model('trantindakan/formtrantindakan');
            $result = $this->formtrantindakan->gettabeltindakan();
            print $result;
        }
        
        public function getpasien(){
            $this->load->model('trantindakan/formtrantindakan');
            $result = $this->formtrantindakan->getpasien();
            print $result;
        }
        
        public function getantrianpasien(){
            $this->load->model('trantindakan/formtrantindakan');
            $result = $this->formtrantindakan->getantrianpasien();
            print $result;
        }
        
        public function gettblfoto(){
            $this->load->model('trantindakan/formtrantindakan');
            $result = $this->formtrantindakan->gettblfoto();
            print $result;
        }
        
        public function tampilfoto(){
            $this->load->model('trantindakan/formtrantindakan');
            $result = $this->formtrantindakan->tampilfoto();
            print $result;
        }
        
        public function modalfoto(){
            $this->load->model('trantindakan/formtrantindakan');
            $result = $this->formtrantindakan->modalfoto();
            print $result;
        }
        
        public function getDaftarTindakan(){
            $this->load->model('trantindakan/formtrantindakan');
            $result = $this->formtrantindakan->getDaftarTindakan();
            print $result;
        }

        public function getDaftarObat(){
            $this->load->model('trantindakan/formtrantindakan');
            $result = $this->formtrantindakan->getDaftarObat();
            print $result;
        }
        
        public function prosesdata(){
            $this->load->model('trantindakan/trantindakandefault');
            $result = $this->trantindakandefault->prosesdata();
            print $result;
        }
        
        public function prosesresep(){
            $this->load->model('trantindakan/trantindakandefault');
            $result = $this->trantindakandefault->prosesresep();
            print $result;
        }
        
        public function submitpasien(){
            $this->load->model('trantindakan/trantindakandefault');
            $result = $this->trantindakandefault->submitpasien();
            print $result;
        }
        
        public function hapusfoto(){
            $this->load->model('trantindakan/trantindakandefault');
            $result = $this->trantindakandefault->hapusfoto();
            print $result;
        }
        
        public function getriwayatpasien(){
            $this->load->model('trantindakan/trantindakandefault');
            $result = $this->trantindakandefault->getriwayatpasien();
            print $result;
        }
        
        public function uploadfoto(){
            $this->load->model('trantindakan/trantindakandefault');
            $result = $this->trantindakandefault->uploadfoto();
            print $result;
        }
        
        public function jsondaftarpasien()
	{
            $this->load->model('trantindakan/json');
            $result = $this->json->datapasien();
            print $result;
            
	}
}

/* 
 * Created by Pudyasto Adi Wibowo
 * Email : mr.pudyasto@gmail.com
 * pudyasto.wibowo@gmail.com
 */

